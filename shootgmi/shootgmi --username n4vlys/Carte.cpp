///////////////////////////////////////////////////////////
//  Carte.cpp
//  Implementation of the Class Carte
//  Created on:      02-nov.-2007 18:52:37
///////////////////////////////////////////////////////////
#include "Carte.h"
#include "math.h"
#include <list>
#include "tinyxml.h"
#define INFINIT 9999999

Carte::Carte()
{

}


Carte::Carte(SceneManager* pSM)
{
	fichier.open("dijstra.txt");
    mSceneMgr=pSM;

  //  mSceneMgr->setAmbientLight(ColourValue(1, 1, 1)) ;
    mSceneMgr->setShadowTechnique(SHADOWTYPE_STENCIL_ADDITIVE) ;

    Plane plane( Vector3::UNIT_Y, 0 );

    MeshManager::getSingleton().createPlane("ground",ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane,2000,2000,1,1,true,1,5,5,Vector3::UNIT_Z);
    Entity * ent = mSceneMgr->createEntity( "GroundEntity", "ground" );
	
    SceneNode * groundNode=mSceneMgr->getRootSceneNode()->createChildSceneNode("groundnode");
	//groundNode->attachObject(ent);
	groundNode->setPosition(Vector3(0,-50,0));

    ent->setMaterialName("Examples/Rockwall");
/*    Ogre::Light* light;
    light = mSceneMgr->createLight( "Light1" );
    light->setType( Light::LT_POINT );
    light->setPosition( Vector3(0, 150, 250) );

    light->setDiffuseColour( 0.50, 0, 0.5 );*/


}


Carte::~Carte()
{

}


void Carte::creerMaillage(Waypoint w1, Waypoint w2, Real pas)
{
    //Initialisation
    int nbLigne=0;
    int  nbCol=0;
    int indVoisin, indWaypoint;
int cpt=0;

    //Cr�ation des Waypoints
    for(Real i=w1.x; i<w2.x; i+=pas)
    {
        nbLigne++;
        nbCol=0;

        for(Real j=w1.z; j<w2.z; j+=pas)
        {
            Waypoint * w=new Waypoint(i,w1.y,j);
            mWaypointList.push_back(w);
            nbCol++;
        }
    }

    for(int i=0; i<nbLigne; i++)
    {
        for(int j=0; j<nbCol; j++)
        {
            indWaypoint=i*nbCol+j;
            //Cr�ation des voisins pour les lignes Verticales
            if(i-1>=0)
            {
                indVoisin=(i-1)*nbCol+j;
                mWaypointList[(int)indWaypoint]->addVoisin(mWaypointList[(int)indVoisin]);
               // mWaypointList[(int)indWaypoint]->drawLine(mWaypointList[(int)indVoisin],mSceneMgr);
                cpt++;

            }
            if(i+1<nbLigne)
            {
                indVoisin=(i+1)*nbCol+j;
                mWaypointList[(int)indWaypoint]->addVoisin(mWaypointList[(int)indVoisin]);
               // mWaypointList[(int)indWaypoint]->drawLine(mWaypointList[(int)indVoisin],mSceneMgr);
                cpt++;
            }
            //Cr�ation des voisins pour les lignes Horizontales
            if(j-1>=0)
            {
                indVoisin=i*nbCol+j-1;
                mWaypointList[(int)indWaypoint]->addVoisin(mWaypointList[(int)indVoisin]);
              //  mWaypointList[(int)indWaypoint]->drawLine(mWaypointList[(int)indVoisin],mSceneMgr);
                cpt++;
            }
            if(j+1<nbCol)
            {
                indVoisin=i*nbCol+j+1;
                mWaypointList[(int)indWaypoint]->addVoisin(mWaypointList[(int)indVoisin]);
               // mWaypointList[(int)indWaypoint]->drawLine(mWaypointList[(int)indVoisin],mSceneMgr);
                cpt++;
            }
            //Cr�ation des voisins pour les lignes en Diagonales ( No/Se )
            if(j+1<nbCol && i+1<nbLigne)
            {
                indVoisin=(i+1)*nbCol+j+1;
                mWaypointList[(int)indWaypoint]->addVoisin(mWaypointList[(int)indVoisin]);
                mWaypointList[(int)indVoisin]->addVoisin(mWaypointList[(int)indWaypoint]);
              //  mWaypointList[(int)indWaypoint]->drawLine(mWaypointList[(int)indVoisin],mSceneMgr);
                cpt++;cpt++;
            }
            //Cr�ation des voisins pour les lignes en Diagonales ( So/Ne )
            if(j!=0 && i<nbLigne-1)
            {
                indVoisin=((i+1)*nbCol)+j-1;
                mWaypointList[(int)indWaypoint]->addVoisin(mWaypointList[(int)indVoisin]);
                mWaypointList[(int)indVoisin]->addVoisin(mWaypointList[(int)indWaypoint]);
               // mWaypointList[(int)indWaypoint]->drawLine(mWaypointList[(int)indVoisin],mSceneMgr);
                cpt++;cpt++;
            }

        }
    }
}

void Carte::init( Waypoint * pWp){   // initialisation d'un sommet

   pWp->valeur = 999999999;
   pWp->couleur = "blanc";
   pWp->pred = NULL;
}

void Carte::suite(Waypoint * a, Waypoint * b){	
    float p;

    p = a->valeur ;
	p+=((*a)-(*b)).length();

    if(p <= b->valeur && b!=a)
    {
         b->pred = a;
         b->valeur = p;
		 std::vector<Waypoint*>::iterator ia;
		 for(ia = b->mVoisin.begin();ia!= b->mVoisin.end(); ia++ )
			{
				suite(b,*ia);
			}
    }
}
/*
void Carte::dijkstra(int n){

    Waypoint * wp = mWaypointList[n];
    std::vector<Waypoint*>::iterator it;
    for( it = mWaypointList.begin();it!=mWaypointList.end(); it++)
        (**it).lance();

    mWaypointList[n]->valeur = 0;
	
	for(it = wp->mVoisin.begin();it!= wp->mVoisin.end(); it++ )
    {
		suite(mWaypointList[n],*it);
	}
}
*/

void Carte::getChemin(Agent *pAgent,int pDepart, int pArrive) {
  
}
/*
void Carte::getChemin(std::deque< Waypoint* > &pWalkList, int pDepart, int pArrive)
{
    //dijkstra(pArrive);
    Waypoint *wp = mWaypointList[pDepart];
    while(wp->pred != NULL)
    {
		wp->drawLine(wp->pred,mSceneMgr);
        pWalkList.push_back(wp);
        wp = wp->pred;
    }
    pWalkList.push_back(mWaypointList[pArrive]);
}*/

Waypoint * Carte::plusProche(const Vector3& w)
{
    double distMin=INFINIT;
	double dist;
	Waypoint * meilleur;
    std::vector<Waypoint*>::iterator it;
    for( it = mWaypointList.begin();it!=mWaypointList.end(); it++)
    {
       dist=(w-**it).length();
	   if(dist<distMin)
	   {
		   meilleur=*it;
		   distMin=dist;
	   }
    }
    return meilleur;
}

Waypoint * Carte::plusProcheDe(Vector3 vector)
{
	std::vector<Waypoint*>::iterator it;
	it = std::upper_bound(mWaypointList.begin(), mWaypointList.end(),&vector);
	//(*it)->drawVoisin(mSceneMgr);
	return (*it);
}
Entity* Carte:: placerObstacle(int x1, int z1, float lon, float lar, float h, String nom)
{
        SceneNode * node=mSceneMgr->getRootSceneNode()->createChildSceneNode(nom, Vector3(x1+50*lon, 50*h, z1+50*lar));
        Entity * ent=mSceneMgr->createEntity(nom.append("m"), "Cube.mesh");

        ent->setMaterialName("Examples/BumpyMetal");

        node->attachObject(ent);
        node->setScale(lon, h, lar);

        return ent;
}


//-----------------Algo A*
void Carte::aStar(std::deque< Waypoint* > &pWalkList, int pDepart, int pArrive)
{
	//std::ofstream fichier("name.txt", std::ios::trunc);
	//Initialisation liste ouvert, liste ferme
	double rayonMax=300;
	std::list<Waypoint*> listOuvert;
	std::list<Waypoint*> listFerme;
    for(std::vector<Waypoint*>::iterator it = mWaypointList.begin();it!=mWaypointList.end(); it++)
	{
		(*it)->initStar();
	}
	
	//Point de d�part
	listOuvert.push_back(mWaypointList[pDepart]);
	mWaypointList[pDepart]->etat=1;
	mWaypointList[pDepart]->setStarDistance(0);
	mWaypointList[pDepart]->setStarPoid(0);
	mWaypointList[pDepart]->setStarHauteur((*mWaypointList[pArrive]-*mWaypointList[pDepart]).length());

	//Parcours du graph par les noeuds ouverts
	bool isEnd=false;
	Waypoint * wpPoidMin;
	std::list<Waypoint*>::iterator itPoidMin;
	while(!listOuvert.empty())
	{
		//Recherche du poid le plus petit dans les noeuds ouverts
		wpPoidMin=*(listOuvert.begin());
		itPoidMin=listOuvert.begin();
		bool isTropLoin=true;
		for(std::list<Waypoint*>::iterator it=listOuvert.begin(); it!=listOuvert.end(); it++)
		{
			if((*it)->mStarPoidGlobal < wpPoidMin->mStarPoidGlobal)
			{
				wpPoidMin=*it;
				itPoidMin=it;
			}
			if((*it)->mStarDistance<rayonMax) isTropLoin=false;
		}

		//Si tous les noeuds ouverts sont trop loin du point de d�part (>rayonMax) : on relance seulement sur le Waypoint de poid mini
		//Attention �a fait de la merde
		/*if(isTropLoin)
		{
			for(std::list<Waypoint*>::iterator it=listOuvert.begin(); it!=listOuvert.end(); it++)
			{(*it)->etat=-1;}
			listOuvert.clear();
			listOuvert.push_back(wpPoidMin);
			wpPoidMin->etat=1;
			itPoidMin=listOuvert.begin();
		}*/

		//On ouvre les noeuds voisin
		for(std::vector<Waypoint*>::iterator it=wpPoidMin->mVoisin.begin(); it!=wpPoidMin->mVoisin.end() && !isEnd; it++)
		{
			double newPoid=wpPoidMin->mStarPoid+(*wpPoidMin-*(*it)).length();
			/*std::list<Waypoint*>::iterator wpFerme=std::find(listFerme.begin(), listFerme.end(), *it);
			std::list<Waypoint*>::iterator wpOuvert=std::find(listOuvert.begin(), listOuvert.end(), *it);
			bool isOuvert=wpOuvert!=listOuvert.end(); 
			bool isFerme=wpFerme!=listFerme.end(); */
			bool isOuvert=(*it)->etat==1;
			bool isFerme=(*it)->etat==0;
			if((*it)==mWaypointList[pArrive]) //Si c'est l'arrivee
			{
				//On est arriv�->fin du parcours
				isEnd=true;
				(*it)->setStarPoid(newPoid);
				(*it)->setStarHauteur(0);
				(*it)->mParent=(*itPoidMin);
			}
			else if(isOuvert) //Si OUVERT on check poid
			{
				if(newPoid<(*it)->mStarPoid)
				{
					(*it)->setStarPoid(newPoid);
					(*it)->mParent=(wpPoidMin);
				}
			}
			else if(!isOuvert && !isFerme) //Si INIT (ni OUVERT, NI FERME) on OUVRE
			{
				(*it)->setStarDistance((**it-*mWaypointList[pDepart]).length());
				(*it)->setStarPoid(newPoid);
				(*it)->setStarHauteur((*mWaypointList[pArrive]-**it).length());
				(*it)->mParent=(wpPoidMin);
				(*it)->etat=1;//OUVRE
				listOuvert.push_back(*it);
			}
		}

		//On ferme le noeud courant
		listFerme.push_back(wpPoidMin);
		(*itPoidMin)->etat=0;//FERME
		listOuvert.erase(itPoidMin);

		if(isEnd) listOuvert.clear();
	}

	//On ajoute les waypoints dans la liste
	Waypoint * w= mWaypointList[pArrive];
	while (w!=mWaypointList[pDepart])
	{
		//w->drawLine(w->mParent, mSceneMgr);
		//fichier << *w << std::endl;
		pWalkList.push_front(w);
		w=w->mParent;
	}
	pWalkList.push_front(w);
}

void Carte::writeWaypoint()
{
	std::ofstream fichier("waypoint.xml", std::ios::trunc);
	std::vector<Waypoint*>::iterator it;
	fichier << "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>" << std::endl;
	fichier << "<carte>" << std::endl;
	for(it=mWaypointList.begin(); it!=mWaypointList.end(); it++)
	{
		// Identification du waypoint courrant
		fichier << "  <waypoint x=\""<<(*it)->x<<"\" y=\""<< (*it)->y<<"\" z=\""<< (*it)->z<<"\" indice=\"" << it-mWaypointList.begin() <<"\" >" << std::endl;

		// Voisins du Waypoint courrant
		for(int i=0; i<(int)(*it)->mVoisin.size(); i++)
		{
			std::vector<Waypoint*>::iterator itVoisin=std::find(mWaypointList.begin(), mWaypointList.end(), (*it)->mVoisin[i]);
			int ind=itVoisin - mWaypointList.begin();
			fichier << "    <voisin indice=\""<< ind << "\" />" << std::endl;
		}

		int nLastCover = -2;

		// Waypoints non visible par ce WayPoints
		for (int i=0; i< this->mWaypointList.size(); i++)
		{
			if (this->m_bVisibilityWP[it-mWaypointList.begin()][i] == false)
			{	
				if (nLastCover + 1 != i)
				{
					fichier << "    <cover indice=\""<< i << "\" />" << std::endl;
				}

				nLastCover = i;
			}
		}

		fichier << "  </waypoint>"<< std::endl;
	}
	fichier << "</carte>" << std::endl;
}


void Carte::readWaypoint()
{
	//std::ofstream debug("parse.txt", std::ios::trunc);

	// Ouverture du fichier
	TiXmlDocument doc("waypoint.xml");
	if(!doc.LoadFile())
	{
		std::cerr << "erreur lors du chargement" << std::endl;
		std::cerr << "error #" << doc.ErrorId() << " : " << doc.ErrorDesc() << std::endl;
	}

	// Cr�ation de l'arbre DOM correspondant au fichier charg�
	TiXmlHandle hdl(&doc);

	TiXmlElement * waypoint;

	// Chargement des Waypoints
	waypoint = hdl.FirstChildElement().ChildElement("waypoint", 0).Element();
	while(waypoint)
	{
		// Initialisations
		int wx, wy, wz, ind;

		// Recuperation des attributs de la balise <waypoint>
		waypoint->QueryIntAttribute("x", &wx);
		waypoint->QueryIntAttribute("y", &wy);
		waypoint->QueryIntAttribute("z", &wz);
		waypoint->QueryIntAttribute("indice", &ind);

		// Cr�ation d'un objet WP
		Waypoint * w=new Waypoint(wx, wy, wz);
		w->mIndice=ind;
		

		// Recup�ration des points de couverture
		TiXmlHandle waypointHdl(waypoint);
		TiXmlElement * cover = waypointHdl.ChildElement("cover", 0).Element();

		while(cover)
		{
			int indCover;
			cover->QueryIntAttribute("indice", &indCover);
			w->addCoverWPindex(indCover);
			cover = cover->NextSiblingElement("cover");
		}



		// Rajout du WP dans le vecteur de WP
		mWaypointList.push_back(w);

		// Incrementation
		waypoint = waypoint->NextSiblingElement("waypoint");
	}

	// Chargement des voisins
	waypoint = hdl.FirstChildElement().ChildElement("waypoint", 0).Element();
	while(waypoint)
	{
		int ind;
		waypoint->QueryIntAttribute("indice", &ind);

		TiXmlHandle waypointHdl(waypoint);
		TiXmlElement * voisin = waypointHdl.ChildElement("voisin", 0).Element();

		while(voisin)
		{
			int indVoisin;
			voisin->QueryIntAttribute("indice", &indVoisin);
			mWaypointList[ind]->mVoisin.push_back(mWaypointList[indVoisin]);
			voisin = voisin->NextSiblingElement("voisin");
		}

		waypoint = waypoint->NextSiblingElement("waypoint");
	}


}


void Carte::generateCoverWP()
{
	// On r�cupere le nombre de WayPoints du graphe
	int nNbWP = this->mWaypointList.size();

	// On cr�e la matrice
	this->m_bVisibilityWP = new bool*[nNbWP];

	// Pour chaque Waypoint, on regarde la visibilit�
	for (int i=0; i<nNbWP; i++)
	{
		this->m_bVisibilityWP[i] = new bool[nNbWP];

		//for (int j=i; j<nNbWP; j++)
		for (int j=0; j<nNbWP; j++)
		{
			// Verification de la visibilit� entre i et j
			bool bIsVisible;
			bIsVisible = this->mWaypointList[i]->visionWP(this->mWaypointList[j], GameManager::getSingleton().getScene());

			// Test de la distance max
			// Un waypoint non visible trop �loign�, est consid�r� comme visible
			if (!bIsVisible)
			{
				bIsVisible = (this->mWaypointList[j]->getVec() - this->mWaypointList[i]->getVec()).length() > 3000;
			}

			// Affectation de cette visibilit� pour les cases de la matrice correspondante
			this->m_bVisibilityWP[i][j] = bIsVisible;
		}
	}
}
