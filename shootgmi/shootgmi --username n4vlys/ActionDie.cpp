///////////////////////////////////////////////////////////
//  ActionFlee.cpp
//  Implementation of the Class ActionFlee
//  Created on:      02-nov.-2007 18:52:36
///////////////////////////////////////////////////////////

#include "ActionDie.h"
#include "GameManager.h"
#include "FSMBot.h"
#include "HistoriqueExemple.h"


ActionDie::ActionDie() : Action()
{
    mActionType="ActionDie";
}


ActionDie::ActionDie(Agent* pAgent)
    : Action(pAgent)
{
    mActionType="ActionDie";
	mAgent=pAgent;
	
}


ActionDie::~ActionDie()
{
}

void ActionDie::init()
{

	//enregistre le moment de la mort de l'agent

	mTimeScan.reset();
	
	mAgent->getBody()->raiseActorFlag(NxActorFlag::NX_AF_DISABLE_COLLISION);
	mAgent->getBody()->raiseActorFlag(NxActorFlag::NX_AF_DISABLE_RESPONSE);
	mAgent->TextState->setCaption("ActionDie");
	for(int i=0; i<(int) mAgent->mAttaquants.size();i++)
	{
		mAgent->mAttaquants[i]->mCible=NULL;
		mAgent->mAttaquants[i]->enleverAttaquant(mAgent);
		mAgent->setCible(NULL);
	
	}
	mAgent->mAttaquants.clear();
	if (mAgent->mMesh=="robot.mesh")
	{
		mAgent->mAnimationState->setEnabled(false);
		
		mAgent->setAnimationState("Die");
		mAgent->mAnimationState->setTimePosition(0);
		mAgent->mAnimationState->setWeight(10);
		mAgent->mAnimationState->setLoop(false);
		mAgent->mAnimationState->setEnabled(true);
	
		mAgent->getGameManager()->createParticule(mAgent->getPosition(),"fumee");
		mAgent->isShooting=false;
	}

	

	
	mAgent->setVelocity(Vector3(0,0,0));
	//mAgent->mFSM->mEtat="Die";
	mIsInit=true;

	mAgent->getBody()->raiseBodyFlag(NxBodyFlag::NX_BF_FROZEN_POS);
	mAgent->getBody()->raiseBodyFlag(NxBodyFlag::NX_BF_FROZEN_ROT);

	mAgent->setVelocity(Vector3(0,0,0));
	mAgent->setRotation(0);
	//mAgent->mAnimationState->setLoop(false);
	
}

void ActionDie::reset()
{
    mIsInit=false;
}

bool ActionDie::isEnd()
{
	return  false;
}


void ActionDie::update(const FrameEvent& pEvt)
{
	mLinear=Vector3::ZERO;
    mAngular=0;

/*
	if (mTimeScan.getMilliseconds()>20000)
	{
		mAgent->mAnimationState->setWeight(0);
		//mAgent->getGameManager()->createParticule(mAgent->getPosition(),"explosion");
		//mAgent->repop();
	}
	*/
}

