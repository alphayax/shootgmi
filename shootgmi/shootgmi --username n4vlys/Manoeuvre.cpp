#include "Manoeuvre.h"

Manoeuvre::Manoeuvre(){}

Manoeuvre::Manoeuvre(unsigned int pnTeamNumber, bool bClearAgentAction)
{
	this->m_nTeamNumber					= pnTeamNumber;
	this->m_bIsInit						= false;
	this->m_oManoeuvreControlerScenario = new ManoeuvreControler();
	this->m_bClearActions				= bClearAgentAction;
	this->m_bManoeuvreCombinee			= false;
	this->m_nnumEtape					= 0;

	// Une nouvelle manoeuvre vide la lsite d'action de tous les agents de l'�quipe
}


Manoeuvre::~Manoeuvre(){}



/**
* @desc	Retourne vrai si l'agent pass� en parametre est �cout�
*/
bool Manoeuvre::isListened(Ogre::String psIdAgent)
{
	vecIdAgents::iterator	it;	// Iterateur
	
	for (it = this->m_vListenedAgents.begin(); it != this->m_vListenedAgents.end(); ++it)
	{
		if (*it == psIdAgent)
			return true;
	}

	return false;
}



Manoeuvre* Manoeuvre::getCurrentScenarioManoeuvre( void )
{
	return this->m_oManoeuvreControlerScenario->getFrontManoeuvre();
}

vecIdAgents Manoeuvre::getListenersVec()
{
	return this->m_vListenedAgents;
}


bool Manoeuvre::isEnd( void )
{
	return this->m_vListenedAgents.empty();
}


void Manoeuvre::getReport(Report * poReport)
{
	if (this->isListened(poReport->getSender()))
	{
		switch (poReport->getReportType())
		{
			case REPORT_IDLE		:	break;
			case REPORT_SEEN_SONE	:	this->getReportSeenSone(poReport);	break;
			case REPORT_SEEN_STHING	:	break;				
			case REPORT_FIGHT		:	break;
			case REPORT_KILLED		:	break;
			case REPORT_MOVETO		:	this->getReportMoveTo(poReport);	break;
			case REPORT_ARRIVED		:	this->getReportArrived(poReport);	break;
		}
	}
	// Gestion des membres non �cout�s
	else
	{
		this->getReportDefault(poReport);
	}
	

	if (!this->m_oManoeuvreControlerScenario->isEmpty())
		this->m_oManoeuvreControlerScenario->m_dManoeuvreList[this->m_nnumEtape]->getReport(poReport);

	this->ManageMCScenario();
}

void Manoeuvre::ManageMCScenario( void )
{
/*
	// Si le Manoeuvre Controler n'est pas vide
	if (!this->m_oManoeuvreControlerScenario->isEmpty())
	{
		// Si la manoeuvre n'est pas initialis�e, on l'initialise
		if (!this->m_oManoeuvreControlerScenario->getFrontManoeuvre()->isInit())
		{
			this->m_oManoeuvreControlerScenario->getFrontManoeuvre()->init();
		}

		// Si la manoeuvre est termin�e, on la d�pile
		if (this->m_oManoeuvreControlerScenario->getFrontManoeuvre()->isEnd())
		{
			this->m_oManoeuvreControlerScenario->remFrontManoeuvre();
		}
	}
	
	else
	{
		if(!(this->m_vListenedAgents.empty()))
			this->m_vListenedAgents.clear();

	}
*/
	// Si nous sommes dans une manoeuvre combin�e
	if ( this->m_bManoeuvreCombinee == true)
	{
		// Si le num�ro d'�tape est inf�rieur au nombre d'�tapes
		if (this->m_nnumEtape < this->m_nnbEtapes)
		{
			// Si la manoeuvre correspondante � l'�tape n'est pas initialis�e
			if (this->m_oManoeuvreControlerScenario->m_dManoeuvreList[this->m_nnumEtape]->isInit() == false)
			{
				// On vide la liste des listeners
				this->m_vListenedAgents.clear();
				
				// On les remplace par ceux de la manoeuvre fille.
				vecIdAgents heritedListeners = this->m_oManoeuvreControlerScenario->m_dManoeuvreList[this->m_nnumEtape]->getListenersVec();

				for (unsigned int i=0; i<heritedListeners.size(); i++)
				{
					this->m_vListenedAgents.push_back(heritedListeners[i]);
				}

				// On initialise la manoeuvre fille
				this->m_oManoeuvreControlerScenario->m_dManoeuvreList[this->m_nnumEtape]->init();
			}

			// Si la manoeuvre correspondante � l'�tape est termin�e
			if (this->m_oManoeuvreControlerScenario->m_dManoeuvreList[this->m_nnumEtape]->isEnd() == true)
			{
				// On passe � l'�tape suivante
				this->m_nnumEtape++;
				ManageMCScenario ();
				
			}
		}
		else
		{
			this->m_vListenedAgents.clear();
		}
	}
	
}


bool Manoeuvre::isInit( void )
{
	return this->m_bIsInit;
}


/**
* @desc	Mise en �coute d'un agent
*/void Manoeuvre::addListenedAgent(Ogre::String psAgent)
{
	this->m_vListenedAgents.push_back(psAgent);
}


/**
* @desc	Supression de l'�coute d'un agent
*/
void Manoeuvre::removeListenedAgent(Ogre::String psAgent)
{
	// Iterateur
	vecIdAgents::iterator	it;	

	for (it = this->m_vListenedAgents.begin(); it != this->m_vListenedAgents.end(); ++it)
	{
		if (*it == psAgent)
		{
			this->m_vListenedAgents.erase(it);
			return;
		}
	}
}