#include "ManoeuvreExploreAttack.h"

ManoeuvreExploreAttack::ManoeuvreExploreAttack() : Manoeuvre() {}

ManoeuvreExploreAttack::ManoeuvreExploreAttack(unsigned int pnTeamNumber, bool bClearAgentAction)
					  : Manoeuvre(pnTeamNumber, bClearAgentAction)
{
	// Mise en �coute de l'agent qui explorera
	this->m_bManoeuvreCombinee = true;
	this->m_nnbEtapes = 3;
	
	mapAgentTeam membres = GameManager::getSingletonPtr()->m_TeamMap[this->m_nTeamNumber]->m_mTeamAgents;
	
	// Iterateur
	mapAgentTeam::iterator	it;	

	for (it = membres.begin(); it != membres.end(); ++it)
	{
		// Mise en �coute de nos agents
		this->addListenedAgent(it->first);
	}
}

void ManoeuvreExploreAttack::init()
{
	if (m_bClearActions)
		GameManager::getSingletonPtr()->m_TeamMap[m_nTeamNumber]->clearAgentAction();

	// Initialisation
	this->m_bIsInit		= true;

	// On cr�e la maoeuvre d'exploration	
	
	ManoeuvreExplore*	oMan = new ManoeuvreExplore(this->m_nTeamNumber, true);
	this->m_oManoeuvreControlerScenario->addBackManoeuvre(oMan);

	this->ManageMCScenario();
}

void ManoeuvreExploreAttack::getReportArrived(Report* poReport)
{

}

void ManoeuvreExploreAttack::getReportMoveTo(Report* poReport)
{

}

void ManoeuvreExploreAttack::getReportSeenSone(Report* poReport)
{
	if (this->m_nnumEtape==0)
	{
		// Recuperation du WP de la cible
		String		sAgent		= poReport->getTargetBot();
		Waypoint*	oTargetWP	= GameManager::getSingletonPtr()->mAgentMap[sAgent]->getWaypointCurrent();

		// On ordonne aux non �cout�s de bouger vers le point o� la cible a �t� d�t�ct�e
			// Cr�ation du manoeuvre regroup vers le point o� la cible a �t� appercu
			// On cr�e la maoeuvre
			ManoeuvreRegroup*	oMan = new ManoeuvreRegroup(this->m_nTeamNumber, oTargetWP, false);
			oMan->removeListenedAgent(poReport->getSender());

			// On la rajoute dans le manoeuvre controller
			this->m_oManoeuvreControlerScenario->addBackManoeuvre(oMan);

			// On cr�e la maoeuvre
			ManoeuvreRegroup*	oMan2 = new ManoeuvreRegroup(this->m_nTeamNumber, oTargetWP, true);
			//oMan2->removeListenedAgent(poReport->getSender());

			// On la rajoute dans le manoeuvre controller
			this->m_oManoeuvreControlerScenario->addBackManoeuvre(oMan2);

			this->m_nnumEtape++;
	}
}

void ManoeuvreExploreAttack::getReportDefault(Report* poReport)
{

}
