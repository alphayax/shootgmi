///////////////////////////////////////////////////////////
//  ActionGoTo.cpp
//  Implementation of the Class ActionGoTo
//  Created on:      02-nov.-2007 18:52:36
///////////////////////////////////////////////////////////

#include "ActionGoTo.h"
#include "GameManager.h"


ActionGoTo::ActionGoTo() : Action()
{
    mActionType="ActionGoTo";
	mIsInit=false;
}


ActionGoTo::ActionGoTo(Agent* pAgent, const Vector3& pDestination)
    : Action(pAgent), mDestination(pDestination)
{
    mActionType="ActionGoTo";
    mDistance=99999999.0f;
    mIsInit=false;
	mWaypointDestination=NULL;
}

ActionGoTo::ActionGoTo(Agent* pAgent, Waypoint * pDestination)
    : Action(pAgent), mDestination(*pDestination), mWaypointDestination(pDestination)
{
    mActionType="ActionGoTo";
    mDistance=99999999.0f;
    mIsInit=false;
}

ActionGoTo::~ActionGoTo()
{
}

void ActionGoTo::init()
{
	mAgent->mAnimationState->setEnabled(false);
	if (mAgent->mFurtif==false) mAgent->setAnimationState("Walk");
	else mAgent->setAnimationState("Stealth");
	mAgent->mAnimationState->setWeight(1);
	mAgent->mAnimationState->setEnabled(true);
    mIsInit=true;
}

void ActionGoTo::reset()
{
    mIsInit=false;
}

bool ActionGoTo::isEnd()
{
	bool isEnd=mIsInit && mDistance <= 30.0f;
	if(isEnd) mAgent->setWaypointCurrent(getWaypointDestination());
	return  isEnd;
}

void ActionGoTo::setDestination(const Vector3& pDestination)
{
    mDestination=pDestination;
	//mWaypointDestination=GameManager::getSingleton().getCarte()->plusProche(mDestination);
}

void ActionGoTo::setDestination(Waypoint* pDestination)
{
	mDestination=*pDestination;
	mWaypointDestination=pDestination;
}

Waypoint* ActionGoTo::getWaypointDestination()
{
	//if(mWaypointDestination==NULL) mWaypointDestination=GameManager::getSingleton().getCarte()->plusProche(mDestination);
	mWaypointDestination=GameManager::getSingleton().getCarte()->plusProche(mDestination);
	return mWaypointDestination;
}

void ActionGoTo::update(const FrameEvent& pEvt)
{
    Real move = mAgent->getVelocity().length() * pEvt.timeSinceLastFrame;
    Vector3 direction=mDestination-mAgent->getPosition();
    mDistance = direction.length()-move;

    //Faire peut etre deceleration

    Vector3 desiredVelocity=mDestination-mAgent->getPosition();
    desiredVelocity.normalise();
    
	if (mAgent->mFurtif==false)
		desiredVelocity*=mAgent->getMaxSpeed();
	else 
		desiredVelocity*=mAgent->getMaxSpeed()/4;

    mLinear=(desiredVelocity-1.8*mAgent->getVelocity())*2;
}
