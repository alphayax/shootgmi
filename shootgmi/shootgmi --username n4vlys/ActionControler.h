///////////////////////////////////////////////////////////
//  ActionControler.h
//  Implementation of the Class ActionControler
//  Created on:      02-nov.-2007 18:52:36
///////////////////////////////////////////////////////////

#if !defined(ACTIONCONTROLEUR_H)
#define ACTIONCONTROLEUR_H
#include <deque>
class Action;

class ActionControler
{

public:

	ActionControler();
	virtual ~ActionControler();

	//Ajoute une action en debut de la liste
	void addInFirstAction(Action* pAction);

	//Ajoute une action en fin de la liste
	void addInLastAction(Action* pAction);

	//Supprime la premiere action de la liste
	void delFirstAction();

	//renvoie la premiere action de la liste
	Action* firstAction();

	bool empty();
	void clear();

	std::deque< Action*> getDeque();

protected:
	std::deque< Action* > mActionList;

};
#endif // ACTIONCONTROLEUR
