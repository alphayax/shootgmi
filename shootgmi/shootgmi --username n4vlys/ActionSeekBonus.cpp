///////////////////////////////////////////////////////////
//  ActionSeek.cpp
//  Implementation of the Class ActionSeek
//  Created on:      02-nov.-2007 18:52:36
///////////////////////////////////////////////////////////

#include "ActionSeekBonus.h"
#include "GameManager.h"


ActionSeekBonus::ActionSeekBonus() : Action()
{
    mActionType="ActionSeekBonus";
	mActionGoByPath=NULL;
	mActionGoTo=NULL;
	mBonusWaypoint=NULL;
}


ActionSeekBonus::ActionSeekBonus(Agent* pAgent, String pType)
    : Action(pAgent)
{
    mActionType="ActionSeekBonus";
	mActionGoByPath=NULL;
	mActionGoTo=NULL;
	mBonusWaypoint=NULL;
	mType=pType;
}


ActionSeekBonus::~ActionSeekBonus()
{
	delete mActionGoByPath;
	delete mActionGoTo;
}

void ActionSeekBonus::init()
{
	reset();

	mBonusWaypoint=GameManager::getSingleton().getBonusProche(mAgent->getPosition());

	mActionGoByPath=new ActionGoByPath(mAgent, mBonusWaypoint);
	mActionGoByPath->init();
	mActionGoTo=new ActionGoTo(mAgent, mBonusWaypoint);
	mActionGoTo->init();

    mIsInit=true;
}

void ActionSeekBonus::reset()
{
	delete mActionGoByPath;
	mActionGoByPath=NULL;
    mIsInit=false;
}

bool ActionSeekBonus::isEnd()
{
	return  ((mBonusWaypoint->getVec()-mAgent->getPosition()).length()<50);
}


void ActionSeekBonus::update(const FrameEvent& pEvt)
{
	mActionGoByPath->update(pEvt);
	mLinear=mActionGoByPath->getLinear();
	mAngular=mActionGoByPath->getAngular();
}

