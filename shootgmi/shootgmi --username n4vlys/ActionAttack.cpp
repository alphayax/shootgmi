///////////////////////////////////////////////////////////
//  ActionAttack.cpp
//  Implementation of the Class ActionAttack
//  Created on:      02-nov.-2007 18:52:36
///////////////////////////////////////////////////////////

#include "ActionAttack.h"
#include "GameManager.h"


ActionAttack::ActionAttack() : Action()
{
    mActionType="ActionAttack";
	mIsInit=false;
	mIsStop=false;
}


ActionAttack::ActionAttack(Agent* pAgent)
    : Action(pAgent)
{
    mActionType="ActionAttack";
    mIsInit=false;
	mIsStop=false;
}

ActionAttack::~ActionAttack()
{
}

void ActionAttack::init()
{
    mIsInit=true;
	mAgent->mAnimationState->setTimePosition(0);
	if (mAgent->mMesh=="ninja.mesh") mAgent->setAnimationState("Idle1");
	else mAgent->setAnimationState("Idle");
}

void ActionAttack::reset()
{
    mIsInit=false;
}

bool ActionAttack::isEnd()
{
	return  false;
}

void ActionAttack::update(const FrameEvent& pEvt)
{
	if (mAgent->mMesh=="ninja.mesh") mAgent->setAnimationState("Idle1");
	else mAgent->setAnimationState("Idle");
	mLinear=Vector3::ZERO;
	mAngular=0;
	/*if(mAgent->tropProche())
	{
	}
	else if(mAgent->tropLoin())
	{
		mAgent->addInFirstAction(new ActionSeek(mAgent, mAgent->getCible()));
	}
	else
	{*/
		mAgent->shoot();
		if(mAgent->onContactWithMap)
		{
			/*if(mAgent->getVelocity().length()<3)
			{
				mAgent->setVelocity(Vector3::ZERO);
				mAgent->setRotation(0);
			}
			else
			{
				mLinear=(mAgent->getVelocity())*(-1);
				mLinear.y=0;
				mAngular=0;
			}*/
			mAgent->setVelocity(Vector3::ZERO);
			mAgent->setRotation(0);
		}
	//}
}
