#include "Projectile.h"
#include "GameManager.h"

Projectile::Projectile(const String& pName, String pProprio, const String& pMesh)
{
	mScene = GameManager::getSingletonPtr()->getScene();
	mName=pName;
	mProprio= pProprio;
	nbFrame=0;
	isAlive=true;
	String paramMesh=pName;
	paramMesh.append("; sphere.mesh");
	mBody = mScene->createBody(paramMesh, new NxOgre::SphereShape(10,"Group: sgrProj"),Vector3(0,0,0),"mass:10, Group: grProj");
	mBody->getEntity()->setCastShadows(true);
	mBody->getNode()->setScale(0.015,0.015,0.015);
	mBody->raiseBodyFlag(NxBodyFlag::NX_BF_DISABLE_GRAVITY);
	setVelocity(getVelocity().normalisedCopy()*1000);
	mTimeProject.reset();
}

Projectile::~Projectile(void)
{
}


Real Projectile::getOrientation()
{
	//return ((NxActor*) mBody)->getGlobalOrientation();
	return  mOrientation;
}


Vector3 Projectile::getPosition()
{
	return mBody->getGlobalPosition();
	//return Vector3(0,0,0);
	//return  mPosition;
}

Vector3 Projectile::getVelocity()
{
	return mBody->getLinearVelocity();
	//return  mVelocity;
}


void Projectile::setOrientation(Real pAngle)
{
    mOrientation=pAngle;
    Vector3 src = Vector3::UNIT_X;

       /*if ((1.0f + src.dotProduct(Vector3(sin(pAngle), 0, cos(pAngle)))) < 0.0001f)
       {
           mSceneNode->yaw(Degree(180));
		  
       }
       else
       {*/
           Ogre::Quaternion quat = src.getRotationTo(Vector3(sin(pAngle), 0, cos(pAngle)));
		   mBody->setGlobalOrientation(quat);
           //mSceneNode->rotate(quat);
       //} // else



}


void Projectile::setPosition(Vector3 pPosition)
{
    mPosition=pPosition;
	mBody->setGlobalPosition(pPosition);
    //mSceneNode->setPosition(pPosition);
}


void Projectile::setRotation(Real pRota)
{
    mRotation=pRota;
	mBody->setAngularVelocity(Vector3(0,pRota,0));
}


void Projectile::setVelocity(Vector3 pVelocity)
{
    mVelocity=pVelocity;
	mBody->setLinearVelocity(pVelocity);
	//((NxActor*)mBody)->
}

bool Projectile::frameStarted(const FrameEvent& pEvt)
{
	setPosition(getPosition()+getVelocity()*pEvt.timeSinceLastFrame);
	

	if(mTimeProject.getMilliseconds()>2000 || !isAlive)
	{
		GameManager::getSingletonPtr()->removeProjectile(mName);
	}
	return true;
}

