#include "FSM.h"

FSM::FSM(int defaultState)
{
	m_CurrentState = defaultState;
	mTime.reset();
}

FSM::~FSM(void)
{
	FSMState* state = NULL;
	Map_State::iterator it;

	if(!m_States.empty()){
		//on detruit tous les etats
		for(it = m_States.begin(); it != m_States.end(); ++it)
		{
			state = (FSMState*) ((*it).second);
			if(state != NULL)
				delete state;
		}
	}
}


FSMState* FSM::getState(int stateId){
	FSMState* state = NULL;
	Map_State::iterator it;

	if(!m_States.empty()){
		//on detruit tous les etats
		it = m_States.find(stateId);
		if(it != m_States.end())
			state = (FSMState*) ((*it).second);
	}
	return state;
}

void FSM::addState(FSMState* pState){
	FSMState* state = NULL;
	Map_State::iterator it;

	if(!m_States.empty()){
		// on va chercher si l'etat n'existe pas deja
		it = m_States.find(pState->getId());
		if(it != m_States.end())
			state = (FSMState*) ((*it).second);
	}
	//si state != NULL alors etat deja present dans la map
	if(state != NULL) return;
	
	//on insert l'etat dans la machine
	m_States.insert(MS_VT(pState->getId(),pState));

}



void FSM::deleteState(int stateId){
	FSMState* state = NULL;
	Map_State::iterator it;

	if(!m_States.empty()){
		
		it = m_States.find(stateId);
		if(it != m_States.end())
			state = (FSMState*) ((*it).second);
	}
	//on enleve l'etat de la map et on detruit state
	if(state != NULL && state->getId() == stateId){
		m_States.erase(it);
		delete state;
	}
}

int FSM::stateTransition(int inputTransition){
 	if(!inputTransition)
		return m_CurrentState;

	FSMState* state = this->getState(m_CurrentState);
	if(state == NULL){
		m_CurrentState = 0;
		return m_CurrentState;
	}

 	m_CurrentState = state->getOuput(inputTransition);
	return m_CurrentState;
}


