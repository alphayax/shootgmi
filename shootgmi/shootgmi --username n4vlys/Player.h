///////////////////////////////////////////////////////////
//  Player.h
//  Implementation of the Class Player
//  Created on:      02-nov.-2007 18:52:37
///////////////////////////////////////////////////////////

#if !defined(PLAYER_H)
#define PLAYER_H
#include "GameEntity.h"

class Player : public GameEntity
{

public:
	Player();
	virtual ~Player();

protected:

};
#endif // PLAYER_H
