///////////////////////////////////////////////////////////
//  Manoeuvre.h
//  Classe Abstraite
///////////////////////////////////////////////////////////

#ifndef MANOEUVRE_H
#define MANOEUVRE_H

#include "Def.h"
#include "GameManager.h"
#include "Command.h"
#include "Message.h"
#include "Report.h"
#include "ManoeuvreControler.h"

typedef std::vector< Command >	vecCommand;
typedef std::vector< String >	vecIdAgents;
typedef stdext::hash_map< const String , vecIdAgents >	mapAgentTeam;		// Map des agents de l'equipe

class ManoeuvreControler;
class ManoeuvreRegroup;

class Manoeuvre
{
public:
	Manoeuvre ();
	Manoeuvre (unsigned int pnTeamNumber, bool bClearAgentAction = true);	// Constructeur
	~Manoeuvre(void);														// Destructeur

public:
	bool	isListened( String );	// Retourne vrai si l'id de l'agent pass� en parametre est �cout�
	bool	isEnd( void );			// Retourne vrai si la manoeuvre est termin�e
	bool	isInit ( void );		// Retourne vrai si la maneouvre est initialis�e

public:
	virtual void getReport( Report* );	// Recuperation du Report

protected:
	virtual void getReportArrived( Report* oReport )	= 0;	// Traite les rapports sp�cifiant l'arriv�e a une destination
	virtual void getReportMoveTo( Report* oReport )		= 0;	// Traite les rapports sp�cifiant le d�placement vers un endroit ou un bot
	virtual void getReportSeenSone( Report* oReport )	= 0;	// Traite les rapports sp�cifiant la d�t�ction d'un agent dans le champ de vision
	virtual void getReportDefault( Report* oReport )	= 0;	// Traite les autres rapports

public:
	virtual void init ( void )	= 0;	// Initialisation de la manoeuvre

public:
	vecIdAgents getListenersVec( void );						// Renvoie le vecteur d'�cout�s
	void		addListenedAgent( String sAgent	);				// Rajoute un agent dans la liste des agents �cout�s
	void		removeListenedAgent( String sAgent	);			// Retire un agent de la liste des agents �cout�s

protected:
	Manoeuvre*	getCurrentScenarioManoeuvre(void);				// Retourne la manoeuvre courante du sc�nario
	void		ManageMCScenario(void);							// Gere le manoeuvre controler du sc�nario

protected:
	vecCommand			m_vCommandList;					// Liste des commandes � envoyer pendant le sc�nario
	vecIdAgents			m_vListenedAgents;				// Liste des agents �cout�s
	unsigned int		m_nTeamNumber;					// Numero d'�quipe
	ManoeuvreControler*	m_oManoeuvreControlerScenario;	// Liste des manoeuvres dont sera compos� le sc�nario
	bool				m_bIsInit;						// vrai si la manoeuvre est initialis�e
	bool				m_bClearActions;				// Doit-on supprimer les actions � l'initialisation de la manoeuvre
	bool				m_bManoeuvreCombinee;			// True = manoeuvre combin�e, False = manoeuvre simple
	unsigned int		m_nnbEtapes;					// Nombre d'�tape de la manoeuvre combin�e
	unsigned int		m_nnumEtape;					// Numero de l'�tape courante
};

#include "ManoeuvreRegroup.h"
#include "ManoeuvreGoVia.h"
#include "ManoeuvreExplore.h"
#include "ManoeuvreExploreAttack.h"

#endif
