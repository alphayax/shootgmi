#include "HistoriqueExemple.h"


HistoriqueExemple::HistoriqueExemple(Agent* pAgent)
{
	//String fileName= pAgent->getName();
	//fileName.append(".txt");
	//mFichier.open(fileName.c_str(),std::ios::trunc);
	mAgent=pAgent;
	mInput=new double[NB_ENTREES_NN];
	mOutput=new double[NB_SORTIES_NN];
	mInputPrec =  new double[NB_ENTREES_NN];
	mCritereNote = new double[NB_NOTES+1]; //+1 pour garder l'indice de la fin de la sequence de l'historique correspondante
	//initEtats();
}

HistoriqueExemple::~HistoriqueExemple()
{
	mFichier.close();
}

void HistoriqueExemple::initEtats()
{
	mAgent->scanEnnemis();
	
	mInput[IN_ENNEMI_VISIBLE]=mAgent->mCible!=NULL && mAgent->vision(mAgent->getCible()); 
	mInput[IN_NON_ENNEMI_VISIBLE]=!mInput[IN_ENNEMI_VISIBLE];
	
	mInput[IN_HAS_TARGET] = (mAgent->getCible()!=NULL);
	mInput[IN_NOT_HAS_TARGET] = !mInput[IN_HAS_TARGET];
	
	mInput[IN_TARGET_FAR]=mAgent->tropLoin();
	mInput[IN_TARGET_CLOSE]=mAgent->tropProche();

	mInput[IN_VIE_0] = (mAgent->mEtat.pointsVie<=0);
	mInput[IN_VIE_0_33]=(mAgent->mEtat.pointsVie>0 && mAgent->mEtat.pointsVie<=33);				
	mInput[IN_VIE_34_66]=(mAgent->mEtat.pointsVie>=34 && mAgent->mEtat.pointsVie<=66);				
	mInput[IN_VIE_67_100]=(mAgent->mEtat.pointsVie>=67 && mAgent->mEtat.pointsVie<=100);				
	
	mInput[IN_VU_PAR_CIBLE]=(mAgent->mCible!=NULL && mAgent->mCible->vision(mAgent));			
	mInput[IN_NON_VU_PAR_CIBLE]= !mInput[IN_VU_PAR_CIBLE];	

	mInput[IN_VIE_ENNEMI_0]     =   (mAgent->mCible!=NULL &&  mAgent->mCible->mEtat.pointsVie<=0);
	mInput[IN_VIE_ENNEMI_0_33]  =	(mAgent->mCible!=NULL &&  mAgent->mCible->mEtat.pointsVie>0 && mAgent->mCible->mEtat.pointsVie<=33);				
	mInput[IN_VIE_ENNEMI_34_66] =	(mAgent->mCible!=NULL &&  mAgent->mCible->mEtat.pointsVie>=34 && mAgent->mCible->mEtat.pointsVie<=66);				
	mInput[IN_VIE_ENNEMI_67_100]=	(mAgent->mCible!=NULL &&  mAgent->mCible->mEtat.pointsVie>=67 && mAgent->mCible->mEtat.pointsVie<=100);				
	
	mInput[IN_NB_ATTAQUANTS_0] = (mAgent->mAttaquants.size()==0);
	mInput[IN_NB_ATTAQUANTS_1_2] = (mAgent->mAttaquants.size()==1 || mAgent->mAttaquants.size()==2);	
	mInput[IN_NB_ATTAQUANTS_SUP3] = (mAgent->mAttaquants.size()>2);

	mInput[IN_ACTION_ATTACK] = (mAgent->getFirstAction()=="ActionAttack");  //A faire
	mInput[IN_ACTION_FLEE] = (mAgent->getFirstAction()=="ActionFlee");	
	mInput[IN_ACTION_WANDER] = (mAgent->getFirstAction()=="ActionWander");		
	mInput[IN_ACTION_ENNEMI_ATTACK] = (mAgent->mCible!=NULL &&  mAgent->mCible->getFirstAction()=="ActionAttack");	//A faire	
	mInput[IN_ACTION_ENNEMI_FLEE] = (mAgent->mCible!=NULL &&  mAgent->mCible->getFirstAction()=="ActionFlee");		
	mInput[IN_ACTION_ENNEMI_WANDER] = (mAgent->mCible!=NULL &&  mAgent->mCible->getFirstAction()=="ActionWander");		
	
	mInput[IN_WEAPON_SHOTGUN] = mAgent->mArme.name == "Shotgun";	
	mInput[IN_WEAPON_PISTOL]  = mAgent->mArme.name == "Pistol";	
	mInput[IN_WEAPON_ASSAULT] = mAgent->mArme.name == "Assault";
	mInput[IN_WEAPON_SNIPE]   = mAgent->mArme.name == "Snipe";
	
}

void HistoriqueExemple::initNote()
{
	mCritereNote[NOTE_DEGAT_FAIT]       = 0;
	mCritereNote[NOTE_DEGAT_SUBIT]      = 0;
	mCritereNote[NOTE_ENNEMI_TUE]       = 0;
	mCritereNote[NOTE_NB_ENNEMI_VU]     = 0;
	mCritereNote[NOTE_AGENT_MORT]       = (mAgent->mEtat.pointsVie > 0) ? 0 : 1;
	mCritereNote[NOTE_TEMPS_SURVIE]     = 0;
	mCritereNote[NOTE_TEMPS_ENNEMI_TUE] = GameManager::getSingleton().mTimerPartie.getMilliseconds();
}

void HistoriqueExemple::modifierEtat(int pEtat, bool pValeur)
{
	mInput[pEtat]=pValeur;
}

void HistoriqueExemple::ecrireHisto()
{
	/*initEtats();
	if(memcmp(mInput,mInputPrec,NB_ENTREES_NN)!=0)
	{*/
		double * ligneHisto=new double[NB_ENTREES_NN];
		for (int i=0; i<NB_ENTREES_NN; i++)
		{
			ligneHisto[i]=mInput[i];
		}
		
		mHisto.push_back(ligneHisto);

		memcpy(mInputPrec,mInput,NB_ENTREES_NN);
		score();
	//}
}

//C'est moche, j'ai fait le cochon et j'assume (sylv)
void HistoriqueExemple::ecrireHistoNote()
{
	//Initialisation des iterateurs
	std::list<double*>::iterator itNote=mHistoNote.end();// fin de la liste des notes
	if(mHistoNote.empty()) mScoredLineHisto=mScore.begin();
	else 
	{
		itNote--;
		if(mScoredLineHisto!=mScore.end()) mScoredLineHisto++;
	}

	//Creation de la note
	if(mHistoNote.empty() || (*itNote)[NB_NOTES]!=mScore.size()-1) //Pour pas avoir 2 scoring cons�cutifs (ca ferait de la merde)
	{
		if(mCritereNote[NOTE_ENNEMI_TUE]==0) mCritereNote[NOTE_TEMPS_ENNEMI_TUE]=-1;
		if(mCritereNote[NOTE_AGENT_MORT]==0) mCritereNote[NOTE_TEMPS_SURVIE]=-1;
		mCritereNote[NB_NOTES]=mScore.size()-1;
		mHistoNote.push_back(mCritereNote);

		//Scoring sur les inputs precedents
		if(GameManager::getSingleton().mNbRound>=999999)
		{
			while(mScoredLineHisto!=mScore.end())
			{
				majScore(*mScoredLineHisto, mCritereNote);
				mScoredLineHisto++;
			}
			if(!mScore.empty())
				mScoredLineHisto--;
		}

		//On init la prochaine note
		mCritereNote= new double[NB_NOTES+1];
		initNote();
		std::cout<<"change action "<<mAgent->getName()<<std::endl;
	}
}


void HistoriqueExemple::afficher()
{
	String fileName= mAgent->getName();
	fileName.append(".txt");
	mFichier.open(fileName.c_str(),std::ios::trunc);
	int indHisto=0;
	std::list<double*>::iterator it=mHisto.begin();
	std::list<double*>::iterator itScore=mScore.begin();
	std::list<double*>::iterator itNote=mHistoNote.begin();

	while(it!=mHisto.end())
	{
		for(int i=0; i<NB_ENTREES_NN; i++)
		{
			mFichier << (*it)[i]<<" ";
		}
		for(int i=0; i<NB_SORTIES_NN; i++)
		{
			mFichier << (*itScore)[i]<<" ";
		}
		mFichier << std::endl;	

		it++;
		itScore++;
		indHisto++;
	}
	mFichier.close();
}


void HistoriqueExemple::afficherDetails()
{
	String fileName= mAgent->getName();
	fileName.append("Note.txt");
	std::ofstream note(fileName.c_str(),std::ios::trunc);

	int indHisto=0;
	std::list<double*>::iterator it=mHisto.begin();
	std::list<double*>::iterator itScore=mScore.begin();
	std::list<double*>::iterator itNote=mHistoNote.begin();

	while(it!=mHisto.end())
	{
		for(int i=0; i<NB_ENTREES_NN; i++)
		{
			note << (*it)[i]<<" ";
		}
		note << " [OUTPUT A/F/W/S : ";
		for(int i=0; i<NB_SORTIES_NN; i++)
		{
			note << (*itScore)[i]<<" ";
		}
		note << "]";
		note << std::endl;

		if(indHisto==(*itNote)[NB_NOTES])
		{
			note << " [NOTE DFait/DSubit/ETu�/EVu/AMort/TSurvie/TETu� : ";
			for(int i=0; i<NB_NOTES; i++)
			{
				note << (*itNote)[i]<<" ";
			}
			note << "]";
			note << std::endl;
			itNote++;
		}

		it++;
		itScore++;
		indHisto++;
	}

	/*note << "################################" << std::endl;
	itNote=mHistoNote.begin();
	for(;itNote!=mHistoNote.end();itNote++)
	{
		for(int i=0; i<NB_NOTES+1; i++)
		{
			note << (*itNote)[i]<<" ";
		}
		note << std::endl;
	}*/

	note.close();
}

void HistoriqueExemple::clear()
{
	std::list<double*>::iterator it;

	for(it=mScore.begin(); it!=mScore.end(); it++){
		delete *it;
	}
	mScore.clear();

	for(it=mHisto.begin(); it!=mHisto.end(); it++){
		delete *it;
	}
	mHisto.clear();

	for(it=mHistoNote.begin(); it!=mHistoNote.end(); it++){
		delete *it;
	}
	mHistoNote.clear();
}

void HistoriqueExemple::score()
{
	double * ligneScore=new double[NB_SORTIES_NN];
	mOutput[OUT_ACTION_ATTACK]=ScoreOutputAttack();
	mOutput[OUT_ACTION_FLEE]=ScoreOutputFlee();
	mOutput[OUT_ACTION_WANDER]=ScoreOutputWander();
	mOutput[OUT_ACTION_SEEK] = this->ScoreOutputSeek();
	mOutput[OUT_ACTION_BONUS] = ScoreOutputBonus();
	for (int i=0; i<NB_SORTIES_NN; i++)
	{
		ligneScore[i]=mOutput[i];
	}
	
	//ligneHisto[NB_ENTREES_NN+1]=note(); //A faire
	mScore.push_back(ligneScore);

	
}

void HistoriqueExemple::majScore(double* pTabScore, double* pTabNote)
{
	if(mAgent->LastAction == "ActionAttack" )
	{
		pTabScore[OUT_ACTION_ATTACK]=ScoringOutputAttack(pTabNote);
		pTabScore[OUT_ACTION_FLEE]=9;//ScoringOutputFlee(pTabNote);
		pTabScore[OUT_ACTION_WANDER]=9;//ScoringOutputWander(pTabNote);
		pTabScore[OUT_ACTION_SEEK]=9;
		pTabScore[OUT_ACTION_BONUS]=9;
	}
	if(mAgent->LastAction == "ActionFlee" )
	{
		pTabScore[OUT_ACTION_ATTACK]=9;//ScoringOutputAttack(pTabNote);
		pTabScore[OUT_ACTION_FLEE]=ScoringOutputFlee(pTabNote);
		pTabScore[OUT_ACTION_WANDER]=9;//ScoringOutputWander(pTabNote);
		pTabScore[OUT_ACTION_SEEK]=9;
		pTabScore[OUT_ACTION_BONUS]=9;
	}
	if(mAgent->LastAction == "ActionWander" )
	{
		pTabScore[OUT_ACTION_ATTACK]=9;//ScoringOutputAttack(pTabNote);
		pTabScore[OUT_ACTION_FLEE]=9;//ScoringOutputFlee(pTabNote);
		pTabScore[OUT_ACTION_WANDER]=ScoringOutputWander(pTabNote);
		pTabScore[OUT_ACTION_SEEK]=9;
		pTabScore[OUT_ACTION_BONUS]=9;
	}
}
//---------------Score supervis�-----------------//
int HistoriqueExemple::ScoreOutputAttack()
{
	if(this->mAgent->peur==1)
	{
		if(mInput[IN_HAS_TARGET] && (mInput[IN_VIE_67_100] ) && !mInput[IN_TARGET_FAR] && !mInput[IN_TARGET_CLOSE] && !mInput[IN_VU_PAR_CIBLE])
			return 1;
	}
	else if(this->mAgent->violent==1)
	{
		if(mInput[IN_HAS_TARGET] && (mInput[IN_VIE_67_100] || mInput[IN_VIE_34_66] ) && !mInput[IN_TARGET_FAR] && !mInput[IN_TARGET_CLOSE])
			return 1;
	}else{
		if(mInput[IN_HAS_TARGET] && (mInput[IN_VIE_67_100] || mInput[IN_VIE_34_66]) && !mInput[IN_TARGET_FAR] && !mInput[IN_TARGET_CLOSE] && !mInput[IN_VU_PAR_CIBLE])
			return 1;
	}
	return 0;
}



int HistoriqueExemple::ScoreOutputFlee()
{
	if(this->mAgent->peur==1)
	{
		if (mInput[IN_HAS_TARGET] && (mInput[IN_VIE_0_33] || mInput[IN_VIE_34_66]) &&(mInput[IN_VU_PAR_CIBLE] || mInput[IN_TARGET_CLOSE]))
			return 1;
	}
	else if(this->mAgent->violent==1)
	{
		if (mInput[IN_HAS_TARGET] &&( mInput[IN_TARGET_CLOSE] || mInput[IN_VIE_0_33]))
			return 1;
	
	}else{
		if (mInput[IN_HAS_TARGET] && (mInput[IN_VU_PAR_CIBLE] || mInput[IN_VIE_0_33]))
			return 1;
	}
	return 0;
}

int HistoriqueExemple::ScoreOutputBonus()
{
	if(this->mAgent->peur==1 && this->mAgent->roublard==1)
	{
		if(mInput[IN_VIE_34_66] && !mInput[IN_HAS_TARGET])
			return 1;
	}
	else if(this->mAgent->violent==1)
	{
		if (!mInput[IN_HAS_TARGET] && mInput[IN_VIE_0_33])
			return 1;
	}
	return 0;
}

int HistoriqueExemple::ScoreOutputWander()
{
	if(!mInput[IN_HAS_TARGET] && (mInput[IN_VIE_67_100] || mInput[IN_VIE_34_66] ) )
		return 1;
	else return 0;
}

int HistoriqueExemple::ScoreOutputSeek()
{
	if(this->mAgent->peur==1)
	{
		if(mInput[IN_HAS_TARGET] && mInput[IN_TARGET_FAR] && !mInput[IN_VU_PAR_CIBLE] )
			return 1;
		else return 0;
	}
	else 
	{
		if(mInput[IN_HAS_TARGET] && mInput[IN_TARGET_FAR] )
			return 1;
		else return 0;
	}
}

//------------- Score par experience ------------//

int HistoriqueExemple::ScoringOutputAttack(double* pTabNote)
{
	if( (pTabNote[NOTE_DEGAT_FAIT] > pTabNote[NOTE_DEGAT_SUBIT]) || (pTabNote[NOTE_ENNEMI_TUE]>0))
	{
		return 1;
	}
	else return 0;
}

int HistoriqueExemple::ScoringOutputFlee(double* pTabNote)
{
	/*if(mCritereNote[NOTE_TEMPS_SURVIE]>0 && mCritereNote[NOTE_TEMPS_SURVIE]<5000 && mCritereNote[NOTE_TEMPS_SURVIE]!=-1)
		return 0;
	if(mCritereNote[NOTE_TEMPS_SURVIE]>=5000 && mCritereNote[NOTE_TEMPS_SURVIE]<10000 && mCritereNote[NOTE_TEMPS_SURVIE]!=-1)
		return 0.5;
	else return 1;*/
	return 0;
}

int HistoriqueExemple::ScoringOutputWander(double* pTabNote)
{
	if(pTabNote[NOTE_DEGAT_SUBIT]>0 )
		return 0;
	else return 1;
}