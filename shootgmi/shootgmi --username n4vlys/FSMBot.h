#ifndef FSMBOT_H
#define FSMBOT_H

#include "FSM.h"
class FSMBot : public FSM
{
public:
	FSMBot(Agent * pAgent);
	~FSMBot();
	void decide();
};


#endif FSMBOT_H