///////////////////////////////////////////////////////////
//  Action.cpp
//  Implementation of the Class Action
//  Created on:      02-nov.-2007 18:52:36
///////////////////////////////////////////////////////////

#include "Action.h"



Action::Action()
{
    mAgent=NULL;
    mIsInit=false;
    mLinear=Vector3::ZERO;
    mAngular=0;
	this->m_RatioVision=0.8;
}

Action::Action(Agent* pAgent)
    : mAgent(pAgent)
{
    mIsInit=false;
    mLinear=Vector3::ZERO;
    mAngular=0;
	this->m_RatioVision=0.8;
}


Action::~Action()
{

}

bool Action::isInit()
{
    return mIsInit;
}


Vector3 Action::getLinear()
{
    return mLinear;
}

Real Action::getAngular()
{
    return mAngular;
}

String Action::getActionType()
{
    return mActionType;
}

Real Action::getRatioVision()
{
	return this->m_RatioVision;
}