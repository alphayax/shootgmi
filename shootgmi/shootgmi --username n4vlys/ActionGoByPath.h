///////////////////////////////////////////////////////////
//  ActionGoByPath.h
//  Implementation of the Class ActionGoByPath
//  Created on:      02-nov.-2007 18:52:36
///////////////////////////////////////////////////////////

#if !defined(ACTIONGOBYPATH_H)
#define ACTIONGOBYPATH_H
#include "Action.h"
#include <deque>
#include "Waypoint.h"

using namespace Ogre;

class ActionGoTo;

class ActionGoByPath : public Action
{

public:
	ActionGoByPath();

	ActionGoByPath(Agent* pAgent, const Vector3& pDestination);
	ActionGoByPath(Agent* pAgent, Waypoint * pDestination);


	virtual ~ActionGoByPath();

    virtual bool isEnd();
    virtual void init();
	virtual void reset();
	virtual void update(const FrameEvent& pEvt);

	virtual void setDestination(const Vector3& pDestination);
	virtual void setDestination(Waypoint * pDestination);
	virtual Waypoint* getWaypointDestination();
	virtual void popFront();

	int mNbWalkedWaypoint;

protected:
	Vector3 mDestination;
	Waypoint * mWaypointDestination;
	Waypoint * mWaypointCurrent;
	std::deque<Waypoint*> mWalkList;

	ActionGoTo * mActionGoTo;
	void arrange();

};
#endif // ACTIONGOBYPATH_H
