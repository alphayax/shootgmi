#ifndef DEF_H_INCLUDED
#define DEF_H_INCLUDED

#include <map>
#include <iostream>

#include "FSMState.h"	// Encore utile ??



//===========================================
//   Definition des constantes
//===========================================
#define CONST_GROUND			5
#define CONST_MESH_WP			"Barrel.mesh"
#define CONST_MAP_DIMENSION		750
#define CONST_BOT_MESH			"robot.mesh"
#define CONST_PARTIE_TIME_OUT   9999990000
#define	CONST_PI				3.1416


//===========================================
//   Definition des types
//===========================================

// MAP  int -> Objet
typedef std::map< int, FSMState* ,std::less<int> > Map_State;		// Useless ? plus de fsm il me semble...
typedef Map_State::value_type MS_VT;




//===========================================
//   Definition des Transitions (INPUT_) & 
//   Definition des Etats (STATE_)
//===========================================

#define STATE_FLEE   1 //Attente , check statut
#define STATE_WANDER 2 //Errer
#define STATE_COMBAT 3 //En combat , tactique ou bourrin
#define STATE_SEEK   4 //En recherche d'adversaire
#define STATE_DEAD   5 //Mort
#define STATE_COVER  6 //Se couvrir
#define STATE_IDLE   7 //Idle

#define INPUT_DAMAGE              1 //se fait tirer dessus
#define INPUT_TARGET_IN_SIGHT     2 //cible en vue
#define INPUT_STOP_FLEE           3 //arrete de fuire
#define INPUT_NO_TARGET_IN_SIGHT  4 //pas de cible en vue
#define INPUT_RESPAWN_TIME		  5 //repop du robot
#define INPUT_STOP_ALL			  6 //On arrete tout

//===========================================
//         Definition des Messages
//===========================================

#define MSG_DAMAGE_FROM_PLAYER 1
#define MSG_DEAD               2
#define MSG_KILLER             3
#define MSG_MISS               4
#define MSG_HELP               5

#define MSG_ALL                -1
#define MSG_TEAM			   -2



//===========================================
//         Definition des entr�es du NN
//===========================================
#define IN_ENNEMI_VISIBLE		0
#define IN_NON_ENNEMI_VISIBLE	1

#define IN_TARGET_FAR			2
#define IN_TARGET_CLOSE			3

#define IN_HAS_TARGET           4
#define IN_NOT_HAS_TARGET       5

#define IN_VIE_0                6
#define IN_VIE_0_33				7
#define IN_VIE_34_66			8
#define IN_VIE_67_100			9

#define IN_VU_PAR_CIBLE			10
#define IN_NON_VU_PAR_CIBLE		11
#define IN_VIE_ENNEMI_0			12
#define IN_VIE_ENNEMI_0_33		13		
#define IN_VIE_ENNEMI_34_66		14	
#define IN_VIE_ENNEMI_67_100	15
#define IN_NB_ATTAQUANTS_0		16
#define IN_NB_ATTAQUANTS_1_2	17
#define IN_NB_ATTAQUANTS_SUP3	18
#define IN_ACTION_ATTACK		19
#define IN_ACTION_FLEE			20
#define IN_ACTION_WANDER		21
#define IN_ACTION_ENNEMI_ATTACK	22
#define IN_ACTION_ENNEMI_FLEE	23
#define IN_ACTION_ENNEMI_WANDER	24

#define IN_WEAPON_SHOTGUN		25
#define IN_WEAPON_PISTOL		26
#define IN_WEAPON_ASSAULT		27
#define IN_WEAPON_SNIPE			28



#define NB_ENTREES_NN			29

//===========================================
//         Definition des sorties du NN
//===========================================

#define OUT_ACTION_ATTACK 0
#define OUT_ACTION_FLEE   1
#define OUT_ACTION_WANDER 2
#define OUT_ACTION_SEEK	  3
#define OUT_ACTION_BONUS  4

#define NB_SORTIES_NN     5


//===========================================
//         Definition des criteres de notation
//===========================================

#define NOTE_DEGAT_FAIT			0
#define NOTE_DEGAT_SUBIT		1
#define NOTE_ENNEMI_TUE			2
#define NOTE_NB_ENNEMI_VU		3
#define NOTE_AGENT_MORT  		4
#define NOTE_TEMPS_SURVIE		5
#define NOTE_TEMPS_ENNEMI_TUE	6

#define NB_NOTES				7


//===========================================
//         Definition des objectifs individuels
//===========================================

#define BUT_TUER				0
#define BUT_SURVIVRE			1

#define NB_BUT					2

#endif
