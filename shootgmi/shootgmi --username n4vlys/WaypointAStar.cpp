#include "WaypointAStar.h"
#define INFINIT 99999999

WaypointAStar::WaypointAStar()
{
	mIndice=-1;
	initStar();
}

void WaypointAStar::initStar()
{
	mParent=-1;
	mStarPoid=INFINIT;
	mStarHauteur=INFINIT;
	mStarPoidGlobal=INFINIT;
	mStarDistance=0;
	etat=-1;
}

void WaypointAStar::setStarPoid(double p)
{
	mStarPoid=p;
	syncStarPoidGlobal();
}

void WaypointAStar::setStarHauteur(double h)
{
	mStarHauteur=h;
	syncStarPoidGlobal();
}
void WaypointAStar::syncStarPoidGlobal()
{
	mStarPoidGlobal=mStarPoid+mStarHauteur;
}

void WaypointAStar::setStarDistance(double d)
{
	mStarDistance=d;
}