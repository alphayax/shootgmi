///////////////////////////////////////////////////////////
//  ActionSentinel.h
///////////////////////////////////////////////////////////

#ifndef ACTIONSENTINEL_H
#define ACTIONSENTINEL_H

#include "Action.h"

class ActionSentinel : public Action
{

public:
	ActionSentinel();
	ActionSentinel(Agent* pAgent);
	virtual ~ActionSentinel();
	
protected:
	bool mIsStop;	// ???

public:
	virtual bool isEnd();
    virtual void init();
	virtual void reset();
	virtual void update(const FrameEvent& pEvt);

};
#endif
