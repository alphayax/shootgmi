#include "ManoeuvreGoVia.h"

ManoeuvreGoVia::ManoeuvreGoVia() : Manoeuvre() {}

ManoeuvreGoVia::ManoeuvreGoVia(unsigned int pnTeamNumber, vecWP pvPath, bool bClearAgentAction)
			  : Manoeuvre(pnTeamNumber, bClearAgentAction)
{
	this->m_vPath	= pvPath;
	
	// Recuperation des membres de la team
	mapAgentTeam membres = GameManager::getSingletonPtr()->m_TeamMap[this->m_nTeamNumber]->m_mTeamAgents;
	
	// Iterateur
	mapAgentTeam::iterator	it;	

	for (it = membres.begin(); it != membres.end(); ++it)
	{
		// Mise en �coute de nos agents
		this->addListenedAgent(it->first);
	}
}


void ManoeuvreGoVia::getReportArrived(Report* poReport)
{
	vecIdAgents::iterator	it;	// Iterateur
	
	for (it = this->m_vListenedAgents.begin(); it != this->m_vListenedAgents.end(); ++it)
	{
		if (*it == poReport->getSender())
		{
			this->m_vListenedAgents.erase(it);
			break;
		}
	}
}

void ManoeuvreGoVia::getReportMoveTo(Report* poReport)
{

}

void ManoeuvreGoVia::getReportSeenSone(Report* poReport)
{

}

void ManoeuvreGoVia::getReportDefault(Report* poReport)
{

}

void ManoeuvreGoVia::init()
{
	//Suppression des actions
	if (m_bClearActions)
		GameManager::getSingletonPtr()->m_TeamMap[m_nTeamNumber]->clearAgentAction();

	// Initialisation
	this->m_bIsInit	= true;
	
	// On cr�e autant de manoeuvre regroup que n�c�ssaire
	vecWP::iterator	it;	

	for (it = this->m_vPath.begin(); it != this->m_vPath.end(); ++it)
	{
		// On cr�e la maoeuvre
		ManoeuvreRegroup*	oMan = new ManoeuvreRegroup(this->m_nTeamNumber, *it);

		this->m_oManoeuvreControlerScenario->addBackManoeuvre(oMan);
	}

	this->ManageMCScenario();
}