///////////////////////////////////////////////////////////
//  Waypoint.h
//  Implementation of the Class Waypoint
//  Created on:      02-nov.-2007 18:52:38
///////////////////////////////////////////////////////////

#if !defined(WAYPOINT_H)
#define WAYPOINT_H
#include "GameEntity.h"
#include "NxOgre.h"
#include <vector>

using namespace Ogre;

class Waypoint;

typedef		std::vector< int >			vecInt;
//typedef		std::vector< Waypoint* >	vecWP;

class Waypoint : public GameEntity, public Vector3
{
public:
    static int	inta;
    Waypoint	*pred;
    float	valeur ;
    String	couleur;
	int		mIndice;

	//--A*
	Waypoint *mParent;
	double	mStarPoid;
	double	mStarHauteur;
	double	mStarPoidGlobal;
	double	mStarDistance;
	int		etat; //-1 init, 0 ferme, 1 ouvert

	void initStar();
	void setStarPoid(double);
	void setStarHauteur(double);
	void setStarDistance(double);
	void syncStarPoidGlobal();
	//--


    Waypoint();

	Waypoint(const Real fX, const Real fY, const Real fZ);
	Waypoint(const Vector3 &);
	
    virtual ~Waypoint();
    void addVoisin(Waypoint* w);
    std::vector<Waypoint*>::iterator removeVoisin(Waypoint *w);
    std::vector< Waypoint* > mVoisin;
    void drawLine(Waypoint *, SceneManager*, String couleur="bleu");
    void drawVoisin(SceneManager*);
    //void lance();
	bool wpVisible(Waypoint * w, NxOgre::Scene * mScene);

	// Vector3
	Vector3 getVec();
    int		getX();
    int		getY();
    int		getZ();

	bool		visionWP(Waypoint* oWPCible, NxOgre::Scene* oScene);
	void		addCoverWPindex(int nCoverWP);
	Waypoint*	getNearestCoverWP(Waypoint*);	// Recuperation du point de couverture le plus proche du Waypoint pass� en parametre

protected:
	vecInt	m_vWPcoverList;	// Liste des waypoints de couverture


private:
};
#endif // WAYPOINT_H
