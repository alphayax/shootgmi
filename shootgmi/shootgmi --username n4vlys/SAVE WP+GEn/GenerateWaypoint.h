#ifndef GENERATEWAYPOINT_H
#define GENERATEWAYPOINT_H

#include "Carte.h"
#include "NxOgre.h"

using namespace NxOgre;

class Index;
class TriangleList;

class GenerateWaypoint : public FrameListener
{
public:
	GenerateWaypoint();
	GenerateWaypoint(Entity *,Carte *,SceneManager*);
	virtual ~GenerateWaypoint();

public:
	bool frameStarted(const FrameEvent& pEvt);
	void update(const FrameEvent& pEvt);
	bool notRepeated(std::vector<Ogre::Vector3>* verts, Ogre::Vector3& A1, Ogre::Vector3& A2);
	void Fabtest();
	TriangleList& triangulationMesh(TriangleList& pTriangleList);
	void createWaypoint(std::vector<Vector3> &triangleList);
	double coefDirOnY(Vector3& v);
	void composanteConnexe(Waypoint *wp);
	void getMeshInformation(
		Mesh* mesh, size_t &vertex_count, Vector3* &vertices,
		size_t &index_count, unsigned long* &indices,
		const Vector3 &position ,
		const Quaternion &orient, const Vector3 &scale );
	void filtreVerticalWaypoint();
	void indexion(TriangleList &pTriangleList) ;
	bool RaycastFromPoint(Waypoint * point,Waypoint *normal);

// Devrait �tre en prot�g�...
public:
	int mFrame;
	std::ofstream fichier;
	std::vector<Waypoint> mIndex;
	std::vector<Vector3> mIndexedTriangleList;

protected:
    Entity*			nil;
    Carte*			mCarte;
    bool			test;
    SceneManager*	mSceneMgr;
};

#endif
