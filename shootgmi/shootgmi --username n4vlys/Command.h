#ifndef COMMAND_H
#define COMMAND_H

#include "ogre.h"
#include "Telegram.h"

using namespace Ogre;

//-------------------
// Types de Rapports
//-------------------
#define	COMMAND_SENTINEL		0	// Reste sur place et surveille les alentours
#define	COMMAND_MOVETO			1	// D�place toi vers un endroit
#define COMMAND_ATTACK			2	// Attaque
#define COMMAND_SEARCH			3	// Cherche un ennemi


class Command : public Telegram
{
public:
	Command(String sReceiver, int nCommandType, int nGoal);		// Ordonne a un agent de ne rien faire
	Command(String sReceiver, int nCommandType, int nGoal, Vector3 oTargetWP);	// Ordonne a un agent une interation avec un emplacement
	Command(String sReceiver, int nCommandType, int nGoal, String sTargetBot);	// Ordonne a un agent une interation avec un Bot

public:
	int getCommandType(void);	// Retourne le type de rapport
	String	getTargetBot(void);	// Retourne le Bot cibl�
	Vector3	getTargetWP(void);	// Retourne le WP cibl�

protected:
	int		m_nCommand;		// Type de commande
	int		m_nGoal;		// But de l'agent
	String	m_sTargetBot;	// L'agent concern� par l'ordre
	Vector3	m_oTargetWP;	// Un emplacement concern� par l'ordre
};

#endif