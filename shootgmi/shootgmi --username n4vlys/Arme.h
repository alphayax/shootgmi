#ifndef ARME_H
#define ARME_H
#include "ogre.h"
using namespace Ogre;

enum typeArme
{
	PISTOL,
	ASSAULT,
	SHOTGUN,
	SNIPE
};


class Arme
{
public:
	String	name;
    int		capacite;
	float	precision;	// entre 0(pas pr�cis) et 1(tr�s pr�cis)
	int		cadence;	// tire une balle tous les x millisecondes
	float	poids;		// facteur multiplicatif de la vitesse de l'agent
	int		degat;		// degat inflige lors d un impact
	int distMin;
	int distMax;

	Timer		mTimeShoot;

	Arme();
	Arme(typeArme);
	~Arme(void);
};

#endif