///////////////////////////////////////////////////////////
//  WaypointAStar.h
//  Implementation of the Class WaypointAStar
//  Created on:      02-nov.-2007 18:52:38
///////////////////////////////////////////////////////////

#ifndef WAYPOINTASTAR_H
#define WAYPOINTASTAR_H

class WaypointAStar
{
public:
		int mIndice;
		int mParent;
		double mStarPoid;
		double mStarHauteur;
		double mStarPoidGlobal;
		double mStarDistance;
		int etat; //-1 init, 0 ferme, 1 ouvert
		void initStar();
		void setStarPoid(double);
		void setStarHauteur(double);
		void setStarDistance(double);
		void syncStarPoidGlobal();

		WaypointAStar();
};

#endif // WAYPOINTASTAR_H
