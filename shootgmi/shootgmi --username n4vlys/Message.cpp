#include "Message.h"

/**
* @desc	Initialisation du singleton
*/
Message* Message::m_oMessageSingleton = NULL;

/**
* @desc	R�cup�ration du singleton Message
*/
Message* Message::getSingleton()
{
	if(m_oMessageSingleton == NULL)
		m_oMessageSingleton = new Message();

	return m_oMessageSingleton;
}

/**
* @desc	Envoie un t�l�gramme a un agent
*/
void Message::send(Telegram *poTelegram)
{
	switch (poTelegram->getType())
	{
		case MESSAGE_DEFAULT : 
		{
			// TODO
			break;
		}
	}
}

/**
* @desc	Envoie un raport � une Team
*/
void Message::send(Report *poReport)
{
	String	sSender;
	int		nTeam;

	// Recuperation de l'agent qui a envoy� le message
	sSender = poReport->getSender();

	// R�cuperation de l'equipe de l'agent (le message est d�stin� a la Team)
	nTeam = GameManager::getSingletonPtr()->mAgentMap[sSender]->getTeamNumber();
	
	// Envoi du rapport � la team
	GameManager::getSingletonPtr()->m_TeamMap[nTeam]->getReport(poReport);
}


/**
* @desc	Envoie un ordre a un agent
*/
void Message::send(Command *poCommand)
{
	String	sAgent;

	// Recuperation de l'agent � qui on va envoyer la commande
	sAgent = poCommand->getReceiver();

	// Affecter une action a l'agent cibl�
	GameManager::getSingletonPtr()->mAgentMap[sAgent]->getCommand(poCommand);
}
