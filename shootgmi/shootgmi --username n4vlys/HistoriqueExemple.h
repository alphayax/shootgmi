#ifndef HISTORIQUEEXEMPLE_H
#define HISTORIQUEEXEMPLE_H

#include "Agent.h"
#include "Def.h"
#include <list>

class HistoriqueExemple
{
	public :
		HistoriqueExemple(Agent* pAgent);
		~HistoriqueExemple();

		void initEtats();
		void initNote();
		void modifierEtat(int pEtat, bool pValeur);
		void ecrireHisto();
		void ecrireHistoNote();

		std::ofstream mFichier;
		Agent* mAgent;
		std::list<double*>mHisto;
		std::list<double*>mHistoNote;
		std::list<double*>mScore;
		double* mInput;
		double* mOutput;
		double* mInputPrec;
		double* mCritereNote;
		std::list<double*>::iterator mScoredLineHisto;

		void afficher();
		void afficherDetails();
		void clear(); 
		//Scoring !
		void score();
		void majScore(double* pTabScore, double* pTabNote); //fonction de scoring
		int ScoreOutputAttack();
		int	ScoreOutputFlee();
		int ScoreOutputWander();
		int ScoreOutputBonus();
		int ScoreOutputSeek();

		int ScoringOutputAttack(double* pTabNote);
		int	ScoringOutputFlee(double* pTabNote);
		int ScoringOutputWander(double* pTabNote);
};
#endif