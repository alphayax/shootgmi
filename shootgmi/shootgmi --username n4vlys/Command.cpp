#include "Command.h"

/*
* @desc	Cr�e une commande simple
*/
Command::Command(String psReceiver, int pnCommand, int pnGoal)
{
	this->m_sReceiver	= psReceiver;
	this->m_nCommand	= pnCommand;
	this->m_nGoal		= pnGoal;
}

/*
* @desc	Cr�e une commande concernant un WayPoint (Vector3)
*/
Command::Command(Ogre::String psReceiver, int pnCommandType, int pnGoal, Ogre::Vector3 poTargetWP)
{
	this->m_sReceiver	= psReceiver;
	this->m_nCommand	= pnCommandType;
	this->m_nGoal		= pnGoal;
	this->m_oTargetWP	= poTargetWP;
	this->m_sTargetBot	= "";
}

/*
* @desc	Cr�e une commande concernant un Agent
*/
Command::Command(Ogre::String psReceiver, int pnCommandType, int pnGoal, Ogre::String psTargetBot)
{
	this->m_sReceiver	= psReceiver;
	this->m_nCommand	= pnCommandType;
	this->m_nGoal		= pnGoal;
	this->m_sTargetBot	= psTargetBot;
	this->m_oTargetWP	= Vector3::ZERO;
}

/*
* @desc	Retourne le type d'ordre donn�
*/
int Command::getCommandType()
{
	return this->m_nCommand;
}

/**
* @desc	Retourne le WayPoint cibl�
*/
Vector3 Command::getTargetWP()
{
	return this->m_oTargetWP;
}

/**
* @desc	Retourne l'identifiant du bot cibl�
*/
String Command::getTargetBot()
{
	return this->m_sTargetBot;
}

