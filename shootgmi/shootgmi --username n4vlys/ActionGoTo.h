///////////////////////////////////////////////////////////
//  ActionGoTo.h
//  Implementation of the Class ActionGoTo
//  Created on:      02-nov.-2007 18:52:36
///////////////////////////////////////////////////////////

#if !defined(ACTIONGOTO_H)
#define ACTIONGOTO_H

#include "Action.h"

class ActionGoTo : public Action
{

public:
	ActionGoTo();
	ActionGoTo(Agent* pAgent, const Vector3& pDestination);
	ActionGoTo(Agent* pAgent, Waypoint * pDestination);
	virtual ~ActionGoTo();

	virtual bool isEnd();
    virtual void init();
	virtual void reset();
	virtual void update(const FrameEvent& pEvt);

	virtual void setDestination(const Vector3& pDestination);
	virtual void setDestination(Waypoint * pDestination);
	virtual Waypoint* getWaypointDestination();

protected:
	Vector3 mDestination;
	Waypoint * mWaypointDestination;
	Real mDistance;
};
#endif // ACTIONGOTO_H
