#if !defined(ACTIONSEEKBONUS_H)
#define ACTIONSEEKBONUS_H
#include "Action.h"
#include "Carte.h"


class ActionGoByPath;

class ActionSeekBonus : public Action
{

public:
	ActionSeekBonus();
	ActionSeekBonus(Agent* pAgent, String pType="vie");
	virtual ~ActionSeekBonus();

    virtual bool isEnd();
    virtual void init();
	virtual void reset();
	virtual void update(const FrameEvent& pEvt);

protected:

	ActionGoByPath * mActionGoByPath;
	ActionGoTo * mActionGoTo;
	Waypoint* mBonusWaypoint;
	String mType;
};
#endif // ACTIONSEEK_H