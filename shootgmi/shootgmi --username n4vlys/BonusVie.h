#if !defined(BONUSVIE_H)
#define BONUSVIE_H
#include "Bonus.h"

using namespace Ogre;


class BonusVie : Bonus
{
	public:
		BonusVie(Vector3 pPos, int pPv);
		~BonusVie();

		void activer(Agent * pAgent);
		int mPv;

	protected :
		
};
#endif
