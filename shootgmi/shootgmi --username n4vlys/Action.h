///////////////////////////////////////////////////////////
//  Action.h
//  Implementation of the Class Action
//  Created on:      02-nov.-2007 18:52:35
///////////////////////////////////////////////////////////

#if !defined(ACTION_H)
#define ACTION_H

#include "ogre.h"
#include "Agent.h"
#include "Report.h"

using namespace Ogre;

class Action
{

public:
    Action();
	Action(Agent* pAgent);
	virtual ~Action();

	virtual bool isEnd()=0;
	virtual void init()=0;
	virtual void reset()=0;
    virtual void update(const FrameEvent& pEvt)=0;

	virtual bool isInit();
	virtual String getActionType();
	virtual Vector3 getLinear();
	virtual Real getAngular();

	Real getRatioVision( void ); // Renvoie le Ration de vision

protected:
    bool mIsInit;
	String mActionType;
	Real mAngular;
	Vector3 mLinear;
	Agent* mAgent;

	Real m_RatioVision; // Entre 0 et 2, 1 vision � 180�
};

#include "ActionGoByPath.h"
#include "ActionGoTo.h"
#include "ActionWander.h"
#include "ActionSeek.h"
#include "ActionFlee.h"
#include "ActionIdle.h"
#include "ActionDie.h"
#include "ActionAttack.h"
#include "ActionSentinel.h"
#include "ActionSeekBonus.h"

#endif // ACTION_H
