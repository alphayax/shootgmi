///////////////////////////////////////////////////////////
//  SceneApplication.cpp
//  Implementation of the Class SceneApplication
//  Created on:      02-nov.-2007 18:52:37
///////////////////////////////////////////////////////////




#include "SceneApplication.h"
#include "ExampleLoadingBar.h"
#include "Waypoint.h"
#include "Action.h"
#include "GenerateWaypoint.h"
#include "CollisionCallBack.h"
#include "BonusVie.h"
#include <iostream>


using namespace NxOgre;

SceneApplication::SceneApplication(){}

SceneApplication::~SceneApplication(){}




void SceneApplication::createScene(void)
{

	mSceneMgr->setShadowTechnique(SHADOWTYPE_STENCIL_MODULATIVE) ;

	std::cout << "Debug Message"<<std::endl;
	
	mWorldlm->getPhysXDriver()->getSDK()->getFoundationSDK().getRemoteDebugger()->connect("127.0.0.1");
	mWorldlm->getPhysXDriver()->setTimeModifier(2.0);
	NxOgre::ActorGroup * grAgent = mScene->createActorGroup("grAgent");
	NxOgre::ActorGroup * grProj = mScene->createActorGroup("grProj");
	NxOgre::ActorGroup * grMap = mScene->createActorGroup("grMap");
	NxOgre::ActorGroup * grBonus = mScene->createActorGroup("grBonus");

	NxOgre::ShapeGroup * sgrAgent = mScene->createShapeGroup("sgrAgent");
	NxOgre::ShapeGroup * sgrProj = mScene->createShapeGroup("sgrProj");
	NxOgre::ShapeGroup * sgrMap = mScene->createShapeGroup("sgrMap");
	NxOgre::ShapeGroup * sgrBonus = mScene->createShapeGroup("sgrBonus");
	
	sgrAgent->setCollisionResponse(sgrMap,ShapeGroup::CR_Collision);
	sgrProj->setCollisionResponse(sgrProj,ShapeGroup::CR_No_Collision);
	sgrAgent->setCollisionResponse(sgrProj,ShapeGroup::CR_Collision);

	sgrBonus->setCollisionResponse(sgrAgent,ShapeGroup::CR_Collision);


	grProj->setCallback<CollisionCallBack>(&collisionCallBack, &CollisionCallBack::onStartTouchProj, &CollisionCallBack::onEndTouch, &CollisionCallBack::onTouch);
	grProj->setCollisionCallback(grAgent, NX_NOTIFY_ALL, true);

	grBonus->setCallback<CollisionCallBack>(&collisionCallBack, &CollisionCallBack::onStartTouchBonus, &CollisionCallBack::onEndTouch, &CollisionCallBack::onTouch);
	grBonus->setCollisionCallback(grAgent, NX_NOTIFY_ALL, true);

	grAgent->setCallback<CollisionCallBack>(&collisionCallBack, &CollisionCallBack::onStartTouch, &CollisionCallBack::onEndTouch, &CollisionCallBack::onTouch);
	grAgent->setCollisionCallback(grMap, NX_NOTIFY_ALL, true);
	
/*	Body * body = mScene->createBody("myBody; Cube.mesh", new CubeShape(-100,100,-100),Vector3(0,0,0),"mass:10000");
	body->getEntity()->setMaterialName("Examples/BumpyMetal");
	Body * body2 = mScene->createBody("myBody2; Cube.mesh", new CubeShape(-100,200,-200),Vector3(0,0,0),"mass:10000");
	body2->getEntity()->setMaterialName("Examples/BumpyMetal");*/
	
	TriangleMeshShape * nil = new TriangleMeshShape("concorde.mesh", "mesh-scale: 40 40 40, Group: sgrMap");
	Body * cart = mScene->createBody("Carte; concorde.mesh", nil ,Vector3(0,-800,0),"Static: Yes, Group: grMap");
	Entity* mE= cart->getEntity();
	mE->setVisible(true);
	mE->MovableObject::setCastShadows(true);
	cart->getNode()->setScale(40.0f, 40.0f, 40.0f);


	       
//Plane plane( Vector3::UNIT_Y, 0 );

/*
       
Ogre::MeshManager::getSingleton().createPlane("ground2",
           ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane,
           1500,1500,20,20,true,1,5,5,Vector3::UNIT_Z);           


       Entity *ent = mSceneMgr->createEntity( "GroundEntity2", "ground2" );
       mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(ent);
	   ent->setMaterialName("Examples/Rockwall");
       ent->setCastShadows(false);*/

	mSceneMgr->setSkyBox(true, "Examples/CloudyNoonSkyBox");
	/*ColourValue fadeColour( 0.9, 0.9, 0.9 );
	mSceneMgr->setFog( FOG_EXP, fadeColour, 0.005 );*/
	
   
		
    // Create a light

/*	Ogre::Light* _light;
	_light = mSceneMgr->createLight("Light #1");
	_light->setType(Ogre::Light::LightTypes::LT_SPOTLIGHT);
	_light->setPosition(10,10,0);
	_light->setDirection(0,0,0);
	_light->setDiffuseColour(0,1,0);//Lumi�re blanche
	_light->setVisible(true);
	_light->setSpotlightRange(Degree(35), Degree(50));*/

    Light* myLight = mSceneMgr->createLight("Light0");
    myLight->setType(Light::LT_POINT);
    myLight->setPosition(1300, 500, 0);
    myLight->setDiffuseColour(1, 0, 0);
    myLight->setSpecularColour(1, 1, 1);
	//myLight->setCastShadows(true); 

    mSceneMgr->setAmbientLight(ColourValue(1, 1, 1));
	//mSceneMgr->setAmbientLight(ColourValue(0, 0, 0)) ; 

  // Create a point light
   /*     Light* l = mSceneMgr->createLight("MainLight");
        // Accept default settings: point light, white diffuse, just set position
        // Add light to the scene node
       SceneNode* rotNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
        rotNode->createChildSceneNode(Vector3(20,40,50))->attachObject(l);*/

    mCamera->setPosition( 0, 150.0f, 0 );
    mCamera->pitch( Degree(-30.0f) );
    mCamera->yaw( Degree(-15.0f) );

    Carte * carte=new Carte(mSceneMgr);
	mGameMgr->setCarte(carte);


	// Generation de la carte (fichier XML)
	bool bGenerateMap = false;

	if (bGenerateMap)
	{
		GenerateWaypoint * Gen = new GenerateWaypoint(cart->getEntity(),carte,mSceneMgr);
		mRoot->addFrameListener(Gen);
	}
	else
	{
		std::cout<<"ReadWaypoint"<<std::endl;
		carte->readWaypoint();
		std::cout<<"Waypoints charges"<<std::endl;


		
		//Creation Arme
		Arme pistol(PISTOL);
		Arme assault(ASSAULT);
		Arme shotgun(SHOTGUN);
		Arme snipe(SNIPE);
		Arme armesTab[]={pistol, assault, shotgun, snipe};


		// Cr�ation des �quipes
		Team* team1 = new Team(0, "Fred et Yann");
		team1->setBaseWP (GameManager::getSingleton().getCarte()->mWaypointList[5]);
		Team* team2 = new Team(1, "Fab et Yo");
		team2->setBaseWP (GameManager::getSingleton().getCarte()->mWaypointList[100]);
		//Team* team3 = new Team(2, "Slougi et Raoul");
		//team3->setBaseWP (GameManager::getSingleton().getCarte()->mWaypointList[300]);

		// Cr�ation des agents
		
		Agent*			oAgentCourrant;
		unsigned int	nNbTeams;

		nNbTeams = mGameMgr->m_TeamMap.size(); // Nombre d'�quipes cr�ees

		for(int j=0; j<nNbTeams; j++)
		{
			for(int i=0; i<4; i++)
			{
				oAgentCourrant	= mGameMgr->createAgent("robot.mesh",Vector3(200,300,-100));
				oAgentCourrant->mArme=armesTab[i%4];
				
				oAgentCourrant->TextArme->setCaption(oAgentCourrant->mArme.name);
				if(j==0)
				{
					oAgentCourrant->peur=1;
					oAgentCourrant->TextTeam->setColor(ColourValue(0,1,0.0));
				}
				else if(j==1){
					oAgentCourrant->roublard=1;
					oAgentCourrant->TextTeam->setColor(ColourValue(1,0,0.0));
				}
				else{
					oAgentCourrant->violent = 1;
					oAgentCourrant->TextTeam->setColor(ColourValue(0,0,1.0));
				}
				mGameMgr->m_TeamMap[j]->addTeamMember(oAgentCourrant->getName());
				oAgentCourrant->repop();
			}
		}
		
		team1->setDecisionState(false);	// Transforme la team 2 en �quipe de teub�s

		//mGameMgr->mRoot->addFrameListener(team1);
		mGameMgr->mRoot->addFrameListener(team2);
	}

	mGameMgr->placerBonus(15);




	//BonusVie *bv1= new BonusVie((carte->plusProche(Vector3(0,0,0)))->getVec(),20);


	
	/*
        std::ofstream fichier("deplacement.txt", std::ios::trunc);
        fichier << "Est sur  : " << StringConverter::toString(agent->getPosition().x) <<"//" <<StringConverter::toString(agent->getPosition().z)<< std::endl;
        fichier << "Va  : " << StringConverter::toString(carte->plusProche(agent->getPosition().x,agent->getPosition().z)->getX()) <<"//" <<StringConverter::toString(carte->plusProche(agent->getPosition().x,agent->getPosition().z)->getZ())<< std::endl;
    */


}

