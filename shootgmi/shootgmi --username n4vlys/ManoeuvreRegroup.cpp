#include "ManoeuvreRegroup.h"

ManoeuvreRegroup::ManoeuvreRegroup()
				: Manoeuvre() {}

ManoeuvreRegroup::ManoeuvreRegroup(unsigned int pnTeamNumber, Waypoint* poDest, bool bClearAgentAction)
				: Manoeuvre(pnTeamNumber, bClearAgentAction)
{
	this->m_sDestAgent	= "";
	this->m_v3DestWP	= poDest->getVec();

	// Recuperation des membres de la team
	mapAgentTeam membres = GameManager::getSingletonPtr()->m_TeamMap[this->m_nTeamNumber]->m_mTeamAgents;
	
	
	// Iterateur
	mapAgentTeam::iterator	it;	

	for (it = membres.begin(); it != membres.end(); ++it)
	{
		// Mise en �coute de nos agents
		this->addListenedAgent(it->first);
	}
}

ManoeuvreRegroup::ManoeuvreRegroup(unsigned int pnTeamNumber, String psAgent, bool bClearAgentAction)
				: Manoeuvre(pnTeamNumber, bClearAgentAction)
{
	this->m_sDestAgent	= psAgent;
	this->m_v3DestWP	= Vector3::ZERO;
}

// On enleve le gars arriv� de la liste des �cout�s
void ManoeuvreRegroup::getReportArrived(Report* poReport)
{
	this->removeListenedAgent(poReport->getSender());
}

void ManoeuvreRegroup::getReportMoveTo(Report* poReport)
{

}

void ManoeuvreRegroup::getReportSeenSone(Report* poReport)
{

}

void ManoeuvreRegroup::getReportDefault(Report* poReport)
{

}

void ManoeuvreRegroup::init()
{
	if (m_bClearActions)
		GameManager::getSingletonPtr()->m_TeamMap[m_nTeamNumber]->clearAgentAction();

	// Initialisation
	this->m_bIsInit	= true;

	// Iterateur
	vecIdAgents::iterator	it;	

	for (it = this->m_vListenedAgents.begin(); it != this->m_vListenedAgents.end(); ++it)
	{
		// Creation de la commande
		Command* maCommande = (this->m_sDestAgent == "")? new Command(*it, 1, 1, this->m_v3DestWP) : new Command(*it, 1, 1, this->m_sDestAgent);

		// Envoi de la commande
		Message::getSingleton()->send(maCommande);
	}

}