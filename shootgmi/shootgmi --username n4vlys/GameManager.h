///////////////////////////////////////////////////////////
//  GameManager.h
//  Implementation of the Class GameManager
//  Created on:      02-nov.-2007 18:52:37
///////////////////////////////////////////////////////////

#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

#include "Team.h"
#include "Agent.h"
#include "Waypoint.h"
#include "GameEntity.h"
#include "Carte.h"
#include "Bonus.h"
#include "Projectile.h"
#include "HistoriqueExemple.h"
#include "NxOgre.h"


#include <map>


using namespace Ogre;

class Team;
class Bonus;
class Carte;


class GameManager : public FrameListener
{

public:
	
	virtual ~GameManager();
	Agent *			createAgent(const String& mesh, const Vector3& pPosition=Vector3::ZERO, Real pOrientation=0);
	Projectile *	createProjectile(const String& mesh, const String& pPropri, const Vector3& pPosition=Vector3::ZERO, const Vector3& pVelo=Vector3::ZERO);
	void			createParticule(const Vector3&, String pType="etincelle");
	String			createBonus(const Vector3&, String pType, Bonus*);
	void			createObjet(const Vector3& pPosition, const String pType);

	int nbBonusActivated;

	void showBonus(String pName);
	void hideBonus(String pName);
	Waypoint * getBonusProche(Vector3 pVec);
	void removeProjectile(String pName);
	void placerBonus(int nb);

	SceneManager*		getSceneManager();
	std::list<Agent*>&	getAgentList();
	Carte*				getCarte();
	NxOgre::Scene*		getScene();

	void setCarte(Carte *pCarte);
	void setSceneMgr(SceneManager*);
	void setScene(NxOgre::Scene*);
	void setRoot(Root*);

	// Récuperation des singleton
	static GameManager& getSingleton();
	static GameManager* getSingletonPtr();

	// Membres (TODO : passer en privé)
	std::map<unsigned int	, Team*					>	m_TeamMap;
	std::map<String			, Agent*				>	mAgentMap;
	std::map<String			, Projectile*			>	mProjMap;
	std::map<String			, Bonus*				>	mBonusMap;
	std::map<String			, BillboardSet*			>	mBillMap;
	std::map<String			, Ogre::ParticleSystem*	>	mPartMap;
	std::map<String			, NxOgre::Body*			>	mObjMap;


	void selectAgent(String pName);
	void unselectAllAgent();
	void controlAgent(String pName);
	void uncontrolAgent(String pName);

	Timer	selectedAgent;
	Agent*	agentSelected;
	std::list<Agent*>	agentControled;
	Root*	mRoot;

	Timer mTimerPartie;
	Carte*			mCarte;
    SceneManager*	mSceneMgr;
	NxOgre::Scene*	mScene;

	bool frameStarted(const FrameEvent& pEvt);

	int mNbRound;
	void restartPartie();

protected:
	static GameManager*		mSingleton;	
    std::list<GameEntity*>	mGameEntityList;
	int						nbParticules;

	GameManager();

};


#endif
