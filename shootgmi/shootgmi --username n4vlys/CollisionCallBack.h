#include "SceneApplication.h"
using namespace NxOgre;

#if !defined(COLLISIONCALLBACK_H)
#define COLLISIONCALLBACK_H
#include "GameManager.h"
#include "HistoriqueExemple.h"


class CollisionCallBack
{ 
public :

	void onStartTouch(Actor *a, Actor *b) 
	{ 
			 /*Body* ba = static_cast<Body*>(a); 
			 Body* bb = static_cast<Body*>(b);

			 if(ba->getName()=="carte") 
			 {
				 bb->setLinearVelocity(Vector3::ZERO);
				 bb->addForce(Vector3(0, 1000, 0));
			 }
			 else if(bb->getName()=="carte") 
			 {
				 ba->setLinearVelocity(Vector3::ZERO);
				 ba->addForce(Vector3(0, 1000, 0));
			 }
			 else{
				 //ba->setLinearVelocity(Vector3::ZERO);
				// a->addForce(Vector3(0, 100000, 10000),NX_IMPULSE );
				 //bb->setLinearVelocity(Vector3::ZERO);
				 b->addForce(Vector3(0, 10000, -1000),NX_IMPULSE);
			 ba->getEntity()->setMaterialName("Examples/BumpyMetal"); 
			 bb->getEntity()->setMaterialName("Examples/BumpyMetal"); 
			 }*/
		if(a->getGroup()==a->getScene()->getActorGroup("grAgent"))
			GameManager::getSingleton().mAgentMap[a->getName()]->onContactWithMap=true;
		if(b->getGroup()==b->getScene()->getActorGroup("grAgent"))
			GameManager::getSingleton().mAgentMap[b->getName()]->onContactWithMap=true;

	} 


	void onStartTouchBonus(Actor *a, Actor *b) 
	{
		//std::cout<<"COLLISION"<<a->getName()<<"  "<<b->getName()<<std::endl;
		
		if(a->getGroup()==a->getScene()->getActorGroup("grBonus"))
		{
			if(GameManager::getSingleton().mAgentMap[b->getName()]->getFirstAction() == "ActionSeekBonus")
				GameManager::getSingleton().mBonusMap[a->getName()]->activer(GameManager::getSingleton().mAgentMap[b->getName()]);
		}else
		{
			if(GameManager::getSingleton().mAgentMap[a->getName()]->getFirstAction() == "ActionSeekBonus")
				GameManager::getSingleton().mBonusMap[b->getName()]->activer(GameManager::getSingleton().mAgentMap[a->getName()]);
		}
	}

	void onStartTouchProj(Actor *a, Actor *b) 
	{
		Agent* shooter;
		Agent* shooted;
		Projectile* projectile;
		bool touchProjAgent=false;

		if(a->getGroup()==a->getScene()->getActorGroup("grProj") && GameManager::getSingleton().mProjMap[a->getName()]->mProprio!=b->getName())
		{
			projectile=GameManager::getSingleton().mProjMap[a->getName()];
			shooter=GameManager::getSingleton().mAgentMap[projectile->mProprio];
			shooted=GameManager::getSingleton().mAgentMap[b->getName()];
			touchProjAgent=true;
		}
		else if(b->getGroup()==b->getScene()->getActorGroup("grProj") && GameManager::getSingleton().mProjMap[b->getName()]->mProprio!=a->getName())
		{
			projectile=GameManager::getSingleton().mProjMap[b->getName()];
			shooter=GameManager::getSingleton().mAgentMap[projectile->mProprio];
			shooted=GameManager::getSingleton().mAgentMap[a->getName()];
			touchProjAgent=true;
		}

		if(touchProjAgent)
		{
			GameManager::getSingleton().createParticule(projectile->getPosition());
			projectile->isAlive=false;	
			//GameManager::getSingleton().removeProjectile(projectile->getName());
			int pv=shooted->mEtat.pointsVie;
			if (shooter->getTeamNumber() != shooted->getTeamNumber() )
			{
				shooted->setDegats(shooter->mArme.degat);
				shooted->setCible(shooter);
				shooted->lookCible();
			}
			shooter->mHisto->mCritereNote[NOTE_DEGAT_FAIT]+=shooter->mArme.degat;
			shooted->mHisto->mCritereNote[NOTE_DEGAT_SUBIT]+=shooter->mArme.degat;

			if(shooted->mEtat.pointsVie <= 0 && pv*shooted->mEtat.pointsVie<=0 && pv!=0)
			{
				shooter->mHisto->mCritereNote[NOTE_ENNEMI_TUE]+=1;
				//shooter->mHisto->mCritereNote[NOTE_TEMPS_SURVIE]=-1;
				shooter->mHisto->mCritereNote[NOTE_TEMPS_ENNEMI_TUE]-=GameManager::getSingleton().mTimerPartie.getMilliseconds();
				//shooter->mHisto->ecrireHistoNote();

				shooted->mHisto->mCritereNote[NOTE_TEMPS_SURVIE] = GameManager::getSingleton().mTimerPartie.getMilliseconds();
				//shooted->mHisto->mCritereNote[NOTE_TEMPS_ENNEMI_TUE] = -1;
				shooted->mHisto->mCritereNote[NOTE_AGENT_MORT] = 1;
				//shooted->mHisto->ecrireHistoNote();
			}
		}
	}

	void onTouch(Actor *a, Actor *b) 
	{
		/*if(a->getGroup()==a->getScene()->getActorGroup("grAgent"))
			a->addForce(a->getScene()->getGravity()*(-1), NX_ACCELERATION);
		if(b->getGroup()==b->getScene()->getActorGroup("grAgent"))
			b->addForce(a->getScene()->getGravity()*(-1), NX_ACCELERATION);*/
		   /*Body* ba = static_cast<Body*>(a); 
			 Body* bb = static_cast<Body*>(b);
		  Body* bd = static_cast<Body*>(b); 
			 //bd->getEntity()->setMaterialName("Examples/BumpyMetal"); */


	} 

	void onEndTouch(Actor *a, Actor *b) 
	{ 
		 // Body* bd = static_cast<Body*>(b); 
			 //sbd->getEntity()->setMaterialName("Examples/BumpyMetal"); 
		/*if(a->getGroup()==a->getScene()->getActorGroup("grAgent"))
			a->raiseBodyFlag(NxBodyFlag::NX_BF_DISABLE_GRAVITY);
		if(b->getGroup()==b->getScene()->getActorGroup("grAgent"))
			b->raiseBodyFlag(NxBodyFlag::NX_BF_DISABLE_GRAVITY);*/

		if(a->getGroup()==a->getScene()->getActorGroup("grAgent"))
			GameManager::getSingleton().mAgentMap[a->getName()]->onContactWithMap=false;
		if(b->getGroup()==b->getScene()->getActorGroup("grAgent"))
			GameManager::getSingleton().mAgentMap[b->getName()]->onContactWithMap=false;

		
	} 

}collisionCallBack;

#endif