///////////////////////////////////////////////////////////
//  ActionFlee.cpp
//  Implementation of the Class ActionFlee
//  Created on:      02-nov.-2007 18:52:36
///////////////////////////////////////////////////////////

#include "ActionFlee.h"
#include "GameManager.h"


ActionFlee::ActionFlee()
	: Action(NULL), mAgentTarget(NULL)
{
    mActionType="ActionFlee";
	mActionGoByPath=NULL;
	mActionGoTo=NULL;
	mTargetLastWaypoint=NULL;
}

ActionFlee::ActionFlee(Agent* pAgent)
    : Action(pAgent), mAgentTarget(NULL)
{
    mActionType="ActionFlee";
	mActionGoByPath=NULL;
	mActionGoTo=NULL;
	mTargetLastWaypoint=NULL;
}

ActionFlee::ActionFlee(Agent* pAgent, Agent* pAgentTarget)
    : Action(pAgent), mAgentTarget(pAgentTarget)
{
    mActionType="ActionFlee";
	mActionGoByPath=NULL;
	mActionGoTo=NULL;
	mTargetLastWaypoint=NULL;
}


ActionFlee::~ActionFlee()
{
	delete mActionGoByPath;
	delete mActionGoTo;
}

void ActionFlee::init()
{
	reset();
	Vector3 target;
	if(mAgentTarget==NULL)
	{
		mTargetLastWaypoint=NULL;
		target=*(GameManager::getSingleton().m_TeamMap[mAgent->getTeamNumber()]->getBaseWP());
	}
	else 
	{
		mTargetLastWaypoint=mAgentTarget->getWaypointCurrent();
		target=mAgent->getPosition()+(mAgent->getPosition()-(Vector3)*mTargetLastWaypoint)*500;
	}
	target.y=mAgent->getPosition().y;
	mActionGoByPath=new ActionGoByPath(mAgent, target);
	mActionGoByPath->init();
	mActionGoTo=new ActionGoTo(mAgent, target);
	mActionGoTo->init();
	mLastNewPathTimer.reset();

    mIsInit=true;
}

void ActionFlee::reset()
{
	delete mActionGoByPath;
	mActionGoByPath=NULL;
	delete mActionGoTo;
	mActionGoTo=NULL;
    mIsInit=false;
}

bool ActionFlee::isEnd()
{
	return  false;
}


void ActionFlee::update(const FrameEvent& pEvt)
{
	/*if((mAgent->getPosition()-mAgentTarget->getPosition()).length()>150)
	{
		if((mActionGoByPath->mNbWalkedWaypoint>=2 && mAgentTarget->getWaypointCurrent()!=mTargetLastWaypoint ) || mActionGoByPath->isEnd())
		{
			mLastNewPathTimer.reset();
			mTargetLastWaypoint=mAgentTarget->getWaypointCurrent();
			Vector3 target=mAgent->getPosition()-(Vector3)*mTargetLastWaypoint;
			target.y=mAgent->getPosition().y;
			mActionGoByPath->setDestination(target);
			mActionGoByPath->popFront();
		}*/
		mActionGoByPath->update(pEvt);
		mLinear=mActionGoByPath->getLinear();
		mAngular=mActionGoByPath->getAngular();
	/*}
	else
	{
		Vector3 target=mAgent->getPosition()-mAgentTarget->getPosition();
		target.y=mAgent->getPosition().y;
		//if(mLastNewPathTimer.getMilliseconds()>5000) {mLastNewPathTimer.reset(); mActionGoTo->setDestination(target);}
		mActionGoTo->setDestination(target);
		mActionGoTo->update(pEvt);
		mLinear=mActionGoTo->getLinear();
		mAngular=mActionGoTo->getAngular();
	}*/
}

