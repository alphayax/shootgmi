///////////////////////////////////////////////////////////
//  Carte.h
//  Implementation of the Class Carte
//  Created on:      02-nov.-2007 18:52:37
///////////////////////////////////////////////////////////

#ifndef CARTE_H
#define CARTE_H

#include "Waypoint.h"
#include "Agent.h"
#include <deque>

class Agent;

typedef	std::vector<Waypoint*>	vecWP;

class Carte
{

public:
	Carte();
	Carte(SceneManager* pSM);
	virtual ~Carte();
    void init(Waypoint *);
   // void dijkstra(int n);
    void suite(Waypoint *a, Waypoint *b);
    void creerMaillage(Waypoint w1, Waypoint w2, Real pas);
    void getChemin(Agent *, int , int);
    //void getChemin(std::deque< Waypoint* >& pWalkList, int , int);
    Waypoint*	plusProche(const Vector3&);
    Entity*		placerObstacle(int,int,float,float,float,String); //x1,y1,long,larg,h

public:
    vecWP			mWaypointList;	// Liste des Waypoints
	std::ofstream	fichier;

public:
	Waypoint * plusProcheDe(Vector3 vector);

	void writeWaypoint();
	void readWaypoint();

	void aStar(std::deque< Waypoint* > &pWalkList, int pDepart, int pArrive);

	void generateCoverWP(void);	// G�n�re la matrice de visibilit� des Waypoints

protected:
	bool			**m_bVisibilityWP;	// Visibilit� des Waypoints les uns par rapport aux autres
	SceneManager	*mSceneMgr;			// Scene manager

};
#endif // CARTE_H
