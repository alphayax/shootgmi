///////////////////////////////////////////////////////////
//  ManoeuvreMoveToAll.h
//  D�rive de Manoeuvre
///////////////////////////////////////////////////////////

#ifndef MANOEUVRE_REGROUP_H
#define MANOEUVRE_REGROUP_H

#include "Manoeuvre.h"
#include "Waypoint.h"

class ManoeuvreRegroup : public Manoeuvre
{
public:
	ManoeuvreRegroup(void);
	ManoeuvreRegroup(unsigned int pnTeamNumber, Waypoint* oDest, bool bClearAgentAction = true);
	ManoeuvreRegroup(unsigned int pnTeamNumber, String psAgent,	 bool bClearAgentAction = true);

public:
	void	init ( void );	// Initialisation
	bool	isEnd( void );	// Retourne vrai si la manoeuvre est termin�e

protected:
	void	getReportArrived( Report * oReport );
	void	getReportMoveTo( Report * oReport );
	void	getReportSeenSone( Report * oReport );
	void	getReportDefault( Report * oReport );

protected:
	String	m_sDestAgent;	// Agent vers qui les membres du groupe vont aller
	Vector3	m_v3DestWP;		// WP vers lequel les membres du groupe vont aller
};

#endif
