///////////////////////////////////////////////////////////
//  D�rive de Manoeuvre
///////////////////////////////////////////////////////////

#ifndef MANOEUVRE_EXPLORE_H
#define MANOEUVRE_EXPLORE_H

#include "Manoeuvre.h"
#include "Waypoint.h"


class ManoeuvreExplore : public Manoeuvre
{
public:
	ManoeuvreExplore(void);
	ManoeuvreExplore(unsigned int pnTeamNumber, bool bClearAgentAction = true);
public:
	void	init ( void );	// Initialisation
	bool	isEnd( void );	// Retourne vrai si la manoeuvre est termin�e

protected:
	void	getReportArrived( Report * oReport );
	void	getReportMoveTo( Report * oReport );
	void	getReportDefault( Report * oReport );
	void	getReportSeenSone( Report * oReport );

protected:
	bool m_bCovered;
};

#endif
