#ifndef REPORT_H
#define REPORT_H

#include "ogre.h"
#include "Telegram.h"

using namespace Ogre;

//-------------------
// Types de Rapports
//-------------------
#define	REPORT_IDLE			0	// Je fais rien
#define REPORT_SEEN_SONE	1	// J'ai vu quelqu'un
#define REPORT_SEEN_STHING	2	// J'ai vu quelque chose
#define	REPORT_FIGHT		3	// Je combat quelqu'un
#define REPORT_KILLED		4	// J'ai tu� quelqu'un
#define	REPORT_MOVETO		5	// Je me d�place vers
#define	REPORT_ARRIVED		6	// Je suis arriv�
#define REPORT_ATTACK		8	// J'engage un ennemi
#define REPORT_ATTACKED		9	// Je suis attaqu�
#define REPORT_COVER		10	// Je suis a couvert



class Report : public Telegram
{
public:
	Report(String sSender, int nReport = REPORT_IDLE);		// Cr�e un raport IDLE par d�faut
	Report(String sSender, int nReport, String sTargetBot);	// Cr�e un rapport concernant un bot
	Report(String sSender, int nReport, Vector3 oTargetWP);	// Cr�e un rapport concernant un WP

public:
	int		getReportType(void);	// Retourne le type de rapport
	String	getTargetBot(void);		// Retourne le bot cibl�
	Vector3	getTargetWP(void);		// Retourne le WP cibl�

protected:
	int		m_nReport;		// Type de rapport
	String	m_sTargetBot;	// L'agent concern� par le message
	Vector3	m_oTargetWP;	// Un emplacement concern� par le message
};

#endif