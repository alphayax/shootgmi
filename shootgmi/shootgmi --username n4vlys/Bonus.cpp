#include "Bonus.h"

#include "GameManager.h"

Bonus::Bonus(Vector3 pPos, String pImage, int pTime)
{
	mName=GameManager::getSingleton().createBonus(pPos,pImage,this);
	mPosition=pPos;
	mImage=pImage;
	mTimeRepop=pTime;
	mTime.reset();
	show();
}

Bonus::~Bonus()
{}


void Bonus::activer(Agent *pAgent)
{}

void Bonus::hide()
{
	NxOgre::Scene* mScene = GameManager::getSingletonPtr()->getScene();
	if (mScene->getActor(mName) !=NULL) mScene->getActor(mName)->disable();
	GameManager::getSingleton().hideBonus(mName);
	mActif=false;
}

void Bonus::show()
{
	NxOgre::Scene* mScene = GameManager::getSingletonPtr()->getScene();
	//if (mScene->getActor(mName) !=NULL) mScene->getActor(mName)->
	mActif=true;
}