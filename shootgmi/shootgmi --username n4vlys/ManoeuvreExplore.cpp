#include "ManoeuvreExplore.h"

ManoeuvreExplore::ManoeuvreExplore() : Manoeuvre() {}

ManoeuvreExplore::ManoeuvreExplore(unsigned int pnTeamNumber, bool bClearAgentAction)
				: Manoeuvre(pnTeamNumber, bClearAgentAction)
{
	// Mise en �coute de l'agent qui explorera
	this->m_bCovered = false;
	this->addListenedAgent(GameManager::getSingletonPtr()->m_TeamMap[this->m_nTeamNumber]->m_mTeamAgents.begin()->first);
}

void ManoeuvreExplore::init()
{
	if (m_bClearActions)
		GameManager::getSingletonPtr()->m_TeamMap[m_nTeamNumber]->clearAgentAction();

	// Initialisation
	this->m_bIsInit	= true;

	// On commande a notre agent qui explorera d'explorer
	// Creation de la commande (Seek&Survive)
	Command* maCommande = new Command(this->m_vListenedAgents.front(), COMMAND_SEARCH, BUT_SURVIVRE);

	// Envoi de la commande
	Message::getSingleton()->send(maCommande);


	// On commande aux autres d'attendre tranquilement
	// Iterateur
	mapAgentTeam::iterator	it, itDebut, itFin;	
	itDebut = GameManager::getSingletonPtr()->m_TeamMap[this->m_nTeamNumber]->m_mTeamAgents.begin();
	itFin	= GameManager::getSingletonPtr()->m_TeamMap[this->m_nTeamNumber]->m_mTeamAgents.end();

	++itDebut;	// On saute le premier agent

	for (it = itDebut; it != itFin; it++)
	{
		// Creation de la commande
		Command* maCommande = new Command((*it).first, COMMAND_SENTINEL, BUT_SURVIVRE);

		// Envoi de la commande
		Message::getSingleton()->send(maCommande);
	}
}

void ManoeuvreExplore::getReportArrived(Report* poReport)
{
	if (this->m_bCovered)
	this->removeListenedAgent(poReport->getSender());
}

void ManoeuvreExplore::getReportMoveTo(Report* poReport)
{

}

void ManoeuvreExplore::getReportSeenSone(Report* poReport)
{
	// On commande a notre agent �cout� de se mettre au point de cover le plus proche
		// Recuperation du WP de la cible
		String		sAgent		= poReport->getTargetBot();
		Waypoint*	oTargetWP	= GameManager::getSingletonPtr()->mAgentMap[sAgent]->getWaypointCurrent();

		// R�cuperation du cover le plus proche de ce WP
		Waypoint*	oCoverWP	= oTargetWP->getNearestCoverWP(oTargetWP);

		// Commande de d�placement vers le WP cover
			// Creation de la commande (Hide&Survive)
			Command* maCommande = new Command(sAgent, COMMAND_MOVETO, BUT_SURVIVRE, oCoverWP->getVec());

			// Envoi de la commande
			Message::getSingleton()->send(maCommande);
			this->m_bCovered = true;
}

void ManoeuvreExplore::getReportDefault(Report* poReport)
{

}

