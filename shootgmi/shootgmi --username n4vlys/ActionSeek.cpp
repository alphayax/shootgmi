///////////////////////////////////////////////////////////
//  ActionSeek.cpp
//  Implementation of the Class ActionSeek
//  Created on:      02-nov.-2007 18:52:36
///////////////////////////////////////////////////////////

#include "ActionSeek.h"
#include "GameManager.h"


ActionSeek::ActionSeek() : Action()
{
    mActionType="ActionSeek";
	mActionGoByPath=NULL;
	mActionGoTo=NULL;
	mTargetLastWaypoint=NULL;
}


ActionSeek::ActionSeek(Agent* pAgent, Agent* pAgentTarget)
    : Action(pAgent), mAgentTarget(pAgentTarget)
{
    mActionType="ActionSeek";
	mActionGoByPath=NULL;
	mActionGoTo=NULL;
	mTargetLastWaypoint=NULL;
}


ActionSeek::~ActionSeek()
{
	delete mActionGoByPath;
	delete mActionGoTo;
}


void ActionSeek::init()
{
	reset();
	mAgent->lookCible();
	mTargetLastWaypoint=mAgentTarget->getWaypointCurrent();
	mActionGoByPath=new ActionGoByPath(mAgent, mTargetLastWaypoint);
	mActionGoByPath->init();
	mActionGoTo=new ActionGoTo(mAgent, mTargetLastWaypoint);
	mActionGoTo->init();
	mLastNewPathTimer.reset();

    mIsInit=true;
}

void ActionSeek::reset()
{
	delete mActionGoByPath;
	mActionGoByPath=NULL;
    mIsInit=false;
}

bool ActionSeek::isEnd()
{
	return  false;
}


void ActionSeek::update(const FrameEvent& pEvt)
{
	mAgent->lookCible();
	if((mAgent->getPosition()-mAgentTarget->getPosition()).length()>150)
	{
		if((mActionGoByPath->mNbWalkedWaypoint>=2 && mAgentTarget->getWaypointCurrent()!=mTargetLastWaypoint) || mActionGoByPath->isEnd())
		{
			mLastNewPathTimer.reset();
			mTargetLastWaypoint=mAgentTarget->getWaypointCurrent();
			mActionGoByPath->setDestination(mTargetLastWaypoint);
			mActionGoByPath->popFront();
		}
		mActionGoByPath->update(pEvt);
		mLinear=mActionGoByPath->getLinear();
		mAngular=mActionGoByPath->getAngular();
	}
	else
	{
		//if(mLastNewPathTimer.getMilliseconds()>3000) {mLastNewPathTimer.reset(); mActionGoTo->setDestination(mAgent->getPosition());}
		mActionGoTo->setDestination(mAgentTarget->getPosition());
		mActionGoTo->update(pEvt);
		mLinear=mActionGoTo->getLinear();
		mAngular=mActionGoTo->getAngular();
	}
}

