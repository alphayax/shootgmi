#if !defined(BONUS_H)
#define BONUS_H
#include "NxOgre.h"
#include <vector>
#include "Agent.h"

using namespace Ogre;

class Agent;

class Bonus
{
	public:
		Bonus(Vector3 pPos, String pImage, int pTime);
		~Bonus();
		virtual void activer(Agent * pAgent);
		void hide();
		void show();

		Vector3 mPosition;
		int mTimeRepop;
		String mImage;
		bool mActif;
		String mName;
		String mType;


	protected :
		Timer mTime;
};
#endif
