#ifndef FSMSTATE_H_INCLUDED
#define FSMSTATE_H_INCLUDED


class FSMState
{
	
private:
	unsigned m_MaxNumTransition;
	int* m_Input;       //Transitions
	int* m_OutputState; //Etats de sorties
	int  m_StateId;     //Id unique de l'etat

public:
	FSMState(int id, unsigned maxTransition);
	~FSMState(void);

	//accesseurs
	int getId(){return m_StateId;}


	/*
	 * ajoute une transition vers outputState
	 * quand survient inputTransition
	 */
	void addTransition(int inputTransition, int outputState);
	void deleteTransition(int outputState);

	/*
	 * retourne l'etat suivant pour la transition 
	 * inputTransition
	 */
	int getOuput(int inputTransition);


};

#endif
