#include "BonusVie.h"


BonusVie::BonusVie(Vector3 pPos, int pPv) : Bonus(pPos,"vie.jpg",30)
{
	mPv=pPv;
	mType="vie";
}


void BonusVie::activer(Agent *pAgent)
{
	if (mActif==true)
	{
		std::cout<<"Activate"<<std::endl;
		pAgent->setDegats(-mPv); // La feinte ! On met des d�gats n�gatifs -> gain de Pv
		hide();
	}
}