///////////////////////////////////////////////////////////
//  Agent.h
//  Implementation of the Class Agent
//  Created on:      02-nov.-2007 18:52:36
///////////////////////////////////////////////////////////

#ifndef AGENT_H
#define AGENT_H

#include "ogre.h"
#include "GameEntity.h"
#include "ActionControler.h"
#include "NxOgre.h"
#include "ObjectTextDisplay.h"
#include "MovableText.h"
#include "Waypoint.h"
#include "WaypointAStar.h"
#include "mlp.h"
#include "Arme.h"

#include "Message.h"	// Gestion de la messagerie
#include "Command.h"	// Structure t�l�gramme pour les commandes

using namespace Ogre;

class GameManager;
class Action;
class FiniteSM;
class FSMBot;
class HistoriqueExemple;

typedef std::vector< String >				vecIdAgents;		// Liste des agents

struct Etat{
    int pointsVie;
    int nbEnnemisEnVue;
};

class Agent : public GameEntity, public FrameListener
{
friend class GameManager;

public:
	virtual ~Agent();

	bool frameStarted(const FrameEvent& pEvt);
    void update(const FrameEvent& pEvt, Action* pAction);
    void addInFirstAction(Action* pAction);
	void addInLastAction(Action* pAction);
	void delFirstAction();
	void clearActionList();
    void setAnimationState(const String& pAnim);
    Real lookAhead();
	void lookCible();

	Real	getOrientation();
	Vector3 getPosition();
	Vector3 getPositionTete();
	Real	getRotation();
	Vector3 getVelocity();

	void setOrientation(Real pAngle);
	void setPosition(Vector3 pPosition);
	void setRotation(Real pRota);
	void setVelocity(Vector3 pVelocity);

	Real getMaxAcceleration();
	void setMaxAcceleration(Real pMaxAcc);
	Real getMinDeceleration();
	void setMinDeceleration(Real pMinDece);
	Real getRadius();
	void setRadius(Real pRad);
	Real getMaxSpeed();
	void setMaxSpeed(Real pMaxSpeed);
	String getFirstAction();
	String getName();

	String LastAction;

	std::vector<Agent*> mAttaquants; //Agents en train de nous shooter
	void ajouterAttaquant(Agent *);
	void enleverAttaquant(Agent *);
	bool isShooting;
	bool isAttacked();
	void repop(); //fait repop l'agent quand il meurt

	
	Waypoint* waypointPlusProche();
	
	void	setCible(Agent *);
	Agent*	getCible();
	bool	vision(Agent * pCible);	//retourne true si la cible est vue
	void	scanEnnemis();			//parcourt la liste des ennemis pour determiner lequel sera la cible
	void	shoot();				//determine si le tir sur la cible r�ussit(tirage > seuil) ou non
	float	seuilShoot();
	bool	tropProche();
	bool	tropLoin();
	void	aStar(std::deque< Waypoint* > &pWalkList, int pDepart, int pArrive);
	bool	waypointVisible(Waypoint * pWp);	//retourne true si le waypoint est visible
	Waypoint*		getCouverture(int prof);	//renvoie le waypoint le moins expos� aux agents du vector attaquant � "prof" Waypoint maximum de mWaypointCurrent
	Waypoint*		getWaypointCurrent();
	void			setWaypointCurrent(Waypoint* pWaypoint);
	NxOgre::Body*	getBody();

	void setNbPointsDeVie(int pPv);
	void setDegats(int pPv);
	

	GameManager*	getGameManager();
	Entity*			getEntity();
	Etat		mEtat;
	Arme		mArme;

	bool onContactWithMap;
	ObjectTextDisplay * mText;

	ActionControler				mActionControler;
	std::vector<WaypointAStar*> mWaypointList;
	AnimationState*				mAnimationState;

	FSMBot *m_FSM;
	HistoriqueExemple *mHisto; // Creation d'un historique pour le bot
	//Creation MLP
	int *layers1;
	MultiLayerPerceptron *mlp1;
	void apprendre();
	void decide();
	Timer mTimeFlee;

	SceneNode*	modelNode;
	String		mMesh;
	Agent*		mCible;
	bool		mFurtif;
	bool		mIsSelected;
	bool		mIsControled;
	int			peur;
	int			violent;
	int			roublard;

	//TEXT
	MovableText* TextReport;	// Affiche les raports envoy�s
	MovableText* TextVie;		// Affiche la vie
	MovableText* TextFrag;		// Affiche le nombre de frags
	MovableText* TextState;		// Affiche l'action courrante
	MovableText* TextTeam;		// Affiche l'�quipe de l'agent
	MovableText* TextArme;		// Affiche l'arme de l'agent

protected:

	String mName;
	
	NxOgre::Body*	mBody;
	Entity*			mEntity;
	GameManager*	mGameManager;
	NxOgre::Scene*	mScene;

	std::ofstream file;

	Real mOrientation;
	Real mRotation;
	Real mMaxAcceleration;
    Real mMinDeceleration;
    Real mMaxSpeed;
    Real mRadius;

	Waypoint * mWaypointCurrent;

	
	bool isInSightCone(Real nAngle); // Determine si l'angle par rapport � l'agent cible est dans le champs de vision.
	

//----------- DevSquad -- debut
protected:
	unsigned int m_nTeamNumber;		// Equipe de l'agent
	bool		 m_bdecideState;	// Etat du comportement de d�cision

public:
	void			setTeamNumber(unsigned int);	// D�finit le numero de l'equipe de l'agent
	unsigned int	getTeamNumber(void);			// Retourne le num�ro de l'�quipe de l'agent
	bool			isAlive(void);					// Retourne vrai si l'agent a ses points de vie > 0
	void			setDecideState ( bool );		// True = decide actif, False = decide inactif

	void			getCommand(Command* oCommand);	// Recupere et execute un ordre donn�

//----------- DevSquad -- fin


	
protected :
	Agent(const String& pName, const String& pMesh);
};
#endif // AGENT_H
