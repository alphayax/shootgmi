///////////////////////////////////////////////////////////
//  OgreApplication.h
//  Implementation of the Class OgreApplication
//  Created on:      02-nov.-2007 18:52:37
///////////////////////////////////////////////////////////

#if !defined(OGREAPPLICATION_H)
#define OGREAPPLICATION_H
#include "InteractListener.h"
#include "GameManager.h"
#include <CEGUI/CEGUISystem.h>
#include <CEGUI/CEGUISchemeManager.h>
#include <OgreCEGUIRenderer.h>
#include "NxOgre.h"

using namespace Ogre;

class OgreApplication
{

public:
	OgreApplication();
	virtual ~OgreApplication();
	virtual void go(void);

protected:
	Camera* mCamera;
	InteractListener* mInteractListener;
	GameManager* mGameMgr;
	Ogre::String mResourcePath;
	Root* mRoot;
	CEGUI::OgreCEGUIRenderer *mGUIRenderer;
	CEGUI::System *mGUISystem;
	SceneManager* mSceneMgr;
	RenderWindow* mWindow;
	NxOgre::World * mWorldlm;
	NxOgre::Scene * mScene;

	virtual bool configure(void);
	virtual void createCamera(void);
	virtual void createFrameListener(void);
	virtual void createGameManager(void);
	virtual void createScene(void)=0;
	virtual void createSceneManager(void);
	virtual void createViewports(void);
	virtual void loadResources(void);
	virtual bool setup(void);
	virtual void setupResources(void);

};
#endif // OGREAPPLICATION
