///////////////////////////////////////////////////////////
//  Team.h
//  Impl�mentation des squads Tactics
///////////////////////////////////////////////////////////

#ifndef TEAM_H
#define TEAM_H

#include "Report.h"
#include "GameManager.h"
#include "Team_AgentsInSight.h"
#include "ManoeuvreControler.h"
#include "Manoeuvre.h"
#include "Waypoint.h"


class GameManager;
class Manoeuvre;
class ManoeuvreControler;
class ManoeuvreRegroup;
class ManoeuvreGoVia;
class Team_AgentsInSight;

typedef std::vector< String >				vecIdAgents;		// Liste des agents
typedef std::vector< Vector3 >				vecStrategicWP;		// Liste des WayPoints Strat�giques d�t�ct�s par l'equipe
//typedef stdext::hash_compare< const char* >	hcmpChar;			// Fonction de hashage pour des char*
//typedef stdext::hash_map< const char* , vecIdAgents			, hcmpChar >	mapAgentTeam;		// Map des agents de l'equipe
typedef stdext::hash_map< const String , vecIdAgents		 >	mapAgentTeam;		// Map des agents de l'equipe
typedef stdext::hash_map< const String , Team_AgentsInSight	 >	mapAgentInSight;	// Map d'Agents d�tect�s


class Team : public FrameListener
{
public :
	Team(unsigned int nTeamNumber, String sTeamName);
	~Team();

//protected:
	unsigned int		m_nTeamNumber;			// Numero de l'equipe
	String				m_sTeamName;			// Nom de l'�quipe
	mapAgentTeam		m_mTeamAgents;			// Membres de l'equipe
	mapAgentInSight		m_mTeamAgentsInSight;	// Agents vus par l'equipe et informations relatives
	vecStrategicWP		m_vTeamStrategicsWP;	// Liste des WP strategiques d�t�ct�s par l'equipe

protected:
	ManoeuvreControler*	m_oManoeuvreControler;	// Gestionnaire des manoeuvres
	Waypoint*			m_oBaseWP;				// Waypoint de base de l'�quipe
	bool				m_bManoeuvreIsLaunched;	// D�finit si la Manoeuvre a �t� execut�

public :
	String	getTeamName( void );					// Retourne le nom de l'equipe
	void	addTeamMember( String );				// Ajoute un Agent a l'�quipe
	void	getReport( Report * );					// Recupere un rapport envoy� par un agent de l'equipe
	void	addStrategicWP(Vector3 vStrategicWP);	// Rajoute le WP pass� en parametre � la liste des WP strategiques d�t�ct�s
	bool	isAgentInSight( String sAgent );		// Permet de savoir si un agent donn� est vu par l'equipe
	int		getAliveMembersCount( void );			// Retourne le nombre de membres de l'�quipe toujours en vie
	int		getMembersCount( void );				// Retourne le nombre de membres de l'�quipe
	int		getTeamLifeCount( void );				// Retourne le % de vie de la team (par rapport � tous les membres)
	void	clearAgentAction( void );				// Vide la liste d'action des membres de l'�quipe
	void	setDecisionState( bool );				// Pour tous les agents de l'�quipe. True = d�cision activ�e - False = d�cision desactiv�e.

	void	DisplayReport(Report* oReport, String sReport);	// Affiche les reports au dessus des agents

	void	addAgentInSight(String sMember, String sTarget);	// Rajoute l'agent pass� en parametre � la liste des agents visible pour l'equipe
	void	remMemberFromAIS(String sMember, String sTarget);	// Retire l'agent pass� en parametre � la liste des agents visible pour l'equipe
	void	addTargetToAgent(String sMember, String sTarget);	// Rejoute la cible a la liste des cible du membre de l'equipe
	void	remTargetToAgent(String sMember, String sTarget);	// Retire la cible a la liste des cible du membre de l'equipe

	// Gestion de la base
	Waypoint*	getBaseWP( void );					// Retourne le Waypoint de le base de l'�quipe
	void	setBaseWP( Waypoint* );					// D�fini le Waypoint de le base de l'�quipe
	void	initBaseTeamPosition( void );			// Place les membres de l'equipe autour d'un WP de base donn�

	// Gestion des manoeuvres
	Manoeuvre*	getCurrentManoeuvre( void );		// Retourne la manoeuvre courrante
	void	addFrontManoeuvre( Manoeuvre* );		// Rajoute une manoeuvre
	void	clearTeamManoeuvres( void );			// Supprime toutes les manoeuvres en cours
	bool	hasManoeuvre( void );					// Retourne vrai si il y a des manoeuvres a executer

public:
    bool	frameStarted(const FrameEvent& pEvt);
    void	update(const FrameEvent& pEvt);

protected:
	void	ManageMC(void);										// Gere le manoeuvre Controler
};

#endif