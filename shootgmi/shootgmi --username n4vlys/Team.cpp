#include "Team.h"

/**
* @desc	Cr�e une �quipe. (Identifiant et Nom)
*/
Team::Team(unsigned int pnTeamNumber, String psTeamName)
{
	this->m_nTeamNumber				= pnTeamNumber;
	this->m_sTeamName				= psTeamName;
	this->m_bManoeuvreIsLaunched	= false;
	this->m_oManoeuvreControler		= new ManoeuvreControler();
	
	GameManager::getSingletonPtr()->m_TeamMap[pnTeamNumber] = this;
	this->setBaseWP(NULL);

}

Team::~Team()
{
	delete this->m_oManoeuvreControler;
}

/**
* @desc	Rajoute un agent en temps que membre de l'equipe
*/
void Team::addTeamMember(Ogre::String psAgent)
{
	// On initialise le vecteur de target de la cible
	vecIdAgents	vTargets;

	// On rajoute l'identifiant de l'agent dans notre vecteur d'Equipe
	this->m_mTeamAgents[psAgent] = vTargets;

	// On affecte le num�ro de l'�quipe � l'agent
	GameManager::getSingletonPtr()->mAgentMap[psAgent]->setTeamNumber(this->m_nTeamNumber);	
}


/**
* @desc	Retourne le nom de l'�quipe
*/
String Team::getTeamName()
{
	return this->m_sTeamName;
}


/**
* @desc	Traite le report envoy� par un agent, et envoie les information � l'equipe
*/
void Team::getReport( Report* poReport )
{
	// TODO : Envoyer les rapports au perceptron

	// Reports prioritaires - ind�pendament de la manoeuvre
	switch (poReport->getReportType())
	{
		case REPORT_IDLE		:	this->DisplayReport(poReport, "Report : Sentinel"); break;
		case REPORT_SEEN_SONE	:	this->DisplayReport(poReport, "Report : Seen");break;
		case REPORT_SEEN_STHING	:	break;
		case REPORT_FIGHT		:	break;
		case REPORT_KILLED		:	break;
		case REPORT_MOVETO		:	this->DisplayReport(poReport, "Report : Move To")
			;break;
		case REPORT_ARRIVED		:	this->DisplayReport(poReport, "Report : Arrived");
			break;

		default					:	break;
	}

	// Envoi des rapports aux manoeuvres
	if (!this->m_oManoeuvreControler->isEmpty())
		this->getCurrentManoeuvre()->getReport( poReport );
}


/**
* @desc Rajoute l'agent pass� en parametre � la liste des agents visible pour l'equipe
*/
void Team::addAgentInSight(Ogre::String psMember, Ogre::String psAgentInSight)
{
	if ( this->isAgentInSight( psAgentInSight ))
	{
		// On rajoute les nouvelles donn�es a l'agent pr�sent
		this->m_mTeamAgentsInSight[psMember.c_str()].updateTarget( psMember, psAgentInSight);
	}
	else
	{
		// On rajoute les donn�es concernant ce nouvel agent d�couvert
		// R�cup�ration des donn�es concernant l'agent
		Team_AgentsInSight oAgent(psMember, psAgentInSight);

		// Insertion dans le tableau
		this->m_mTeamAgentsInSight[psAgentInSight.c_str()] = oAgent;
	}
}

/**
* @desc Rajoute une cible a la liste des cible d'un Membre de l'equipe
*/
void Team::addTargetToAgent(Ogre::String psMember, Ogre::String psTarget)
{
	vecIdAgents VIDA = this->m_mTeamAgents[psMember.c_str()];

	vecIdAgents::iterator it;
	bool test=false;

		for (it=VIDA.begin(); it!=VIDA.end(); it++)
		{
			if (psTarget == *it) {test=true;}
		}

	if (!test)
		this->m_mTeamAgents[psMember.c_str()].push_back(psTarget);
}

/**
* @desc Retire une cible a la liste des cible d'un Membre de l'equipe
*/
void Team::remTargetToAgent(Ogre::String psMember, Ogre::String psTarget)
{
	vecIdAgents VIDA = this->m_mTeamAgents[psMember.c_str()];

	vecIdAgents::iterator it;

		for (it=VIDA.begin(); it!=VIDA.end(); it++)
		{
			if (psTarget == *it) {VIDA.erase(it); return;}
		}
}


/*
* @Desc Permet de savoir si l'agent donn� est vu par l'equipe
*/
bool Team::isAgentInSight(Ogre::String psAgent)
{
	// Si l'agent cible est pr�sent dans le tableau
	if ( this->m_mTeamAgentsInSight.find(psAgent.c_str()) != this->m_mTeamAgentsInSight.end() )
		return true;

	return false;
}

/**
* @Desc Rajoute un waypoint strategique a la liste des waypoints strat�giques d�t�ct�s par l'equipe
*/
void Team::addStrategicWP(Ogre::Vector3 pvStrategicWP)
{
	this->m_vTeamStrategicsWP.push_back(pvStrategicWP);
	
	
}

/**
* @Desc	Retourne le nombre de membres de l'�quipe toujours en vie
*/
int Team::getAliveMembersCount()
{
	// Initialisations
	mapAgentTeam::iterator	it;						// Iterateur
	unsigned int			nNbAliveMembers = 0;	// Compteur de membres en vie
	
	for (it = this->m_mTeamAgents.begin(); it != this->m_mTeamAgents.end(); ++it)
	{
		if (GameManager::getSingletonPtr()->mAgentMap[it->first]->isAlive())
			nNbAliveMembers++;
	}

	return nNbAliveMembers;
}

/**
* @Desc Retourne le % de vie de la team (par rapport � tous les membres)
*/
int Team::getTeamLifeCount()
{
	// Initialisations
	mapAgentTeam::iterator	it;						// Iterateur
	int						TeamLife = 0;			// Points de vie team
	int						TeamLifePerCent = 0;	// Points de vie team en %

	for (it = this->m_mTeamAgents.begin(); it != this->m_mTeamAgents.end(); ++it)
	{
		TeamLife += GameManager::getSingletonPtr()->mAgentMap[it->first]->mEtat.pointsVie;
	}
	
	TeamLifePerCent = (TeamLife / this->getMembersCount());
	return TeamLifePerCent;
}

/**
* @Desc	Retourne le nombre de membres de l'�quipe
*/
int Team::getMembersCount()
{
	return (int) this->m_mTeamAgents.size();
}

/**
* @desc Retourne la manoeuvre courrante
*/
Manoeuvre*	Team::getCurrentManoeuvre()
{
	return this->m_oManoeuvreControler->getFrontManoeuvre();
}

/**
* @desc	Vide la liste d'action de tous les membres de l'�quipe
*/
void Team::clearAgentAction()
{

	if (GameManager::getSingletonPtr()->mAgentMap.size() > 0)
	{
		// It�rateur sur la liste des agents de l'�quipe
		mapAgentTeam::iterator	it;
		
		for (it = this->m_mTeamAgents.begin(); it != this->m_mTeamAgents.end(); ++it)
		{
			//if (GameManager::getSingletonPtr()->mAgentMap[it->first]->mActionControler.getDeque() != NULL)
			//if (!GameManager::getSingletonPtr()->mAgentMap[it->first]->mActionControler.empty())
				GameManager::getSingletonPtr()->mAgentMap[it->first]->mActionControler.clear();
				
		}
	}

}

bool Team::frameStarted(const Ogre::FrameEvent& pEvt)
{
	
	// Si aucune manoeuvre n'est d�finie, on en met une par d�faut.	
	if (!this->m_bManoeuvreIsLaunched && this->m_oManoeuvreControler->isEmpty())
	{
		// TEST - Manoeuvre Explore
	/*	ManoeuvreExplore* ME = new ManoeuvreExplore(this->m_nTeamNumber);

		mapAgentTeam::iterator it;

		for (it = this->m_mTeamAgents.begin(); it != this->m_mTeamAgents.end(); it++)
		{
			ME->removeListenedAgent(it->first);
		}

		ME->addListenedAgent(this->m_mTeamAgents.begin()->first);

		this->addFrontManoeuvre(ME);
		*/


		
		// TEST - Manoeuvre Explore Attack
		ManoeuvreExploreAttack* MEA = new ManoeuvreExploreAttack(this->m_nTeamNumber);
		this->addFrontManoeuvre(MEA);
	
		/*
		// TEST - Manoeuvre Go Via
		//ManoeuvreRegroup* MMAT = new ManoeuvreRegroup(this->m_nTeamNumber, GameManager::getSingletonPtr()->getCarte()->mWaypointList[100]);
		//ManoeuvreRegroup* MMAT = new ManoeuvreRegroup(this->m_nTeamNumber, "Agent6");
		std::vector<Waypoint*> monVecWP;

		monVecWP.push_back(GameManager::getSingletonPtr()->getCarte()->mWaypointList[100]);
		monVecWP.push_back(GameManager::getSingletonPtr()->getCarte()->mWaypointList[20]);
		monVecWP.push_back(GameManager::getSingletonPtr()->getCarte()->mWaypointList[100]);
		monVecWP.push_back(GameManager::getSingletonPtr()->getCarte()->mWaypointList[10]);

		ManoeuvreGoVia* MGV = new ManoeuvreGoVia(this->m_nTeamNumber, monVecWP);

		this->addFrontManoeuvre(MGV);
		*/

		this->m_bManoeuvreIsLaunched = true;

	}
	
	this->ManageMC();

	return true;
}

void Team::update(const Ogre::FrameEvent& pEvt)
{
}

void Team::addFrontManoeuvre( Manoeuvre* poManoeuvre )
{
	this->m_oManoeuvreControler->addFrontManoeuvre( poManoeuvre );
}

void Team::ManageMC(void)
{
	// Si le Manoeuvre Controler n'est pas vide
	if (!this->m_oManoeuvreControler->isEmpty())
	{
		// Si la manoeuvre n'est pas initialis�e, on l'initialise
		if (!this->m_oManoeuvreControler->getFrontManoeuvre()->isInit())
		{
			this->m_oManoeuvreControler->getFrontManoeuvre()->init();
			this->setDecisionState(false);
		}

		// Si la manoeuvre est termin�e, on la d�pile
		if (this->m_oManoeuvreControler->getFrontManoeuvre()->isEnd())
		{
			this->m_oManoeuvreControler->remFrontManoeuvre();
			this->setDecisionState(true);
		}
	}
/*
	else
	{
		if(!this->m_oManoeuvreControler->getFrontManoeuvre()->m_vListenedAgents.empty())
			this->m_oManoeuvreControler->getFrontManoeuvre()->m_vListenedAgents.clear();
	}
*/
}


void Team::clearTeamManoeuvres(void)
{
	this->m_oManoeuvreControler->clear();
}

bool Team::hasManoeuvre(void)
{
	return !this->m_oManoeuvreControler->isEmpty();
}

void Team::setBaseWP(Waypoint* poBaseWP)
{
	this->m_oBaseWP = poBaseWP;
}


Waypoint* Team::getBaseWP(void)
{
	return this->m_oBaseWP;
}

void Team::initBaseTeamPosition()
{
	// Recuperation de la liste des voisins du WP de base
	std::vector< Waypoint* > vBaseVoisins = this->m_oBaseWP->mVoisin;

	// Initialisations
	mapAgentTeam::iterator	itMembres;
	unsigned int			iVoisins	= vBaseVoisins.size() - 1;

	for (itMembres = this->m_mTeamAgents.begin(); itMembres != this->m_mTeamAgents.end(); ++itMembres)
	{
		GameManager::getSingletonPtr()->mAgentMap[itMembres->first]->setPosition(*vBaseVoisins[iVoisins]);
		GameManager::getSingletonPtr()->mAgentMap[itMembres->first]->setWaypointCurrent(vBaseVoisins[iVoisins]);

		if (iVoisins == 0)
		{
			vBaseVoisins = vBaseVoisins[iVoisins]->mVoisin;
			iVoisins = vBaseVoisins.size() - 1;
		}
		else		
			iVoisins--;
	}
}

void Team::setDecisionState(bool pbstate)
{
	if (GameManager::getSingletonPtr()->mAgentMap.size() > 0)
	{
		// It�rateur sur la liste des agents de l'�quipe
		mapAgentTeam::iterator	it;
		
		for (it = this->m_mTeamAgents.begin(); it != this->m_mTeamAgents.end(); it++)
		{
				GameManager::getSingletonPtr()->mAgentMap[it->first]->setDecideState(pbstate);
		}
	}
}

void Team::remMemberFromAIS(Ogre::String psMember, Ogre::String psAgentInSight)
{
	if ( this->isAgentInSight( psAgentInSight ))
	{
		//On supprime le membre de l'�quipe qui le voit
		this->m_mTeamAgentsInSight[psAgentInSight].remMemberViewing(psMember);
	}
}


void Team::DisplayReport(Report* poReport, String psReport)
{
	// Affichage des reports envoy�s par l'agent
	if (poReport->getTargetBot() != "")
	{GameManager::getSingletonPtr()->mAgentMap[poReport->getSender()]->TextReport->setCaption(psReport+" : "+poReport->getTargetBot());}
	else
	{GameManager::getSingletonPtr()->mAgentMap[poReport->getSender()]->TextReport->setCaption(psReport);}
	
}