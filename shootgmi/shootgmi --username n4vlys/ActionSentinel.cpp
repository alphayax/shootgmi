#include "ActionSentinel.h"

ActionSentinel::ActionSentinel() : Action()
{
    this->mActionType	= "ActionSentinel";
	this->mIsInit		= false;
	this->mIsStop		= false;
	this->m_RatioVision	= 1.2;
}


ActionSentinel::ActionSentinel(Agent* pAgent)
    : Action(pAgent)
{
    this->mActionType	= "ActionSentinel";
    this->mIsInit		= false;
	this->mIsStop		= false;
	this->m_RatioVision = 1.2;
}

ActionSentinel::~ActionSentinel()
{
}


void ActionSentinel::init()
{
    this->mIsInit = true;

	this->mAgent->mAnimationState->setTimePosition(0);
	this->mAgent->setAnimationState("Idle");
	

	// Envoi un rapport pour signifiquer qu'on commence un déplacement
	Report* oReport = new Report(this->mAgent->getName(), REPORT_IDLE);
	Message::getSingleton()->send(oReport);
}

void ActionSentinel::reset()
{
    this->mIsInit = false;
}

bool ActionSentinel::isEnd()
{
	return false; // ???
}

void ActionSentinel::update(const FrameEvent& pEvt)
{
	this->mLinear=Vector3::ZERO;
	this->mAngular=0;

	if(mAgent->onContactWithMap)
	{
		this->mAgent->setVelocity(Vector3::ZERO);
		this->mAgent->setRotation(0);
	}
}
