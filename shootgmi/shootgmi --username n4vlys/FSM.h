#ifndef FSM_H_INCLUDED
#define FSM_H_INCLUDED


#include "Def.h"
#include "Action.h"

//typedef FSMState State;

class FSM
{
protected:
	Timer	  mTime;
	Map_State m_States;       //Map des etats
	int       m_CurrentState; //Etat courant de la machine
	Agent *m_Agent;
public:
	FSM(int DefaultState);
	~FSM(void);


	//accesseur
	int  getCurrentState()          {return m_CurrentState;}
	void setCurrentState(int state) {m_CurrentState = state;}


	FSMState* getState(int stateId);

	void addState(FSMState* state);
	void deleteState(int stateId);

	int stateTransition(int inputTransition);

	virtual void decide()=0;


};

#endif