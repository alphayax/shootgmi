///////////////////////////////////////////////////////////
//  OgreApplication.cpp
//  Implementation of the Class OgreApplication
//  Created on:      02-nov.-2007 18:52:37
///////////////////////////////////////////////////////////

#include "Ogre.h"
#include "OgreConfigFile.h"
#include "ExampleFrameListener.h"
#include "OgreApplication.h"

using namespace Ogre;

/// Standard constructor
OgreApplication::OgreApplication()
{
    mInteractListener = 0;
    mRoot = 0;
}

/// Standard destructor
OgreApplication::~OgreApplication()
{
    if (mInteractListener)
        delete mInteractListener;
    if (mRoot)
        delete mRoot;
}

/// Start the example
void OgreApplication::go(void)
{
    if (!setup())
        return;

    mRoot->startRendering();
}


// These internal methods package up the stages in the startup process
/** Sets up the application - returns false if the user chooses to abandon configuration. */
bool OgreApplication::setup(void)
{

    String pluginsPath;
    // only use plugins.cfg if not static
#ifndef OGRE_STATIC_LIB
    pluginsPath = mResourcePath + "plugins.cfg";
#endif

    mRoot = new Root(pluginsPath,mResourcePath + "ogre.cfg", mResourcePath + "Ogre.log");

    setupResources();

    bool carryOn = configure();
    if (!carryOn) return false;

    createSceneManager();
    createGameManager();
    createCamera();
    createViewports();
    loadResources();
    createFrameListener();
    createScene();

    return true;

}

/** Configures the application - returns false if the user chooses to abandon configuration. */
bool OgreApplication::configure(void)
{
    // Show the configuration dialog and initialise the system
    // You can skip this and use root.restoreConfig() to load configuration
    // settings if you were sure there are valid ones saved in ogre.cfg
    if (mRoot->showConfigDialog())
    {
        // If returned true, user clicked OK so initialise
        // Here we choose to let the system create a default rendering window by passing 'true'
        mWindow = mRoot->initialise(true);
        return true;
    }
    else
    {
        return false;
    }
}

void OgreApplication::createSceneManager(void)
{
    // Create the SceneManager, in this case a generic one
    mSceneMgr = mRoot->createSceneManager(ST_GENERIC, "OgreApplication");
	mWorldlm = new NxOgre::World();
	mScene = mWorldlm->createScene("Myscene",mSceneMgr,"gravity: 0 -100 0, floor: no");
}

void OgreApplication::createGameManager(void)
{
    // Create the SceneManager, in this case a generic one
	mGameMgr = GameManager::getSingletonPtr();
	mGameMgr->setSceneMgr(mSceneMgr);
	mGameMgr->setScene(mScene);
	mGameMgr->setRoot(mRoot);
	mRoot->addFrameListener(mGameMgr);
}

void OgreApplication::createCamera(void)
{
    // Create the camera
    mCamera = mSceneMgr->createCamera("PlayerCam");

    // Position it at 500 in Z direction
    mCamera->setPosition(Vector3(0,0,500));
    // Look back along -Z
    mCamera->lookAt(Vector3(0,0,-300));
    mCamera->setNearClipDistance(5);
}

void OgreApplication::createFrameListener(void)
{
    mInteractListener= new InteractListener(mWindow, mCamera);
    mInteractListener->showDebugOverlay(true);
    mRoot->addFrameListener(mInteractListener);
}

void OgreApplication::createViewports(void)
{
    // Create one viewport, entire window
    Viewport* vp = mWindow->addViewport(mCamera);
    vp->setBackgroundColour(ColourValue(0,0,0));

    // Alter the camera aspect ratio to match the viewport
    mCamera->setAspectRatio(Real(vp->getActualWidth()) / Real(vp->getActualHeight()));
}

/// Method which will define the source of resources (other than current folder)
void OgreApplication::setupResources(void)
{
    // Load resource paths from config file
    ConfigFile cf;
    cf.load(mResourcePath + "resources.cfg");

    // Go through all sections & settings in the file
    ConfigFile::SectionIterator seci = cf.getSectionIterator();

    String secName, typeName, archName;
    while (seci.hasMoreElements())
    {
        secName = seci.peekNextKey();
        ConfigFile::SettingsMultiMap *settings = seci.getNext();
        ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i)
        {
            typeName = i->first;
            archName = i->second;
            ResourceGroupManager::getSingleton().addResourceLocation(
                archName, typeName, secName);
        }
    }
}

/// Optional override method where you can perform resource group loading
	/// Must at least do ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
	void OgreApplication::loadResources(void)
	{
		// Initialise, parse scripts etc
		ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

	}
