#include "ManoeuvreControler.h"

// Constructeurs
ManoeuvreControler::ManoeuvreControler(){}
ManoeuvreControler::~ManoeuvreControler(){}

/**
* @desc	Ajoute une manoeuvre en haut de la pile
*/
void ManoeuvreControler::addFrontManoeuvre(Manoeuvre *pManoeuvre)
{
	this->m_dManoeuvreList.push_front(pManoeuvre);
}

/**
* @desc	Ajoute une manoeuvre en haut de la pile
*/
void ManoeuvreControler::addBackManoeuvre(Manoeuvre *pManoeuvre)
{
	this->m_dManoeuvreList.push_back(pManoeuvre);
}

/**
* @desc Retourne la manoeuvre courrante (en haut de la pile)
*/
Manoeuvre* ManoeuvreControler::getFrontManoeuvre()
{
	return this->m_dManoeuvreList.front();
}

/**
* @desc	Supprime la manoeuvre courrante (en haut de la pile)
*/
void ManoeuvreControler::remFrontManoeuvre()
{
	delete	this->m_dManoeuvreList.front();
	this->m_dManoeuvreList.pop_front();
}

/**
* @desc	Retourne vrai si la pile de manoeuvres est vide
*/
bool ManoeuvreControler::isEmpty()
{
	return this->m_dManoeuvreList.empty();
}

/**
* @desc	Vide l'intégralité de la pile de manoeuvres
*/
void ManoeuvreControler::clear()
{
	this->m_dManoeuvreList.clear();
}