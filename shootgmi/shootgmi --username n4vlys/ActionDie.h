#if !defined(ACTIONDIE_H)
#define ACTIONDIE_H
#include "Action.h"


class ActionDie : public Action
{

public:
	ActionDie();
	ActionDie(Agent* pAgent);
	virtual ~ActionDie();

    virtual bool isEnd();
    virtual void init();
	virtual void reset();
	virtual void update(const FrameEvent& pEvt);
	

	Timer mTimeScan;
};
#endif 
