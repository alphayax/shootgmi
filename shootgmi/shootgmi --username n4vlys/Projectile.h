#pragma once
#include "NxOgre.h"
#include "ogre.h"
using namespace Ogre;

class Projectile : public FrameListener
{
public:
	Projectile(const String& pName, String pProprio, const String& pMesh);
	~Projectile(void);

	Real getOrientation();
	Vector3 getPosition();
	Vector3 getPositionTete();
	Real getRotation();
	Vector3 getVelocity();

	bool frameStarted(const FrameEvent& pEvt);

	void setOrientation(Real pAngle);
	void setPosition(Vector3 pPosition);
	void setRotation(Real pRota);
	void setVelocity(Vector3 pVelocity);

	NxOgre::Body *mBody;
	NxOgre::Scene* mScene;

	Real mOrientation;
	Vector3 mPosition;
	Real mRotation;
	Vector3 mVelocity;
	int nbFrame;
	String mName;
	bool isAlive;
	String mProprio;
	Timer mTimeProject;
};
