#include "GenerateWaypoint.h"
#include <OgreEdgeListBuilder.h> 
#define INFINIT 9999999

class TriangleOgre
{
public:
	Vector3 a;
	Vector3 b;
	Vector3 c;
	int subMeshIndex;
	TriangleOgre(){

	
	};
	~TriangleOgre()
	{

	}
};

class Index
{

public:
	unsigned short x;
	unsigned short y;
	unsigned short z;
	int subMeshIndex;
	Index()
	{
	}
};


class IndList
{

public:
	unsigned int x;
	unsigned int y;
	unsigned int z;

	std::vector<Index*> index;
	int mTaille;
	int offset;
	IndList(){
	mTaille=0;
	offset=0;
	};

	Index* add(unsigned int Taille )
	{
		index.resize(index.size() + 1 );
		index[index.size()-1]= new Index();
		return index[index.size()-1];
	}
	Index* get(unsigned int i ){ return index[i];}

	void clear()
	{
		mTaille=0;
		offset=0;
		index.clear();
		
	}

};

class TriangleList
{

public:
	std::vector<TriangleOgre *> t;
	unsigned int mTaille;
	int offset;

	TriangleList(){
		mTaille=0;
		offset=0;

	
	};
	TriangleOgre* add( unsigned int Taille )
	{
		

		t.resize(t.size() + 1 );
		t[t.size()-1]= new TriangleOgre();
		return t[t.size()-1];

	}
	TriangleOgre* get(unsigned int i ){ return t[i];}

	void clear()
	{
		mTaille=0;
		offset=0;
		t.clear();
		
		
	}
	

	

}; 
GenerateWaypoint::GenerateWaypoint()
{
}


GenerateWaypoint::GenerateWaypoint(Entity * pEntity, Carte * pCarte, SceneManager* pSceneMgr)

{
        nil=pEntity;
        //mCarte=pCarte;
        test=true;
        mSceneMgr=pSceneMgr;
        mFrame=0;
		fichier.open("name.txt", std::ios::trunc);
		mCarte=pCarte;
}

GenerateWaypoint::~GenerateWaypoint()
{
    //dtor
}
static int frame = 0;

void GenerateWaypoint::indexion(TriangleList &pTriangleList) 
      { 
		  mCarte->mWaypointList.clear();
		  
		  for(int i= 0;i<(int) pTriangleList.t.size();i++) //435-445
		  {  
				Waypoint * aw= new Waypoint((const Vector3) (nil->getParentSceneNode()->getOrientation() * pTriangleList.t[i]->a * nil->getParentSceneNode()->getScale()) + nil->getParentSceneNode()->getPosition());
				Waypoint * bw= new Waypoint((const Vector3) (nil->getParentSceneNode()->getOrientation() * pTriangleList.t[i]->b * nil->getParentSceneNode()->getScale()) + nil->getParentSceneNode()->getPosition());
				Waypoint * cw= new Waypoint((const Vector3) (nil->getParentSceneNode()->getOrientation() * pTriangleList.t[i]->c * nil->getParentSceneNode()->getScale()) + nil->getParentSceneNode()->getPosition());
				
				Vector3 difVect1=(*aw)-(*bw);
				Vector3 difVect2=(*bw)-(*cw);
				Vector3 difVect3=(*cw)-(*aw);

				(*bw)+=difVect1/2;
				(*cw)+=difVect2/2;
				(*aw)+=difVect3/2;

				bool existA=false, existB=false, existC=false;
				
				for(int j=0; j< (int) mCarte->mWaypointList.size(); j++)
				{
					if((*aw-*(mCarte->mWaypointList[j])).length()<=8 ) {aw=mCarte->mWaypointList[j]; existA=true;}
					if((*bw-*(mCarte->mWaypointList[j])).length()<=8) {bw=mCarte->mWaypointList[j]; existB=true;}
					if((*cw-*(mCarte->mWaypointList[j])).length()<=8 ) {cw=mCarte->mWaypointList[j]; existC=true;}
				}
				
				
				double coefMax=0.1;
				double hautMax=40;
				
				double coef1=coefDirOnY(difVect1);
				double coef2=coefDirOnY(difVect2);
				double coef3=coefDirOnY(difVect3);
				
				//fichier <<"[aw "<<*aw<<" bw "<<*bw<<" cw "<<*cw<<"]"<<std::endl;
				//fichier <<"[bw-aw "<<difVect1<<" cw-aw "<<difVect2<<" bw-cw "<<difVect3<<"]"<<std::endl;
				//fichier <<"["<<abs(coef1)<<" "<<abs(coef2)<<" "<<abs(coef3)<<"]["<<abs(difVect1.y)<<" "<<abs(difVect2.y)<<" "<<abs(difVect3.y)<<"]\t=>"<<std::endl;
				if((abs(coef1)<=coefMax && abs(coef2)<=coefMax && abs(coef3)<=coefMax) 
					|| (abs(difVect1.y)<=hautMax && abs(difVect2.y)<=hautMax && abs(difVect3.y)<=hautMax
					&& (abs(coef1)>=INFINIT || abs(coef2)>=INFINIT || abs(coef3)>=INFINIT)
					))

				/*if(((coef1!=INFINIT && abs(coef1)<=coefMax && abs(difVect1.y)<=30&& abs(difVect1.x)<=400&& abs(difVect1.z)<=400) &&
					(coef2!=INFINIT && abs(coef2)<=coefMax && abs(difVect2.y)<=30&& abs(difVect2.x)<=400&& abs(difVect2.z)<=400) &&
					(coef3!=INFINIT && abs(coef3)<=coefMax && abs(difVect3.y)<=30&& abs(difVect3.x)<=400&& abs(difVect3.z)<=400)) ||
					(abs(difVect1.y)<=hautMax && (abs(coef1)>=coefMax ||abs(coef3)>=coefMax || abs(coef2)>=coefMax )  && abs(difVect2.y)<=hautMax && abs(difVect3.y)<=hautMax ))*/
				{
					//fichier <<"true"<<std::endl<<std::endl;
					if(!existA)	mCarte->mWaypointList.push_back(aw);
					if(!existB) mCarte->mWaypointList.push_back(bw);
					if(!existC) mCarte->mWaypointList.push_back(cw);

					if(*aw!=*bw)aw->addVoisin(bw);
					if(*aw!=*cw)aw->addVoisin(cw);
					if(*bw!=*aw)bw->addVoisin(aw);
					if(*bw!=*cw)bw->addVoisin(cw);
					if(*cw!=*aw)cw->addVoisin(aw);
					if(*cw!=*bw)cw->addVoisin(bw);
				}	//else fichier <<"false"<<std::endl<<std::endl;
		}
} 

void GenerateWaypoint::filtreVerticalWaypoint()
{
	/*for(int i=0; i<(int) mCarte->mWaypointList.size(); i++)
	{
		delVerticalWaypoint(*(mCarte->mWaypointList[i]));
	}*/

	for(std::vector<Waypoint*>::iterator it=mCarte->mWaypointList.begin(); it<mCarte->mWaypointList.end(); it++)
	{
		if((int)(*it)->mVoisin.size()==2)
		{	
			Waypoint* fils1=(*it)->mVoisin[0];
			Waypoint* fils2=(*it)->mVoisin[1];
			bool surFils1=false, surFils2=false;
			Vector3 dif1=*fils1- (Vector3)(*(*it));
			Vector3 dif2=*fils2- (Vector3)(*(*it));
			if(abs(coefDirOnY(dif1))>0.1) surFils1=true;
			if(abs(coefDirOnY(dif2))>0.1) surFils2=true;
			if(surFils1 && surFils2) 
			{
				(*it)->mVoisin[0]->removeVoisin(*it);
				(*it)->mVoisin[1]->removeVoisin(*it);
				mCarte->mWaypointList.erase(it);
				it=mCarte->mWaypointList.begin();
			}
		}
	}
}

void GenerateWaypoint::composanteConnexe(Waypoint *pWp)
{
	pWp->valeur=1;
	for(int i=0;i<(int)pWp->mVoisin.size();i++)
	{
		if(pWp->mVoisin[i]->valeur!=1)
			composanteConnexe(pWp->mVoisin[i]);
	}
}

bool GenerateWaypoint::frameStarted(const FrameEvent& pEvt)
{	
    if(test==true && frame++==10)
	{
		int	yMin	= 5000;
		int wpMin	= 0;

		test = false;
		//nil->setVisible(false);
		TriangleList triangleList;
		triangulationMesh(triangleList);
		indexion(triangleList);
		//fichier << mCarte->mWaypointList.size() << std::endl;
		for (int i=0; i<(int)mCarte->mWaypointList.size(); i++)
		{
			//fichier << i << std::endl;
			mCarte->init(mCarte->mWaypointList[i]);
			if (mCarte->mWaypointList[i]->getY()<yMin) 
			{
				wpMin=i;
				yMin=mCarte->mWaypointList[i]->getY();
			}
		}
		//fichier << mCarte->mWaypointList.size() << std::endl;
		// On triangule la composante connexe � partir du WP le plus bas de la map
		composanteConnexe(mCarte->mWaypointList[wpMin]);

		
		for (int i=0;i<(int)mCarte->mWaypointList.size();i++)
		{
			if(mCarte->mWaypointList[i]->valeur!=1)
			{
				std::vector<Waypoint * >::iterator itW;
				itW=mCarte->mWaypointList.begin();
				//fichier<<i<<" toto"<<std::endl;
				itW=mCarte->mWaypointList.erase(itW+i);
				i--;
			//Waypoint *test = new Waypoint(Vector3(0,0,0));
			//mCarte->mWaypointList[i]=test;
			}
		}
		
		for(int i=0;i<(int)mCarte->mWaypointList.size();i++)//(int)mCarte->mWaypointList.size()
		{
			mCarte->mWaypointList[i]->drawVoisin(mSceneMgr);
		}

		
		// Generation des points de couverture

		mCarte->generateCoverWP();

		//std::stable_sort(mCarte->mWaypointList.begin(),mCarte->mWaypointList.end());
		mCarte->writeWaypoint();
		//mCarte->readWaypoint();
	}
	return true;
}

double GenerateWaypoint::coefDirOnY(Vector3& v)
{
	double res;
	if(v.x==0 && v.z==0) return INFINIT;
	else res=(double)v.y/sqrt(pow((double)v.x,2)+pow((double)v.z,2));

	//fichier << StringConverter::toString(v) << "\t-> " << res << std::endl;

	return res;
}


bool GenerateWaypoint::RaycastFromPoint(Waypoint *point,
                                        Waypoint * normal)
{
    RaySceneQuery *mRaySceneQuery= mSceneMgr->createRayQuery(Ray());
    mRaySceneQuery->setSortByDistance(true);

    Ogre::Ray ray(Ogre::Vector3(point->x, point->y, point->z),
                  Ogre::Vector3(normal->x, normal->y, normal->z));

    if (mRaySceneQuery != NULL)
    {
        mRaySceneQuery->setRay(ray);

        if (mRaySceneQuery->execute().size() <= 0)
        {
            mSceneMgr->destroyQuery(mRaySceneQuery);
            return (false);

        }
        else {
            mSceneMgr->destroyQuery(mRaySceneQuery);
            return(true);
        }
    }
    else
    {
        mSceneMgr->destroyQuery(mRaySceneQuery);
        return (false);
    }
}

void GenerateWaypoint::createWaypoint(std::vector<Vector3> &triangleList)
{
	/*for(int i=0; i<(int)triangleList.size(); i++)
	{
		
	fichier<<mCarte->mWaypointList.size()<<std::endl;
	fichier<<mCarte->mWaypointList.size()<<std::endl;
	fichier<<triangleList[i].x<<std::endl;
	fichier<<triangleList[i].y<<std::endl;
	fichier<<triangleList[i].z<<std::endl;
	fichier<<std::endl;
		
		Waypoint *aw =mCarte->mWaypointList[triangleList[i].x];
		Waypoint *bw =mCarte->mWaypointList[triangleList[i].y];
		Waypoint *cw =mCarte->mWaypointList[triangleList[i].z];

		
	
		
		double coefMax=0.3;
		double hautMax=0;
		Vector3 difVect1=(*bw)-(*aw);
		Vector3 difVect2=(*cw)-(*aw);
		Vector3 difVect3=(*bw)-(*cw);
		
		double coef1=coefDirOnY(difVect1);
		double coef2=coefDirOnY(difVect2);
		double coef3=coefDirOnY(difVect3);
		
		if((coef1!=INFINIT && coef1<=coefMax &&
			coef2!=INFINIT && coef2<=coefMax &&
			coef3!=INFINIT && coef3<=coefMax) ||
			(abs(difVect1.y)<=hautMax && abs(difVect2.y)<=hautMax && abs(difVect2.y)<=hautMax))
		{*/
			/*
			aw.drawLine(&bw,mSceneMgr);
			aw.drawLine(&cw,mSceneMgr);
			cw.drawLine(&bw,mSceneMgr);
			*/
			/*aw->addVoisin(bw);
			aw->addVoisin(cw);
			bw->addVoisin(aw);
			bw->addVoisin(cw);
			cw->addVoisin(aw);
			cw->addVoisin(bw);

		}
		else 
		{
			std::vector<Waypoint * >::iterator itW;
			itW=mCarte->mWaypointList.begin();
			itW=mCarte->mWaypointList.erase(itW+int(triangleList[i].x));
			itW=mCarte->mWaypointList.erase(itW+int(triangleList[i].x));
			itW=mCarte->mWaypointList.erase(itW+int(triangleList[i].x));
		}
	}*/
}



void GenerateWaypoint::update(const FrameEvent& pEvt)
{

}


TriangleList& GenerateWaypoint::triangulationMesh(TriangleList& pTriangleList)

{

// declare the linkedlist we will need for each submesh..
   // IndList is a custom linkedlist of an object of 3 unsigned ints
   //to hold index for one triangle.. TriList is a linkedlist of 3 floats[3]
   // to hold the vertext for one triangle. I didnt absolutly need the linked
   // list i could have read the data straight from the buffer but I have
   // other uses for retaining the data in that form later.

   // also there isnt code to parse meshes with sharedvertex im not sure what to
   // do about them I dont have a mesh with shared vertex to test with yet
   IndList   *ill   = new IndList;
	//TriList   *tll   = new TriList;
   unsigned int indexListCount = 0;
   unsigned int vertListCount  = 0;
   unsigned int i=0,j=0;
   unsigned int numSubMeshes   = 0;

    // in the class this code resides i have a CurrentNode being edited so the gui
   // button just passes the index of the object to draw. the currentnode is used
   // to get the entity from.

   Ogre::MeshPtr pMesh = nil->getMesh();
   numSubMeshes =pMesh->getNumSubMeshes();
 TriangleOgre *tri;
   // to avoid submesh problems we preform the entire operation on a presubmesh basis
   for (i=0;i<numSubMeshes;i++)
   {
      // each submesh pass clear the prev index list but not the vertex triangle list
      ill->clear();
      Index *ind;
	 
      indexListCount = 0;
      unsigned short test2[3];
      float test[3];

      // get the current submesh & indexbuffer
      Ogre::SubMesh *pSubMesh = pMesh->getSubMesh(i);
      Ogre::HardwareIndexBufferSharedPtr ibuf = pSubMesh->indexData->indexBuffer;
	
      // load the linked list with the index points for each triangle face
      // by reading the buffer in blocks of 3 unsigned shorts
      for (j=0;j<pSubMesh->indexData->indexCount;j+=3)
      {
         ibuf->readData(sizeof(unsigned short)*j,sizeof(unsigned short)*3,&test2);
         ind = ill->add(indexListCount);
         ind->x = test2[0];
         ind->y = test2[1];
         ind->z = test2[2];
         ind->subMeshIndex = i;
		 indexListCount++;
		 
      }
      // get the vertex element of the submesh and a buffer for it
      const Ogre::VertexElement *elem =
         pSubMesh->vertexData->vertexDeclaration->findElementBySemantic(Ogre::VES_POSITION);

      Ogre::HardwareVertexBufferSharedPtr vbuf =
            pSubMesh->vertexData->vertexBufferBinding->getBuffer(elem->getSource());

      // here we ask the declaration the size of the element so we know how much extra
      // data like normals and uv's are in the element not all meshes will have the same
      // element size
      size_t vOffset = pSubMesh->vertexData->vertexDeclaration->getVertexSize(elem->getSource())/4;

      // instead of reading the whole buffer we just read the vertex positions of each triangle
      // and place them in the linked list as a complete triangle of the mesh
      for(unsigned int g = 0;g<indexListCount;g++)
      {
         // here we are stuffing the linked list but this is also the place
         // where you could pass the triangles to coldet for the collision
         // mesh with Coldet collisionobject->addtriangle(f[3],f[3],f[3])
         // just use 3 test varibles like testX[3] testY[3]...
         tri = pTriangleList.add(vertListCount);
         ind = ill->get(g);
         vbuf->readData(ind->x*sizeof(float)*vOffset,3 * sizeof(float), &test);
		 tri->a.x = test[0];
         tri->a.y = test[1];
         tri->a.z = test[2];
         vbuf->readData(ind->y*sizeof(float)*vOffset,3 * sizeof(float), &test);
         tri->b.x = test[0];
		 tri->b.y = test[1];
		 tri->b.z = test[2];
         vbuf->readData(ind->z*sizeof(float)*vOffset,3 * sizeof(float), &test);
		 tri->c.x = test[0];
		 tri->c.y = test[1];
		 tri->c.z = test[2];
         tri->subMeshIndex = i;
         vertListCount++;
      }
   }
   // make a node to draw the lines and set at the same position
   // as the mesh
   // note i may need to add orientation matching later
   SceneNode *myNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
   myNode->setPosition(nil->getParentNode()->getPosition());   
   
   // line is the same clsLine3D from the ogre wiki
   
    /*for (i=0;i<vertListCount;i++)
   {
      // I get the triangle from the linked list and add the first point
      // twice first and last so it will draw 3 lines
      tri = pTriangleList.get(i);
	Vector3 a(tri->x[0],tri->x[1],tri->x[2]);
	Vector3 b(tri->y[0],tri->y[1],tri->y[2]);
	Vector3 c(tri->z[0],tri->z[1],tri->z[2]);
	

	Waypoint aw((const Vector3) (nil->getParentSceneNode()->getOrientation() * ((Vector3)a * nil->getParentSceneNode()->getScale()) + nil->getParentSceneNode()->getPosition()));
	Waypoint bw((const Vector3) (nil->getParentSceneNode()->getOrientation() * ((Vector3)b * nil->getParentSceneNode()->getScale()) + nil->getParentSceneNode()->getPosition()));
	Waypoint cw((const Vector3) (nil->getParentSceneNode()->getOrientation() * ((Vector3)c * nil->getParentSceneNode()->getScale()) + nil->getParentSceneNode()->getPosition()));

	aw.drawLine(&bw,mSceneMgr);
	aw.drawLine(&cw,mSceneMgr);
	bw.drawLine(&cw,mSceneMgr);
     // line->addPoint(Ogre::Vector3(tri->x[0],tri->x[1],tri->x[2]));
     // line->addPoint(Ogre::Vector3(tri->y[0],tri->y[1],tri->y[2]));
     // line->addPoint(Ogre::Vector3(tri->z[0],tri->z[1],tri->z[2]));
      //line->addPoint(Ogre::Vector3(tri->x[0],tri->x[1],tri->x[2]));
   }*/

   // draw the lines from all the points this method my cause a few stray lines
   // if the triangles are not in order but you will still be able to see the
   // basic mesh but making each triangle its own node and line is extreamly
   // hard on the engine
   //myNode->attachObject(line);
   //line->drawLines();
   
   //ill->clear();
   //pTriangleList.clear();
   //delete ill;
   //delete tll; 

   return pTriangleList;


   }
