///////////////////////////////////////////////////////////
//  GameManager.cpp
//  Implementation of the Class GameManager
//  Created on:      02-nov.-2007 18:52:37
///////////////////////////////////////////////////////////

#include "GameManager.h"
#include "OgreStringConverter.h"
#include "BonusVie.h"


GameManager * GameManager::mSingleton = NULL;

GameManager::GameManager()
{
	nbParticules=0;
	mNbRound=0;
	agentSelected=NULL;
	nbBonusActivated=0;
}

void GameManager::setSceneMgr(SceneManager* pSceneMgr)
{
	mSceneMgr=pSceneMgr;
}

void GameManager::setScene(NxOgre::Scene* pScene)
{
	mScene=pScene;
}

GameManager& GameManager::getSingleton()
{
	if(mSingleton==NULL)
	{
		mSingleton=new GameManager();
	}
	return *mSingleton;
}

GameManager* GameManager::getSingletonPtr()
{
	if(mSingleton==NULL)
	{
		mSingleton=new GameManager();
	}
	return mSingleton;
}


GameManager::~GameManager()
{

}


Agent * GameManager::createAgent(const String& mesh, const Vector3& pPosition, Real pOrientation)
{

    String name="Agent";
	name.append(Ogre::StringConverter::toString(mAgentMap.size()));

    Agent * a=new Agent(name, mesh);
    a->setPosition(pPosition);
    a->setOrientation(pOrientation);
    mAgentMap[name]=a;
	mRoot->addFrameListener(a);
	
    return a;
}

void GameManager::createObjet(const Vector3& pPosition, const String pType)
{
	String name="Objet";
	name.append(Ogre::StringConverter::toString(mObjMap.size()));
	
	NxOgre::Body * bodyObj = mScene->createBody(name.append("; Cube.mesh"), new NxOgre::CubeShape(-100,200,-200),pPosition,"mass:10000");
	bodyObj->getEntity()->setMaterialName("Examples/BumpyMetal");
	mObjMap[name]=bodyObj;
}

Projectile* GameManager::createProjectile(const String& mesh, const String& pPropri, const Vector3& pPosition, const Vector3& pVelo)
{

    String name="Projectile";
	name.append(Ogre::StringConverter::toString(mProjMap.size()));

    Projectile * p=new Projectile(name, pPropri, "sphere.mesh");
    p->setPosition(pPosition);
    p->setVelocity(pVelo);
    mProjMap[name]=p;
	mRoot->addFrameListener(p);
	
    return p;
}

void GameManager::removeProjectile(String pName)
{
	if (mScene->getActor(pName)!=NULL)
	{
		//std::cout<<pName<<"  body :"<<mProjMap[pName]->mBody->getName()<<std::endl;
		mSceneMgr->destroyEntity(pName);
		mRoot->removeFrameListener(mProjMap[pName]);
		mScene->destroyBody(pName);
		//mProjMap.erase(mProjMap.find(pName));
	}
}

SceneManager* GameManager::getSceneManager()
{
    return mSceneMgr;
}


NxOgre::Scene* GameManager::getScene()
{
    return mScene;
}

Carte* GameManager::getCarte()
{
    return mCarte;
}

void GameManager::setCarte(Carte *pCarte)
{
    mCarte=pCarte;
}

void GameManager::setRoot(Root* pRoot)
{
	mRoot=pRoot;
}

void GameManager::createParticule(const Vector3& pVec, String pType)
{
	String name="Particule";
	name.append(Ogre::StringConverter::toString(nbParticules));
	String name2="Part";
	name2.append(Ogre::StringConverter::toString(nbParticules));
	nbParticules++;

	

	ParticleSystem* etincelle=new ParticleSystem();
	if (pType=="fumee")  etincelle= mSceneMgr->createParticleSystem(name2, "PEExamples/smoke");
	else if (pType=="explosion")  etincelle= mSceneMgr->createParticleSystem(name2, "PEExamples/space");
	else  etincelle = mSceneMgr->createParticleSystem(name2, "test/etin2");
	 SceneNode* particleNode = mSceneMgr->getRootSceneNode()->createChildSceneNode(name);
	particleNode->attachObject(etincelle);
	particleNode->setPosition(pVec);
	mPartMap[name]=etincelle;
	
	if(mPartMap.size()>100)
	{
		mSceneMgr->destroyParticleSystem(mPartMap.begin()->second);
		mPartMap.erase(mPartMap.begin());
	}
	
	/*
	//boule lumineuse
	String name2="ReconstructedSet";
	name2.append(Ogre::StringConverter::toString(mBillMap.size()));
	Ogre::BillboardSet* pSet = mSceneMgr->createBillboardSet(name2, 100);
	pSet->setMaterialName("Examples/FlyingLightMaterial");
	//pSet->createBillboard(Ogre::Vector3(0, 0, 0), Ogre::ColourValue::Red); 

	//pSet->setMaterialName("Chrome.jpg");
	mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(pSet);
	Billboard* pBoard = pSet->createBillboard(pVec);
	*/
	//mBillMap[name]=pBoard;

}


String GameManager::createBonus(const Vector3& pVec, String pType, Bonus* pB)
{
	String name="Bonus";
	name.append(Ogre::StringConverter::toString(mBonusMap.size()));
	//boule lumineuse
	String name2="ReconstructedSet";
	name2.append(Ogre::StringConverter::toString(mBonusMap.size()));
	Ogre::BillboardSet* pSet = mSceneMgr->createBillboardSet(name2, 100);
	pSet->setMaterialName("Examples/FlyingLightMaterial");
	//pSet->createBillboard(Ogre::Vector3(0, 0, 0), Ogre::ColourValue::Red); 
	NxOgre::Body *body = mScene->createBody(name, new NxOgre::SphereShape(10,"Group: sgrBonus"),pVec+Vector3(0,50,0),"mass:10, Group: grBonus");
	body->raiseBodyFlag(NxBodyFlag::NX_BF_FROZEN_POS);
	body->raiseBodyFlag(NxBodyFlag::NX_BF_FROZEN_ROT);
	//pSet->setMaterialName("Chrome.jpg");
	mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(pSet);
	Billboard* pBoard = pSet->createBillboard(pVec+Vector3(0,50,0));

	mBonusMap[name]=pB;
	mBillMap[name]=pSet;
	nbBonusActivated++;
	return name;
}

void GameManager::hideBonus(String pName)
{
	mBillMap[pName]->setVisible(false);
	nbBonusActivated--;
}

void GameManager::showBonus(String pName)
{
	mBillMap[pName]->setVisible(true);
}

void GameManager::selectAgent(String pName)
{
	if(mAgentMap[pName]->mIsSelected && selectedAgent.getMilliseconds()>200)
	{
		mAgentMap[pName]->mIsSelected=false;
		agentSelected=NULL;
		selectedAgent.reset();
		mAgentMap[pName]->mEntity->getParentSceneNode()->showBoundingBox(false);
	}
	else if (selectedAgent.getMilliseconds()>200)
	{
		mAgentMap[pName]->mIsSelected=true;
		if(agentSelected!=NULL)
		{
			agentSelected->mIsSelected=false;
			agentSelected->mEntity->getParentSceneNode()->showBoundingBox(false);
		}
		agentSelected=mAgentMap[pName];
		selectedAgent.reset();
		mAgentMap[pName]->mEntity->getParentSceneNode()->showBoundingBox(true);
	}
}

void GameManager::controlAgent(String pName)
{
	Agent * agent=mAgentMap[pName];
	if(!agent->mIsControled)
	{
		agent->mIsControled=true;
		agentControled.push_back(agent);
	}
}

void GameManager::uncontrolAgent(String pName)
{
	Agent * agent=mAgentMap[pName];
	if(agent->mIsControled)
	{
		agent->mIsControled=false;
		agent->clearActionList();
		agentControled.erase(std::find(agentControled.begin(), agentControled.end(), agent));
	}
}
void GameManager::unselectAllAgent()
{
	std::list<Agent*>::iterator it;
	it = agentControled.begin();
	while( it !=agentControled.end() )
	{
		(*it)->mEntity->getParentSceneNode()->showBoundingBox(false);
		this->selectAgent((*it)->getName());
		this->uncontrolAgent((*it)->getName());
		
		it = agentControled.begin();
		//it=agentControled.erase(it);
	}
}
bool GameManager::frameStarted(const FrameEvent& pEvt)
{
	std::map<unsigned int, Team*>::iterator it;
	bool hasTeamDown=false;
	/*for(it=m_TeamMap.begin(); it!=m_TeamMap.end() && !hasTeamDown; it++)
	{
		if(it->second->getAliveMembersCount()==0) hasTeamDown=true;
	}*/

	if(hasTeamDown || mTimerPartie.getMilliseconds()>=CONST_PARTIE_TIME_OUT)
	{
		restartPartie();
	}

	return true;
}

void GameManager::restartPartie()
{
	std::map<String, Agent*>::iterator		it;
	std::map<unsigned int, Team*>::iterator itTeam;

	for(it=mAgentMap.begin(); it!=mAgentMap.end(); it++)
	{
		//if(std::find(agentControled.begin(), agentControled.end(), it->second)!=agentControled.end())
		//{
			it->second->mHisto->ecrireHistoNote();
			it->second->mHisto->afficherDetails();
			it->second->mHisto->afficher();
			it->second->mHisto->clear();
			it->second->apprendre();			
			it->second->mHisto->initNote();


		//}
	}
	for(it=mAgentMap.begin(); it!=mAgentMap.end(); it++)
	{
		//if(std::find(agentControled.begin(), agentControled.end(), it->second)!=agentControled.end())
		//{
			it->second->repop();
		//}
	}

	
	for	(itTeam = this->m_TeamMap.begin(); itTeam != this->m_TeamMap.end(); ++itTeam) 
	{
		// Positionnement des Agents de la Team autour de leur WP de base
		//itTeam->second->initBaseTeamPosition();

		// Gestion des manoeuvres
		if (itTeam->second->hasManoeuvre())
			itTeam->second->clearTeamManoeuvres();
	}
	

	mTimerPartie.reset();
	mNbRound++;
}

Waypoint * GameManager::getBonusProche(Vector3 pVec)
{
	int distance=9999999;
	std::map<String, Bonus*>::iterator it;
	std::map<String, Bonus*>::iterator it2;
	for(it=mBonusMap.begin(); it!=mBonusMap.end(); it++)
	{
		if ((pVec-it->second->mPosition).length()<distance && it->second->mActif==true)
		{
			it2=it;
			distance=(pVec-it->second->mPosition).length();
		}
	}
	if( distance==9999999)
	{
		return NULL;
	}
	else return mCarte->plusProche(it2->second->mPosition);
}


void GameManager::placerBonus(int nb)
{
	for (int i=0; i<nb; i++)
	{
		srand(mTimerPartie.getMicroseconds());
		Vector3 vec=mCarte->mWaypointList[rand()%(mCarte->mWaypointList.size()-1)]->getVec();
		BonusVie *bv1= new BonusVie(vec,20);
		std::cout<<"Bonus cree"<<std::endl;
	}
}

