#ifndef TELEGRAM_H
#define TELEGRAM_H

#include "ogre.h"

using namespace Ogre;


//-------------------
// Types de messages
//-------------------
#define	MESSAGE_DEFAULT		0	// Message simple
#define	MESSAGE_REPORT		1	// Message de Type Rapport
#define	MESSAGE_COMMAND		2	// Message de Type Commande (ordre)


class Telegram
{
public:
	Telegram(void);
	Telegram(String sSender, String sReceiver, void* oExtra = NULL);	// Cr�e un t�l�gramme basique

public:
	int		getType(void);		// Retourne le Type du Message
	String	getSender(void);	// Retourne l'expediteur du message
	String	getReceiver(void);	// Retourne le destinataire du message

protected:
	int		m_nType;		// La nature du message
	String	m_sSender;		// L'expediteur du message
	String	m_sReceiver;	// Le destinataire du message
	void*	m_oExtra;		// L'objet concern� par le message
};

#endif
