///////////////////////////////////////////////////////////
//  Team_AgentsInSight.h
//  Impl�mentation des squads Tactics
//	Classe contenant les informations a propos des 
//	agents d�t�ct�s.
///////////////////////////////////////////////////////////

#ifndef Team_AgentsInSight_H
#define Team_AgentsInSight_H

#include "GameManager.h"

typedef std::vector< String >	vecIdAgents;	// Liste d'agents

class Team_AgentsInSight
{
public:
	Team_AgentsInSight();
	Team_AgentsInSight(String psMember, String psAgent);

protected:
	Vector3			m_oLastWPSeen;		// Dernier endroit o� la cible a �t� apper�u
	bool			m_bAlreadyVisible;	// Dit si la cible est toujours dans le champ de vision d'un des agents
	vecIdAgents		m_vTargetByMembers;	// Tableau des membres de l'equipe qui voient cette cible

public:
	bool			isVisible(void);								// Retourne vrai si la cible est en vue par au moins un membre de l'equipe
	vecIdAgents		isVisibleBy(void);								// Retourne la liste des membres qui ont cet agent en vue
	void			updateTarget( String sMember, String psAgent );	// Met a jour les donn�es concernant une cible potentielle
	void			remMemberViewing( String sMember);				// Supprime un membre de l'�quipe dans la liste des voyants.
};


#endif