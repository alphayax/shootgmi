///////////////////////////////////////////////////////////
//  ActionIdle.cpp
//  Implementation of the Class ActionIdle
//  Created on:      02-nov.-2007 18:52:36
///////////////////////////////////////////////////////////

#include "ActionIdle.h"
#include "GameManager.h"


ActionIdle::ActionIdle() : Action()
{
    mActionType="ActionIdle";
	mIsInit=false;
	mIsStop=false;
}


ActionIdle::ActionIdle(Agent* pAgent)
    : Action(pAgent)
{
    mActionType="ActionIdle";
    mIsInit=false;
	mIsStop=false;
}

ActionIdle::~ActionIdle()
{
}

void ActionIdle::init()
{
    mIsInit=true;
	mAgent->mAnimationState->setTimePosition(0);
	if (mAgent->mMesh=="ninja.mesh") mAgent->setAnimationState("Idle1");
	else mAgent->setAnimationState("Idle");
}

void ActionIdle::reset()
{
    mIsInit=false;
}

bool ActionIdle::isEnd()
{
	return  false;
}

void ActionIdle::update(const FrameEvent& pEvt)
{
	
	if (mAgent->mMesh=="ninja.mesh") mAgent->setAnimationState("Idle1");
	else mAgent->setAnimationState("Idle");
	mLinear=Vector3::ZERO;
	mAngular=0;
	if(mAgent->onContactWithMap)
	{
		/*if(mAgent->getVelocity().length()<3)
		{
			mAgent->setVelocity(Vector3::ZERO);
			mAgent->setRotation(0);
		}
		else
		{
			mLinear=(mAgent->getVelocity())*(-1);
			mLinear.y=0;
			mAngular=0;
		}*/
		mAgent->setVelocity(Vector3::ZERO);
		mAgent->setRotation(0);
	}
}
