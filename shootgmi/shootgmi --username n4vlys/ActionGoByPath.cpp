///////////////////////////////////////////////////////////
//  ActionGoByPath.cpp
//  Implementation of the Class ActionGoByPath
//  Created on:      02-nov.-2007 18:52:36
///////////////////////////////////////////////////////////

#include "ActionGoByPath.h"
#include "GameManager.h"
#include "ActionGoTo.h"


ActionGoByPath::ActionGoByPath() : Action()
{
    mActionType="ActionGoByPath";
    mActionGoTo=NULL;
	mWaypointDestination=NULL;
	mNbWalkedWaypoint=0;
	mIsInit=false;
}


ActionGoByPath::ActionGoByPath(Agent* pAgent, const Vector3& pDestination)
    : Action(pAgent), mDestination(pDestination)
{
    mActionType="ActionGoByPath";
    mActionGoTo=NULL;
	mNbWalkedWaypoint=0;
	mWaypointDestination=GameManager::getSingleton().getCarte()->plusProche(mDestination);
	mIsInit=false;
}

ActionGoByPath::ActionGoByPath(Agent* pAgent, Waypoint* pDestination)
    : Action(pAgent), mDestination(*pDestination), mWaypointDestination(pDestination)
{
    mActionType="ActionGoByPath";
    mActionGoTo=NULL;
	mNbWalkedWaypoint=0;
	mIsInit=false;
}

ActionGoByPath::~ActionGoByPath()
{
    delete mActionGoTo;
}


void ActionGoByPath::init()
{
	reset();
    GameManager * gameManager=mAgent->getGameManager();
    mAgent->aStar(mWalkList, mAgent->getWaypointCurrent()->mIndice, this->getWaypointDestination()->mIndice);
	arrange();
	mActionGoTo=new ActionGoTo(mAgent, mWalkList.front());
	mActionGoTo->init();
	arrange();

    mIsInit=true;

	// Envoi un rapport pour signifiquer qu'on commence un déplacement
	Report* oReport = new Report(this->mAgent->getName(), REPORT_MOVETO, this->mDestination);		
	Message::getSingleton()->send(oReport);
}

void ActionGoByPath::reset()
{
	mNbWalkedWaypoint=0;
    mWalkList.clear();
    delete mActionGoTo;
	mActionGoTo=NULL;
    mIsInit=false;
}

bool ActionGoByPath::isEnd()
{
	return  mIsInit && mWalkList.size()==0 && mActionGoTo->isEnd();
}

void ActionGoByPath::setDestination(const Vector3& pDestination)
{
    mDestination=pDestination;
	mNbWalkedWaypoint=0;
	mWaypointDestination=GameManager::getSingleton().getCarte()->plusProche(mDestination);
	GameManager * gameManager=mAgent->getGameManager();
	mWalkList.clear();
    mAgent->aStar(mWalkList, mAgent->getWaypointCurrent()->mIndice, getWaypointDestination()->mIndice);
	arrange();
	mActionGoTo->setDestination(mWalkList.front());
	
}

void ActionGoByPath::setDestination(Waypoint* pDestination)
{
	mDestination=*pDestination;
	mNbWalkedWaypoint=0;
	mWaypointDestination=pDestination;
	GameManager * gameManager=mAgent->getGameManager();
	mWalkList.clear();
    mAgent->aStar(mWalkList, mAgent->getWaypointCurrent()->mIndice, getWaypointDestination()->mIndice);
	arrange();
	mActionGoTo->setDestination(mWalkList.front());
	
}

Waypoint* ActionGoByPath::getWaypointDestination()
{
	if(mWaypointDestination==NULL) 
		mWaypointDestination=GameManager::getSingleton().getCarte()->plusProche(mDestination);
	return mWaypointDestination;
}

void ActionGoByPath::popFront()
{
	mWalkList.pop_front();
}

void ActionGoByPath::update(const FrameEvent& pEvt)
{
	if(mActionGoTo->isEnd() && !mWalkList.empty())
	{
		mNbWalkedWaypoint++;
		mActionGoTo->setDestination(*(mWalkList.front()));
		mWaypointCurrent=mWalkList.front();
		mWalkList.pop_front();
	}
	
	if(this->isEnd())
	{
		// Envoi d'un rapport a la team pour dire que l'on est bien arrivé
		Report* oReport = new Report(this->mAgent->getName(), REPORT_ARRIVED);
		Message::getSingleton()->send(oReport);
	}

	mActionGoTo->update(pEvt);
	mLinear=mActionGoTo->getLinear();
	mAngular=mActionGoTo->getAngular();
}

void ActionGoByPath::arrange()
{
	std::deque<Waypoint*>::iterator it;
	if(mWalkList.size()>2)
	{
		for (it=mWalkList.begin(); it<mWalkList.end()-2; ++it)
		{
			if ((*it)->wpVisible((*(it+2)), GameManager::getSingleton().getScene()))
			{
				it=mWalkList.erase(it+1);
				it=mWalkList.begin();
				
			}
		}
	}
	/*
	for (it=mWalkList.begin(); it<mWalkList.end()-1; ++it)
	{
		(*it)->drawLine((*(it+1)), GameManager::getSingleton().getSceneManager(),"rouge");
	}
	*/
	

	
}

