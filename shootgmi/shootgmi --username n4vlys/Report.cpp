#include "Report.h"

Report::Report(String psSender, int pnReport)
{
	this->m_nType	= MESSAGE_REPORT;
	this->m_nReport = pnReport;
	this->m_sSender = psSender;
}

Report::Report(String psSender, int pnReport, String psTargetBot) 
{
	this->m_nType		= MESSAGE_REPORT;
	this->m_nReport		= pnReport;
	this->m_sSender		= psSender;
	this->m_sTargetBot	= psTargetBot;
}

Report::Report(String psSender, int pnReport, Vector3 poTargetWP)
{
	this->m_nType		= MESSAGE_REPORT;
	this->m_nReport		= pnReport;
	this->m_sSender		= psSender;
	this->m_oTargetWP	= poTargetWP;
}

/**
* Accesseurs
**/
int		Report::getReportType()	{	return this->m_nReport;		}
String	Report::getTargetBot()	{	return this->m_sTargetBot;	}
Vector3 Report::getTargetWP()	{	return this->m_oTargetWP;	}

