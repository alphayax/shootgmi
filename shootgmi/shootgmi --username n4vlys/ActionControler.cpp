///////////////////////////////////////////////////////////
//  ActionControler.cpp
//  Implementation of the Class ActionControler
//  Created on:      02-nov.-2007 18:52:36
///////////////////////////////////////////////////////////

#include "ActionControler.h"
#include "Action.h"



ActionControler::ActionControler()
{

}


ActionControler::~ActionControler()
{

}


void ActionControler::addInFirstAction(Action* pAction)
{
	if (mActionList.empty() )
	{
		if(!mActionList.empty() && pAction->isInit()) mActionList.front()->reset();
		if(!pAction->isInit()) pAction->init();
		mActionList.push_front(pAction);
	}
	else if (firstAction()->getActionType()!="ActionDie")
	{
		if(!mActionList.empty() && pAction->isInit()) mActionList.front()->reset();
		if(!pAction->isInit()) pAction->init();
		mActionList.push_front(pAction);
	}
}

void ActionControler::addInLastAction(Action* pAction)
{
	if(empty() && !pAction->isInit()) pAction->init();
    mActionList.push_back(pAction);
}

void ActionControler::delFirstAction()
{
    delete mActionList.front();
    mActionList.pop_front();

    if(!mActionList.empty() && !mActionList.front()->isInit()) mActionList.front()->init();
}

Action* ActionControler::firstAction()
{
    return mActionList.front();
}

bool ActionControler::empty()
{
    return mActionList.empty();
	//return (mActionList.size() > 0);
}

std::deque< Action*> ActionControler::getDeque()
{
	return mActionList;
}

void ActionControler::clear()
{
	mActionList.clear();
}