///////////////////////////////////////////////////////////
//  Agent.cpp
//  Implementation of the Class Agent
//  Created on:      02-nov.-2007 18:52:37
///////////////////////////////////////////////////////////

#include "Agent.h"
#include <cmath>
#include <windows.h> 
#include "GameManager.h"
#include "Action.h"
#include "ActionDie.h"
#include "FSMBot.h"
#include "HistoriqueExemple.h"

Agent::Agent(const String& pName, const String& pMesh)
{
	this->m_bdecideState=true;
	//caract�re
	peur=0;
	violent=0;
	roublard=0;

	file.open("fichier.txt");
    //Initialisation
	LastAction="";
	mScene = GameManager::getSingletonPtr()->getScene();
	mName=pName;
	mMesh=pMesh;
	mFurtif=false;
	mIsSelected=false;
	mIsControled=false;
	mGameManager=GameManager::getSingletonPtr();
    mOrientation=0;
    mRotation=0;
    mMaxAcceleration=70;
    mMinDeceleration=10;
    mRadius=200;
    if (mMesh=="robot.mesh") mMaxSpeed=250;
	if (mMesh=="ninja.mesh") mMaxSpeed=220;
	onContactWithMap=false;
	mCible=NULL;
	mWaypointCurrent=NULL;
	isShooting=false;
	//initialisation du MLP
	layers1= new int[3];
	layers1[0]= NB_ENTREES_NN;
	layers1[1]= NB_ENTREES_NN * 0.75;;
	layers1[2]= NB_SORTIES_NN;
	mlp1= new MultiLayerPerceptron(3,layers1);
	this->mTimeFlee.reset();
	//apprendre();

	m_FSM= new FSMBot(this);
	mHisto=new HistoriqueExemple(this);
	//Initialisation vecteur des waypoint pour a*
	mWaypointList.resize(mGameManager->getCarte()->mWaypointList.size());
	for(int i=0; i<(int)mWaypointList.size(); i++) {mWaypointList[i]=new WaypointAStar(); mWaypointList[i]->mIndice=i;}

	//Initialisation Etat
	mEtat.pointsVie=50;
    mEtat.nbEnnemisEnVue=0;

	 // Initialisation Equipe
	 this->m_nTeamNumber = 0;


    //Creer agent
	//name.append("Mesh");
	//"myRobot; robot.mesh"
	String paramMesh=mName;
	paramMesh.append("; ");
	paramMesh.append(pMesh);

	mBody = mScene->createBody(paramMesh, new NxOgre::ConvexShape(pMesh,"Group: sgrAgent"),Vector3(0,0,0),"mass:10000, Group: grAgent");
	
	//String name=mSceneNode->getName();
	//mEntity=mGameManager->getSceneManager()->createEntity(name, pMesh);
	mEntity = mBody->getEntity();
	mEntity->setCastShadows( true );

	if (pMesh=="robot.mesh") setAnimationState( "Idle" );
	if (pMesh=="ninja.mesh") 
	{
		setAnimationState( "Idle1" );
		mEntity->getParentSceneNode()->scale(Vector3(0.6,0.6,0.6));
		//mEntity->getBoundingBox().scale(Vector3(0.6,0.6,0.6));
		mEntity->getParentSceneNode()->roll(Degree(90.0f));
	}
	
	//mBody->setGlobalOrientation(mBody->getEntity()->getParentNode()->getOrientation());
	
    //Dessine cercle
    /*ManualObject * circle = mGameManager->getSceneManager()->createManualObject("circle_name");
    circle->begin("BaseWhiteNoLighting", RenderOperation::OT_LINE_STRIP);
    unsigned point_index = 0;

    for(float theta = 0; theta <= 2 * Math::PI; theta += Math::PI / (mRadius * 5)) {
        circle->position(mRadius * cos(theta), 0, mRadius * sin(theta));
        circle->index(point_index++);
    }
    circle->index(0); // Rejoins the last point to the first.
    circle->end();*/

	//Text du dessus
	mText = new ObjectTextDisplay(getEntity(), GameManager::getSingleton().mSceneMgr->getCamera("PlayerCam"));
	//mText->setText("test");
	mText->enable(true);

	//============ TEXT =============
	TextVie = new MovableText("txt_vie_", "100");
	TextVie->setTextAlignment(MovableText::H_CENTER, MovableText::V_ABOVE); // Center horizontally and display above the node
	TextVie->setAdditionalHeight( 120.0f ); //msg->setAdditionalHeight( ei.getRadius() )
	TextVie->setCharacterHeight(20.0f);
	TextVie->setColor(ColourValue(0.0,1.0,0.4));
	mBody->getEntity()->getParentSceneNode()->attachObject(TextVie); 
	TextVie->setVisible(true);

	TextFrag = new MovableText("txt_frag_", "0");
	TextFrag->setTextAlignment(MovableText::H_CENTER, MovableText::V_ABOVE); // Center horizontally and display above the node
	TextFrag->setAdditionalHeight( 140.0f );
	TextFrag->setCharacterHeight(20.0f);
	mBody->getEntity()->getParentSceneNode()->attachObject(TextFrag); 
	TextFrag->setVisible(true);
	

	TextState = new MovableText("txt_state_", "Idle");
	TextState->setTextAlignment(MovableText::H_CENTER, MovableText::V_ABOVE); // Center horizontally and display above the node
	TextState->setAdditionalHeight( 160.0f );
	TextState->setCharacterHeight(20.0f);
	mBody->getEntity()->getParentSceneNode()->attachObject(TextState); 
	TextState->setVisible(true);

	TextArme = new MovableText("txt_arme_", "no_arme");
	TextArme->setTextAlignment(MovableText::H_CENTER, MovableText::V_ABOVE); // Center horizontally and display above the node
	TextArme->setAdditionalHeight( 180.0f );
	TextArme->setCharacterHeight(20.0f);
	TextArme->setColor(ColourValue(0.0,0.2,0.8));
	mBody->getEntity()->getParentSceneNode()->attachObject(TextArme); 
	TextArme->setVisible(true);

	TextTeam = new MovableText("txt_team_", "no_team");
	TextTeam->setTextAlignment(MovableText::H_CENTER, MovableText::V_ABOVE); // Center horizontally and display above the node
	TextTeam->setAdditionalHeight( 200.0f );
	TextTeam->setCharacterHeight(20.0f);
	TextTeam->setColor(ColourValue(0.8,0.0,0.0));
	mBody->getEntity()->getParentSceneNode()->attachObject(TextTeam); 
	TextTeam->setVisible(true);
	
	this->TextReport = new MovableText("txt_report_", "no report");
	this->TextReport->setTextAlignment(MovableText::H_CENTER, MovableText::V_ABOVE); // Center horizontally and display above the node
	this->TextReport->setAdditionalHeight( 220.0f );
	this->TextReport->setCharacterHeight(20.0f);
	this->TextReport->setColor(ColourValue(0.1,1.0,1.0));
	this->mBody->getEntity()->getParentSceneNode()->attachObject(this->TextReport); 
	this->TextReport->setVisible(true);

	//============ FIN-TEXT =============
	TextFrag->setCaption(this->getName());
	mHisto->initNote();
}


Agent::~Agent()
{
	for(int i=0; i<(int)mWaypointList.size(); i++) delete mWaypointList[i];
}


bool Agent::frameStarted(const FrameEvent& pEvt)
{

	//if (mMesh="ninja.mesh") std::cout<<Ogre::StringConverter::toString(pEvt.timeSinceLastFrame) <<std::endl;
	//mAnimationState->addTime( pEvt.timeSinceLastEvent );
	if (mMesh=="ninja.mesh")
	{
		if(mAnimationState->getAnimationName()=="Walk" || mAnimationState->getAnimationName()=="Stealth")
		{
			mAnimationState->addTime( (pEvt.timeSinceLastFrame*getVelocity().length()/35)/2 );
		}
		else
		{
			mAnimationState->addTime( pEvt.timeSinceLastFrame/2 );
		}
	}
	else
	{
		if(mAnimationState->getAnimationName()=="Walk")
		{
			mAnimationState->addTime( (pEvt.timeSinceLastFrame*getVelocity().length()/35) );
		}
		else
		{
			mAnimationState->addTime( pEvt.timeSinceLastFrame );
		}
	}
	mText->update();
	/*if(mEtat.pointsVie==0 && mName=="Agent1")
	{
		
		Carte *mCarte=mGameManager->getCarte();
		std::cout<<(int)mCarte->mWaypointList.size()<<std::endl;
		for(int i=0;i<(int)mCarte->mWaypointList.size();i++)	
		{
			
			waypointVisible(mCarte->mWaypointList[i]);
		}
		
	}
	mEtat.pointsVie--;*/
	

	 

    if(!mActionControler.empty() && mActionControler.firstAction()->isEnd())
    {
        delFirstAction();
		if(!mActionControler.empty())	TextState->setCaption(getFirstAction());
    }

	if(mActionControler.empty() && !this->mGameManager->m_TeamMap[this->getTeamNumber()]->hasManoeuvre())
    {
        //Action par defaut
		/*
		if(GameManager::getSingletonPtr()->nbBonusActivated>0)
			addInFirstAction(new ActionSeekBonus(this));
		else 
			//addInFirstAction( new ActionWander(this)); 
		  ///--
		*/
		addInFirstAction(new ActionSentinel(this)); //--
		TextState->setCaption(getFirstAction());
    }

    if(!mActionControler.empty())
    {		 
        update(pEvt, mActionControler.firstAction());
    }


	//this->TextReport->setCaption(StringConverter::toString(this->getWaypointCurrent()->mIndice));

	return true;
}

Entity * Agent::getEntity()
{
	return mEntity;
}

NxOgre::Body * Agent::getBody()
{
	return mBody;
}

void Agent::addInFirstAction(Action* pAction)
{
    this->mActionControler.addInFirstAction(pAction);
	this->TextState->setCaption(getFirstAction());
}

void Agent::addInLastAction(Action* pAction)
{
    mActionControler.addInLastAction(pAction);
}

void Agent::delFirstAction()
{
	//mHisto->ecrireHisto();
	LastAction=this->getFirstAction();
	mActionControler.delFirstAction();
}

void Agent::clearActionList()
{
	mActionControler.clear();
}

Real Agent::getOrientation()
{
	return  mOrientation;
}


Vector3 Agent::getPosition()
{
	return mBody->getGlobalPosition();
}


Real Agent::getRotation()
{
	return  mRotation;
}


Vector3 Agent::getVelocity()
{
	return mBody->getLinearVelocity();
}

GameManager* Agent::getGameManager()
{
    return mGameManager;
}

void Agent::setOrientation(Real pAngle)
{
    mOrientation=pAngle;
    Vector3 src = Vector3::UNIT_X;

    Ogre::Quaternion quat = src.getRotationTo(Vector3(sin(pAngle), 0, cos(pAngle)));
	mBody->setGlobalOrientation(quat);
	//if (mMesh=="ninja.mesh") mEntity->getParentNode()->yaw(Degree(-90.0f));
}


void Agent::setPosition(Vector3 pPosition)
{
	mBody->setGlobalPosition(pPosition);
}


void Agent::setRotation(Real pRota)
{
    mRotation=pRota;
	mBody->setAngularVelocity(Vector3(0,pRota,0));
}


void Agent::setVelocity(Vector3 pVelocity)
{
	mBody->setLinearVelocity(pVelocity);
}

Real Agent::getMaxAcceleration()
{
    return mMaxAcceleration;
}

void Agent::setMaxAcceleration(Real pMaxAcc)
{
    mMaxAcceleration=pMaxAcc;
}

Real Agent::getMaxSpeed()
{
    return mMaxSpeed;
}

void Agent::setMaxSpeed(Real pMaxSpeed)
{
    mMaxSpeed=pMaxSpeed;
}

Real Agent::getMinDeceleration()
{
    return mMinDeceleration;
}

void Agent::setMinDeceleration(Real pMinDece)
{
    mMinDeceleration=pMinDece;
}

Real Agent::getRadius()
{
    return mRadius;
}

void Agent::setRadius(Real pRad)
{
    mRadius=pRad;
}

Real Agent::lookAhead()
{
    if(getVelocity().length()==0)
        return getRotation();
    else
	{
		Real res=(atan2(getVelocity().x, getVelocity().z)-getOrientation());
		if(res>3.14) res-=6.28;
		else if(res<-3.14) res+=6.28;
        return res*2;
	}
}

void Agent::setAnimationState(const String& pAnim)
{
    mAnimationState=mEntity->getAnimationState( pAnim );
    mAnimationState->setLoop( true );
    mAnimationState->setEnabled( true );
}

static int frame=0;

void Agent::update(const FrameEvent& pEvt, Action* pAction)
{
	
	if(pEvt.timeSinceLastFrame<0.1)
	{
		TextFrag->setCaption("p"+Ogre::StringConverter::toString(peur)
							 +"v"+Ogre::StringConverter::toString(violent)
							 +"r"+Ogre::StringConverter::toString(roublard)
							 );
		frame++;
		//if(onContactWithMap) ((NxOgre::Body*)mBody)->addForce(mBody->getScene()->getGravity()*(-1), NX_ACCELERATION);

		pAction->update(pEvt);
		Real time=pEvt.timeSinceLastFrame;
		//if(time>3)	time = 0;
		setRotation(lookAhead());
		setVelocity((getVelocity()+pAction->getLinear()*time)*mArme.poids);
		setRotation(getRotation()+pAction->getAngular()*time);
		setPosition(getPosition()+0.5*pAction->getLinear()*time*time+getVelocity()*time);
		setOrientation(getOrientation()+0.5*pAction->getAngular()*time*time+getRotation()*time);
		
		mHisto->initEtats();
		if(!mIsControled && this->mEtat.pointsVie>0 && (this->mTimeFlee.getMilliseconds()>10000 || memcmp(mHisto->mInput,mHisto->mInputPrec,NB_ENTREES_NN)))
		{
			//if(frame>100 && frame%100==0)
			{
			scanEnnemis();
			if (this->m_bdecideState) 
				this->decide(); //--
			}
		}
	}
}


String Agent::getFirstAction()
{
	if(mActionControler.empty() )
		return "empty";
	else return mActionControler.firstAction()->getActionType();
}

Waypoint* Agent::getWaypointCurrent()
{
	if (this->mWaypointCurrent == NULL || (this->getPosition() - *mWaypointCurrent).length() > 50)
		this->mWaypointCurrent = this->waypointPlusProche();

	return this->mWaypointCurrent;
}

void Agent::setWaypointCurrent(Waypoint* pWaypoint)
{
	mWaypointCurrent = pWaypoint;
}

void Agent::setNbPointsDeVie(int pPv)
{
	mEtat.pointsVie=pPv;
	if( mEtat.pointsVie<=0 && this->getFirstAction()!="ActionDie")
	{
		this->mActionControler.clear();
		this->addInFirstAction(new ActionDie(this));
	}
	// Affichage des points de vie (mise a jour)
	this->TextVie->setCaption(StringConverter::toString(this->mEtat.pointsVie));
}

void Agent::setDegats(int pPv)
{
	mEtat.pointsVie-=pPv;
	if( mEtat.pointsVie<=0 && this->getFirstAction()!="ActionDie")
	{
		this->mActionControler.clear();
		this->addInFirstAction(new ActionDie(this));
	}

	// Affichage des points de vie (mise a jour)
	this->TextVie->setCaption(StringConverter::toString(this->mEtat.pointsVie));
}

Agent * Agent::getCible()
{
	return mCible;
}

void Agent::setCible(Agent *pAgent)
{
	if(mCible!=NULL)
	{
		mCible->enleverAttaquant(this);
	}
	mCible=pAgent;
}

float Agent::seuilShoot()
{
		float attDistance= (1000-(getPosition()-mCible->getPosition()).length())/750; //plus on est loin, plus c'est dur
		if (attDistance>1) attDistance=1; 
		if (attDistance<0.1) attDistance=0.1; 
		float resultat=
			mArme.precision
			*( (mMaxSpeed-getVelocity().length())/mMaxSpeed ) //plus la vitesse est grande, plus c'est dur
			//* attDistance
		//	* ( (mCible->mMaxSpeed-mCible->getVelocity().length())/mCible->mMaxSpeed)
			//* ( (mMaxSpeed-(mCible->getVelocity()-getVelocity()).length())/mMaxSpeed) //vitesse relative
			//+ (0.2*rand()) //un al�atoire pour remonter un peu tout �a
			;

		resultat=200-(200*resultat); //plus resultat est proche de 1, moins la dispersion est grande
		if (rand()>0.5) resultat=resultat*-1; //on met le signe au pifometre

		

	//	std::cout<<"V1 :" <<StringConverter::toString((mMaxSpeed-getVelocity().length())/mMaxSpeed)<<std::endl;
	//	std::cout<<"V cible :" <<StringConverter::toString( (mMaxSpeed-getVelocity().length())/mMaxSpeed )<<std::endl;
	//	std::cout<<"Distance :" <<attDistance<<std::endl;
	//	std::cout<<"Resultat shoot :" <<StringConverter::toString(resultat)<<std::endl<<std::endl;
		return resultat;
}

void Agent::shoot()//On est des gros cochons !!!
{
	lookCible();
	if (mArme.mTimeShoot.getMilliseconds()>=mArme.cadence && mCible!=NULL && !tropProche() && !tropLoin()) 
	{	
		TextArme->setCaption(mArme.name);
		mArme.mTimeShoot.reset();
		//float s=seuilShoot();
		float s=0;
		mGameManager->createProjectile("sphere.mesh",mName,getPositionTete()+Vector3(0,15,0), (mCible->getPosition()-getPositionTete()+Vector3(0,15,0) +Vector3(s,s,s)) );
		if (mArme.name=="Shotgun") //shotgun
		{
			mGameManager->createProjectile("sphere.mesh",mName,getPositionTete()+Vector3(0,15,0), (mCible->getPositionTete()-getPositionTete()+Vector3(0,15,0) +Vector3(s+5,s+5,s+5)) );
			mGameManager->createProjectile("sphere.mesh",mName,getPositionTete()+Vector3(0,15,0), (mCible->getPositionTete()-getPositionTete()+Vector3(0,15,0) +Vector3(s-5,s-5,s-5)) );
			mGameManager->createProjectile("sphere.mesh",mName,getPositionTete()+Vector3(0,15,0), (mCible->getPositionTete()-getPositionTete()+Vector3(0,15,0) +Vector3(s+10,s+10,s+10)) );
			mGameManager->createProjectile("sphere.mesh",mName,getPositionTete()+Vector3(0,15,0), (mCible->getPositionTete()-getPositionTete()+Vector3(0,15,0) +Vector3(s-10,s-10,s-10)) );
		}

	}
}

bool Agent::tropProche()
{
	if(mCible==NULL)
		return true;
	return (getPosition()-mCible->getPosition()).length()<mArme.distMin;
}

bool Agent::tropLoin()
{
	if(mCible==NULL)
		return true;
	else return (getPosition()-mCible->getPosition()).length()>mArme.distMax;
}

/**
* @Desc Retourne la position du centre de la BoundingBox de l'agent
*/
Vector3 Agent::getPositionTete()
{
	return mEntity->getParentSceneNode()->getOrientation()*mEntity->getBoundingBox().getCenter()+getPosition();
}

/**
* @Desc Determine si l'angle par rapport � l'agent cible est dans le champs de vision.
*/
bool Agent::isInSightCone(Real pnAngle)
{	
	if(this->mActionControler.empty())
		return false;

	Real ratioVision = this->mActionControler.firstAction()->getRatioVision();

	if (  (pnAngle < ratioVision*(CONST_PI/2))		&& (pnAngle > ratioVision*(CONST_PI / -2))
		||(pnAngle > ((3*CONST_PI/2) + ((CONST_PI/2)*(1 - ratioVision)))	&& (pnAngle < (2*CONST_PI)))
		||(pnAngle < ((-3*CONST_PI/2) - ((CONST_PI/2)*(1 - ratioVision)))	&& (pnAngle > (-2*CONST_PI)))
		)
		return true;
	else
		return false;
}


bool Agent::vision(Agent * pCible)
{	
	
	if (pCible==NULL || pCible->mEtat.pointsVie<=0) 
	{
		//setCible(NULL);
		//TextArme->setCaption(mArme.name);
		return false;
	}
	if (pCible->mFurtif==true) 
	{
		//en furtif, 9/10 de pas le voir
		if (rand()<0.9) return false; 
	}

	if(pCible!= this && pCible->mEtat.pointsVie>=0)
	{
		NxOgre::RayCaster * rc= new NxOgre::RayCaster(getPositionTete(),(pCible->getPositionTete()-getPositionTete()).normalisedCopy(),5000,NxOgre::RayCaster::RCT_CLOSEST,mScene);
		
		if(   rc->castShape( NxOgre::RayCaster::AF_NONE ) ) //sert a detecter tous les types de collisions
		{
			 //Waypoint * a= new Waypoint(getPositionTete());
			 NxOgre::RayCastHit hit = rc->mReport._begin(); 
			 
			 if( mBody->getName()==mName)
			 {
				 while(!rc->mReport._atEnd() && ( hit.mActor->getName()==mName || hit.mActor->getName().substr(0,2)=="Pr" || hit.mActor->getName().substr(0,2)=="Bo" ))
				 {
					 hit = rc->mReport._next();  
				 }

				// On verifie que on voit bien un agent, et pas une caisse, ou la carte
				if(hit.mActor->getName()!=mName && hit.mActor->getName().substr(0,5)=="Agent" )
				{
					Vector3 tmp		= pCible->getPosition()-getPosition();
					Real	angle	= atan2(tmp.x, tmp.z)-getOrientation();

					if (isInSightCone(angle))
					{
						//setCible(pCible);
						return true;
					}
				}
			}
		}
	}
	//TextArme->setCaption(mArme.name);
	return false;

}





void Agent::scanEnnemis()
{
	if(this->mCible!=NULL && this->vision(this->mCible))
		return;
	if(this->mCible!=NULL && this->getFirstAction()=="ActionFlee" && this->mCible->vision(this))
		return;

	this->setCible(NULL);
	mEtat.nbEnnemisEnVue=0;
	std::map<String, Agent*>::iterator it;
	for(it=mGameManager->mAgentMap.begin(); it!=mGameManager->mAgentMap.end(); it++)
	{
		// Si il existe, et qu'il n'est pas dans notre �quipe et qu'on le voit
		if (it->second != NULL && it->second->getTeamNumber() != this->m_nTeamNumber && vision(it->second)) 
		{
			// Si ce n'est pas notre cible actuelle
			if(mCible != it->second)
			{
				// TODO : D�gager ce truc pourri et passer par les methodes de la classe Team pour stoquer les ennemis en vue
				if (!this->m_bdecideState)
				{
					vecIdAgents VIDA = GameManager::getSingleton().m_TeamMap[m_nTeamNumber]->m_mTeamAgents[this->getName()];

					vecIdAgents::iterator it2;

					for (it2=VIDA.begin(); it2!=VIDA.end(); it2++)
					{
						if (!(it->second->getName() == *it2))
						{
							Report* oReport = new Report(this->getName(), REPORT_SEEN_SONE, it->second->getName());		
							Message::getSingleton()->send(oReport);
							break;
						}
					}

				}
				
				GameManager::getSingleton().m_TeamMap[m_nTeamNumber]->addTargetToAgent(this->getName(), it->second->getName());
				GameManager::getSingleton().m_TeamMap[m_nTeamNumber]->addAgentInSight(this->getName(), it->second->getName());

				//mEtat.nbEnnemisEnVue++; //pas top, � arranger pour que tous les ennemis proches soient consid�r�s comme en vue, et pas seulement ceux du champ de vision
				setCible(it->second); // du coup, le dernier scann� sera la cible (a revoir)
				it->second->ajouterAttaquant(this);
			}
		}
		else
		{
			GameManager::getSingleton().m_TeamMap[m_nTeamNumber]->remTargetToAgent(this->getName(), it->second->getName());
			GameManager::getSingleton().m_TeamMap[m_nTeamNumber]->remMemberFromAIS(this->getName(), it->second->getName());
		}
	}
	if(mCible!=NULL)
	{
		if(tropProche())
		{
			TextArme->setCaption(mArme.name+"["+mCible->getName()+"]-");
		}
		else if(tropLoin())
		{
			TextArme->setCaption(mArme.name+"["+mCible->getName()+"]+");
		}
		else TextArme->setCaption(mArme.name+" ["+mCible->getName()+"]");
	}
	else TextArme->setCaption(mArme.name);
}

Waypoint* Agent::waypointPlusProche()
{
	return GameManager::getSingleton().getCarte()->plusProche(getPosition());
}

bool Agent::waypointVisible(Waypoint * pWp)
{
	bool vois=true;
	NxOgre::RayCaster * rc= new NxOgre::RayCaster(getPositionTete(),((*pWp+Vector3(0,100,0))-getPositionTete()).normalisedCopy(),((*pWp+Vector3(0,100,0))-getPositionTete()).length()-10,NxOgre::RayCaster::RCT_CLOSEST,mScene);
	
	if(   rc->castShape( NxOgre::RayCaster::AF_NONE ) ) //sert a detecter tous les types de collisions
     { 
		 Waypoint * a= new Waypoint(getPositionTete());
		 NxOgre::RayCastHit hit = rc->mReport._begin(); 
		 
		 if( mBody->getName()==mName)
		 {
			 while(!rc->mReport._atEnd() && hit.mActor->getName()==mName && hit.mActor->getName().substr(0,5)=="Agent")
			 {
				 hit = rc->mReport._next();  
			 }
			 if(hit.mActor->getName()!=mName && hit.mActor->getName().substr(0,5)=="Agent")
			 {
				 vois= false;
			 }
		 }
	}
	return vois;
}

void Agent::ajouterAttaquant(Agent * pA)
{
	// Initialisations
	bool exist = false;

	for(int i=0; (i < (int) mAttaquants.size()) && !exist ;i++)
	{
		if (pA == mAttaquants[i]) 
			exist = true;
	}
	if(!exist)
		mAttaquants.push_back(pA);
}

bool Agent::isAttacked()
{
	for(int i=0; i<(int) mAttaquants.size();i++)
	{
		if(mAttaquants[i]->getCible()==this && mAttaquants[i]->isShooting)
		{
			mCible=mAttaquants[i];
			return true;
		}
	}
	return false;
}
String Agent::getName()
{
	return mName;
}

void Agent::enleverAttaquant(Agent * pA)
{
	if(pA!=NULL)
	{

		std::vector<Agent*>::iterator it;
		for(it=mAttaquants.begin(); it!=mAttaquants.end(); it++)
			{
				if(pA==(*it))
				{
					it=mAttaquants.erase(it);
					break;
				}
			}
	}
}
	

Waypoint * Agent::getCouverture(int pProf)
{
	Waypoint* waypointCurrent=getWaypointCurrent();

	//pour le moment, je ne gere pas la prof, flemme de recursiver, du coup, �a fait une fonction en O(N2) :s
	int visibleMin=mAttaquants.size()+1;
	Waypoint * retWp=waypointCurrent;
	int visible;

	Carte *mCarte=mGameManager->getCarte();
	for(int i=0;i<(int)mCarte->mWaypointList.size();i++)	
	{
		visible=0;
		std::vector<Agent*>::iterator it;
		for(it=mAttaquants.begin(); it!=mAttaquants.end(); it++)
        {
			if((*it)->waypointVisible(mCarte->mWaypointList[i])) visible++;
		}

		if (visible<visibleMin)
		{
			visibleMin=visible;
			retWp=mCarte->mWaypointList[i];
		}
		else if (visible==visibleMin && waypointCurrent!=mCarte->mWaypointList[i]) //en cas d'�galit�, on prend le plus proche
		{
			
			if ( ((*mWaypointCurrent)-(*mCarte->mWaypointList[i])).length() < ((*waypointCurrent)-(*retWp)).length() )
			{
				retWp=mCarte->mWaypointList[i];
			}
		}
	}
	//waypointCurrent->drawLine(retWp,GameManager::getSingletonPtr()->mSceneMgr,"rouge");
	return retWp;
	
}
void Agent::lookCible()
{
	if(mCible!=NULL)
	{
	Vector3 tmp = mCible->getPosition()-getPosition();
	setOrientation((atan2(tmp.x, tmp.z)));
	}
}
//-----------------Algo A*
void Agent::aStar(std::deque< Waypoint* > &pWalkList, int pDepart, int pArrive)
{
	//std::ofstream fichier("name.txt", std::ios::trunc);
	Carte* carte=GameManager::getSingleton().getCarte();

	//Initialisation liste ouvert, liste ferme
	double rayonMax=300;
	std::list<WaypointAStar*> listOuvert;
	std::list<WaypointAStar*> listFerme;
    for(std::vector<WaypointAStar*>::iterator it = mWaypointList.begin();it!=mWaypointList.end(); it++)
	{
		(*it)->initStar();
	}
	
	//Point de d�part
	listOuvert.push_back(mWaypointList[pDepart]);
	mWaypointList[pDepart]->etat=1;
	mWaypointList[pDepart]->setStarDistance(0);
	mWaypointList[pDepart]->setStarPoid(0);
	mWaypointList[pDepart]->setStarHauteur((*(carte->mWaypointList[pArrive])-*(carte->mWaypointList[pDepart])).length());

	//Parcours du graph par les noeuds ouverts
	bool isEnd=false;
	WaypointAStar * wpaPoidMin;
	Waypoint * wpPoidMin;
	std::list<WaypointAStar*>::iterator itPoidMin;
	while(!listOuvert.empty())
	{
		//Recherche du poid le plus petit dans les noeuds ouverts
		wpaPoidMin=*(listOuvert.begin());
		itPoidMin=listOuvert.begin();
		bool isTropLoin=true;
		for(std::list<WaypointAStar*>::iterator it=listOuvert.begin(); it!=listOuvert.end(); it++)
		{
			if((*it)->mStarPoidGlobal < wpaPoidMin->mStarPoidGlobal)
			{
				wpaPoidMin=*it;
				itPoidMin=it;
			}
			if((*it)->mStarDistance<rayonMax) isTropLoin=false;
		}

		//On ouvre les noeuds voisin
		wpPoidMin=carte->mWaypointList[wpaPoidMin->mIndice];
		for(std::vector<Waypoint*>::iterator it=wpPoidMin->mVoisin.begin(); it!=wpPoidMin->mVoisin.end() && !isEnd; it++)
		{
			double newPoid=wpaPoidMin->mStarPoid+(*wpPoidMin-*(*it)).length();
			
			//debut modif A* bas� sur danger
		/*	std::map<String, Agent*>::iterator itA;
			for(itA=mGameManager->mAgentMap.begin(); itA!=mGameManager->mAgentMap.end(); itA++)
			{
				// Si il existe, et qu'il n'est pas dans notre �quipe et qu'on le voit
				if (itA->second != NULL && itA->second->getTeamNumber() != this->m_nTeamNumber && vision(itA->second)) 
				{
					if (itA->second->waypointVisible(*it) )
					{
						newPoid+=itA->second->mArme.degat;
					}
				}
			}*/
			//fin modif 

			bool isOuvert=mWaypointList[(*it)->mIndice]->etat==1;
			bool isFerme=mWaypointList[(*it)->mIndice]->etat==0;
			if((*it)==carte->mWaypointList[pArrive]) //Si c'est l'arrivee
			{
				//On est arriv�->fin du parcours
				isEnd=true;
				mWaypointList[(*it)->mIndice]->setStarPoid(newPoid);
				mWaypointList[(*it)->mIndice]->setStarHauteur(0);
				mWaypointList[(*it)->mIndice]->mParent=wpaPoidMin->mIndice;
			}
			else if(isOuvert) //Si OUVERT on check poid
			{
				if(newPoid<mWaypointList[(*it)->mIndice]->mStarPoid)
				{
					mWaypointList[(*it)->mIndice]->setStarPoid(newPoid);
					mWaypointList[(*it)->mIndice]->mParent=wpaPoidMin->mIndice;
				}
			}
			else if(!isOuvert && !isFerme) //Si INIT (ni OUVERT, NI FERME) on OUVRE
			{
				mWaypointList[(*it)->mIndice]->setStarDistance((**it-*(carte->mWaypointList[pDepart])).length());
				mWaypointList[(*it)->mIndice]->setStarPoid(newPoid);
				mWaypointList[(*it)->mIndice]->setStarHauteur((*(carte->mWaypointList[pArrive])-**it).length());
				mWaypointList[(*it)->mIndice]->mParent=wpaPoidMin->mIndice;
				mWaypointList[(*it)->mIndice]->etat=1;//OUVRE
				listOuvert.push_back(mWaypointList[(*it)->mIndice]);
			}
		}

		//On ferme le noeud courant
		listFerme.push_back(wpaPoidMin);
		wpaPoidMin->etat=0;//FERME
		listOuvert.erase(itPoidMin);

		if(isEnd) listOuvert.clear();
	}

	//On ajoute les waypoints dans la liste
	WaypointAStar * wpa= mWaypointList[pArrive];
	while (carte->mWaypointList[wpa->mIndice]!=carte->mWaypointList[pDepart])
	{
		//carte->mWaypointList[wpa->mIndice]->drawLine(carte->mWaypointList[wpa->mParent], mGameManager->getSceneManager());
		//fichier << *(carte->mWaypointList[wpa->mIndice]) << std::endl;
		pWalkList.push_front(carte->mWaypointList[wpa->mIndice]);
		wpa=mWaypointList[wpa->mParent];
	}
	pWalkList.push_front(carte->mWaypointList[wpa->mIndice]);
}

void Agent::repop()
{
	/*setAnimationState("Die");
	mAnimationState->setLoop(false);
	*/
	//on le remet en wander
	
	mAnimationState->setLoop(true);
	
	isShooting=false;
	
	
	//on fait le repop sur la carte
	Carte * carte=GameManager::getSingleton().getCarte();
	
	/*srand(time(NULL)*this->getPosition().x);
	int dest=0;//rand()%(carte->mWaypointList.size()-1);

	if(this->m_nTeamNumber == 0)
		dest = 10;
	else
		dest = 100;
	setPosition(*(carte->mWaypointList[dest]));
	this->setWaypointCurrent(carte->mWaypointList[dest]);*/

	Waypoint* base = GameManager::getSingleton().m_TeamMap[m_nTeamNumber]->getBaseWP();
	if(base==NULL) 
	{
		srand(time(NULL)*this->getPosition().x);
		int dest=rand()%(carte->mWaypointList.size()-1);
		base=carte->mWaypointList[dest];
	}


	/////////////////----------------
	this->setPosition(*base);
	this->setWaypointCurrent(base);

	mActionControler.clear();
	//mActionControler.addInFirstAction(new ActionWander(this));
	//this->LastAction="ActionWander"; //--
	//mActionControler.addInFirstAction(new ActionSentinel(this));	//-- fred et yann
	//this->LastAction="ActionSentinel";							//-- fred et yann
	
	mBody->clearBodyFlag(NxBodyFlag::NX_BF_FROZEN_POS);
	mBody->clearBodyFlag(NxBodyFlag::NX_BF_FROZEN_ROT);
	mBody->clearActorFlag(NxActorFlag::NX_AF_DISABLE_COLLISION);
	mBody->clearActorFlag(NxActorFlag::NX_AF_DISABLE_RESPONSE);

	setNbPointsDeVie(100);
	setCible(NULL);

}


//----------- DevSquad -- debut


/**
* @desc	D�finit la team de l'agent
*/
void Agent::setTeamNumber(unsigned int pnTeamNumber)
{
	this->m_nTeamNumber = pnTeamNumber;
	this->TextTeam->setCaption(this->getGameManager()->m_TeamMap[m_nTeamNumber]->getTeamName());
}

/**
* @desc	Retourne le num�ro de la team de l'agent
*/
unsigned int Agent::getTeamNumber()
{
	return this->m_nTeamNumber;
}

/**
* @desc	Execute un ordre donn�
*/
void Agent::getCommand(Command *poCommand)
{
	switch (poCommand->getCommandType())
	{
		case COMMAND_SENTINEL	:
		{
			// Cr�ation de l'action
			ActionSentinel* oAction = new ActionSentinel(this);

			this->addInFirstAction(oAction);
			this->LastAction="ActionSentinel";

			break;
		}

		case COMMAND_MOVETO		:
		{
			// Initialisations
			Vector3	oTargetWP;

			// Si le targetWP n'est pas d�fini (= ZERO)
			if (poCommand->getTargetWP() == Vector3::ZERO)
			{
				String	sAgent		= poCommand->getTargetBot();
				oTargetWP			= this->mGameManager->mAgentMap[sAgent]->getPosition();
			}
			else
			{
				oTargetWP = poCommand->getTargetWP();
			}

			// Cr�ation de l'action
			ActionGoByPath* oAction = new ActionGoByPath(this, oTargetWP);

			this->addInFirstAction(oAction);
			this->LastAction="ActionGoByPath";

			break;
		}

		case COMMAND_ATTACK		:	
		{
			// D�finition de la cible
			this->setCible(this->mGameManager->mAgentMap[poCommand->getTargetBot()]);

			// Cr�ation de l'action
			ActionAttack* oAction = new ActionAttack(this);

			this->addInFirstAction(oAction);
			this->LastAction="ActionAttack";

			break;
		}

		case COMMAND_SEARCH		:
		{
			// Cr�ation de l'action
			ActionWander* oAction = new ActionWander(this);

			this->addInFirstAction(oAction);
			this->LastAction="ActionWander";

			break;
		}

		default					:	break;
	}
}

/**
* @desc Retourne vrai si l'agent est en vie (points de vie > 0)
*/
bool Agent::isAlive()
{
	if (this->mEtat.pointsVie > 0)
		return true;
	else
		return false;
}


//----------- DevSquad -- fin

void Agent::apprendre()
{
	String fichier = getName();
	fichier.append(".txt");
	mlp1->Run(fichier.c_str(),100);

}

void Agent::decide()
{
	bool changeAction=false;
	double *Output= new double[NB_SORTIES_NN];
	mHisto->ecrireHisto();
	this->mTimeFlee.reset();
	bool explore=true;
	mlp1->SetInputSignal(mHisto->mInput);
	mlp1->PropagateSignal();
	mlp1->GetOutputSignal(Output);
	double max=0;
	int outputSelected=0;
	for(int i =0;i<NB_SORTIES_NN;i++)
	{
		if(Output[i]>max)
		{
			max=Output[i];
			outputSelected=i;
		}
	}
	if(outputSelected==OUT_ACTION_ATTACK && !changeAction)
	{
		delFirstAction();
		addInFirstAction(new ActionAttack(this));
		changeAction=true;
		
	}
	else if(outputSelected==OUT_ACTION_SEEK && getFirstAction()!="ActionSeek" &&  mCible!=NULL && !changeAction)
	{
		delFirstAction();
		addInFirstAction(new ActionSeek(this,mCible));
		changeAction=true;
	}
	else if(outputSelected==OUT_ACTION_FLEE&& getFirstAction()!="ActionFlee" && mCible!=NULL && !changeAction)
	{
		delFirstAction();
		addInFirstAction(new ActionFlee(this,mCible));
		changeAction=true;
		
	}
	else if(outputSelected==OUT_ACTION_BONUS && GameManager::getSingletonPtr()->nbBonusActivated>0 &&getFirstAction()!="ActionSeekjBonus" && !changeAction)
	{
		delFirstAction();
		addInFirstAction(new ActionSeekBonus(this,"vie"));
		changeAction=true;
		
	}
	else if(outputSelected==OUT_ACTION_WANDER && getFirstAction()!="ActionWander" && !changeAction)
	{
		delFirstAction();
		addInFirstAction(new ActionWander(this));
		changeAction=true;
		
	}
	/*else if ( GameManager::getSingleton().mNbRound>3 && explore && !changeAction)
	{
		srand(time(NULL)*this->getPosition().x);
		int choixAction=rand()%3;

		if( choixAction == 0)
		{
			delFirstAction();
			addInFirstAction(new ActionAttack(this));
			this->mTimeFlee.reset();
			changeAction=true;
		}
		if( choixAction == 1)
		{
			delFirstAction();
			addInFirstAction(new ActionAttack(this));
			this->mTimeFlee.reset();
			changeAction=true;
		}
		if( choixAction == 2)
		{
			delFirstAction();
			addInFirstAction(new ActionWander(this));
			this->mTimeFlee.reset();
			changeAction=true;
		}


	}*/
	this->TextTeam->setCaption("a"+Ogre::StringConverter::toString((int)(Output[OUT_ACTION_ATTACK]*100)) + "f" + Ogre::StringConverter::toString((int)(Output[OUT_ACTION_FLEE]*100)) + "w" + Ogre::StringConverter::toString((int)(Output[OUT_ACTION_WANDER]*100)) + "s" + Ogre::StringConverter::toString((int)(Output[OUT_ACTION_SEEK]*100)) );

	delete[] Output;

	if(changeAction)
	{
		mHisto->ecrireHistoNote();
	}
}

void Agent::setDecideState(bool pbstate)
{
	this->m_bdecideState = pbstate;
}