///////////////////////////////////////////////////////////
//  D�rive de Manoeuvre
///////////////////////////////////////////////////////////

#ifndef MANOEUVRE_GOVIA_H
#define MANOEUVRE_GOVIA_H

#include "Manoeuvre.h"
#include "Waypoint.h"

typedef	std::vector < Waypoint* >	vecWP;

class ManoeuvreGoVia : public Manoeuvre
{
public:
	ManoeuvreGoVia(void);
	ManoeuvreGoVia(unsigned int pnTeamNumber, vecWP oPath, bool bClearAgentAction = true);
public:
	void	init ( void );	// Initialisation
	bool	isEnd( void );	// Retourne vrai si la manoeuvre est termin�e

protected:
	void	getReportArrived( Report * oReport );
	void	getReportMoveTo( Report * oReport );
	void	getReportSeenSone( Report * oReport );
	void	getReportDefault( Report * oReport );

protected:
	vecWP	m_vPath;	// Vecteur de WP que l'on va suivre
};

#endif
