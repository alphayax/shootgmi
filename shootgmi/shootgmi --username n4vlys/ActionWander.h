///////////////////////////////////////////////////////////
//  ActionWander.h
//  Implementation of the Class ActionWander
//  Created on:      02-nov.-2007 18:52:36
///////////////////////////////////////////////////////////

#if !defined(ACTIONWANDER_H)
#define ACTIONWANDER_H
#include "Action.h"
#include "Carte.h"


class ActionGoByPath;

class ActionWander : public Action
{

public:
	ActionWander();
	ActionWander(Agent* pAgent);
	virtual ~ActionWander();

    virtual bool isEnd();
    virtual void init();
	virtual void reset();
	virtual void update(const FrameEvent& pEvt);

protected:

	ActionGoByPath * mActionGoByPath;
	Waypoint * mDestWp;
};
#endif // ACTIONWANDER_H
