/*
-----------------------------------------------------------------------------
This source file is part of OGRE
    (Object-oriented Graphics Rendering Engine)
For the latest info, see http://www.ogre3d.org/

Copyright (c) 2000-2006 Torus Knot Software Ltd
Also see acknowledgements in Readme.html

You may use this sample code for anything you like, it is not covered by the
LGPL like the rest of the engine.
-----------------------------------------------------------------------------
*/
/*
-----------------------------------------------------------------------------
Filename:    ExampleFrameListener.h
Description: Defines an example frame listener which responds to frame events.
This frame listener just moves a specified camera around based on
keyboard and mouse movements.
Mouse:    Freelook
W or Up:  Forward
S or Down:Backward
A:        Step left
D:        Step right
             PgUp:     Move upwards
             PgDown:   Move downwards
             F:        Toggle frame rate stats on/off
			 R:        Render mode
             T:        Cycle texture filtering
                       Bilinear, Trilinear, Anisotropic(8)
             P:        Toggle on/off display of camera position / orientation
-----------------------------------------------------------------------------
*/

#ifndef __InteractListener_H__
#define __InteractListener_H__

#include "Ogre.h"
#include "OgreStringConverter.h"
#include "OgreException.h"

//Use this define to signify OIS will be used as a DLL
//(so that dll import/export macros are in effect)
#define OIS_DYNAMIC_LIB
#include <OIS/OIS.h>
#include <CEGUI/CEGUISystem.h>
#include <CEGUI/CEGUISchemeManager.h>
#include <OgreCEGUIRenderer.h>
#include "GameManager.h"
#include "Action.h"


using namespace Ogre;

class InteractListener: public FrameListener, public WindowEventListener, public OIS::MouseListener

{
protected:
	void updateStats(void)
	{
		static String currFps = "Current FPS: ";
		static String avgFps = "Average FPS: ";
		static String bestFps = "Best FPS: ";
		static String worstFps = "Worst FPS: ";
		static String tris = "Triangle Count: ";
		static String batches = "Batch Count: ";

		// update stats when necessary
		try {
			OverlayElement* guiAvg = OverlayManager::getSingleton().getOverlayElement("Core/AverageFps");
			OverlayElement* guiCurr = OverlayManager::getSingleton().getOverlayElement("Core/CurrFps");
			OverlayElement* guiBest = OverlayManager::getSingleton().getOverlayElement("Core/BestFps");
			OverlayElement* guiWorst = OverlayManager::getSingleton().getOverlayElement("Core/WorstFps");

			const RenderTarget::FrameStats& stats = mWindow->getStatistics();
			guiAvg->setCaption(avgFps + StringConverter::toString(stats.avgFPS));
			guiCurr->setCaption(currFps + StringConverter::toString(stats.lastFPS));
			guiBest->setCaption(bestFps + StringConverter::toString(stats.bestFPS)
				+" "+StringConverter::toString(stats.bestFrameTime)+" ms");
			guiWorst->setCaption(worstFps + StringConverter::toString(stats.worstFPS)
				+" "+StringConverter::toString(stats.worstFrameTime)+" ms");

			OverlayElement* guiTris = OverlayManager::getSingleton().getOverlayElement("Core/NumTris");
			guiTris->setCaption(tris + StringConverter::toString(stats.triangleCount));

			OverlayElement* guiBatches = OverlayManager::getSingleton().getOverlayElement("Core/NumBatches");
			guiBatches->setCaption(batches + StringConverter::toString(stats.batchCount));

		}
		catch(...) { /* ignore */ }
	}

public:
	// Constructor takes a RenderWindow because it uses that to determine input context
	InteractListener(RenderWindow* win, Camera* cam, bool bufferedKeys = false, bool bufferedMouse = false,
			     bool bufferedJoy = false ) :
		mCamera(cam), mTranslateVector(Vector3::ZERO), mWindow(win), mStatsOn(true), mNumScreenShots(0),
		mMoveScale(0.0f), mRotScale(0.0f), mTimeUntilNextToggle(0), mFiltering(TFO_BILINEAR),
		mAniso(1), mSceneDetailIndex(0), mMoveSpeed(1000), mRotateSpeed(36), mDebugOverlay(0),
		mInputManager(0), mMouse(0), mKeyboard(0), mJoy(0)
	{
		using namespace OIS;

		// CEGUI setup
		mGUIRenderer = new CEGUI::OgreCEGUIRenderer(mWindow, Ogre::RENDER_QUEUE_OVERLAY, false, 3000, GameManager::getSingleton().getSceneManager());
        mGUISystem = new CEGUI::System(mGUIRenderer);

		// Mouse
       CEGUI::SchemeManager::getSingleton().loadScheme((CEGUI::utf8*)"TaharezLookSkin.scheme");
       CEGUI::MouseCursor::getSingleton().setImage("TaharezLook", "MouseArrow");



		mLMouseDown = false;
        mRMouseDown = false;
		mCMouseDown = false;

		mDebugOverlay = OverlayManager::getSingleton().getByName("Core/DebugOverlay");

		LogManager::getSingletonPtr()->logMessage("*** Initializing OIS ***");
		ParamList pl;
		size_t windowHnd = 0;
		std::ostringstream windowHndStr;

		win->getCustomAttribute("WINDOW", &windowHnd);
		windowHndStr << windowHnd;
		pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));

		mInputManager = InputManager::createInputSystem( pl );

		//Create all devices (We only catch joystick exceptions here, as, most people have Key/Mouse)
		mKeyboard = static_cast<Keyboard*>(mInputManager->createInputObject( OISKeyboard, bufferedKeys ));
		mMouse = static_cast<Mouse*>(mInputManager->createInputObject( OISMouse, bufferedMouse ));
		try {
			mJoy = static_cast<JoyStick*>(mInputManager->createInputObject( OISJoyStick, bufferedJoy ));
		}
		catch(...) {
			mJoy = 0;
		}

		//Set initial mouse clipping size
		windowResized(mWindow);

		showDebugOverlay(true);

		//Register as a Window listener
		WindowEventUtilities::addWindowEventListener(mWindow, this);
	}

	//Adjust mouse clipping area
	virtual void windowResized(RenderWindow* rw)
	{
		unsigned int width, height, depth;
		int left, top;
		rw->getMetrics(width, height, depth, left, top);

		const OIS::MouseState &ms = mMouse->getMouseState();
		ms.width = width;
		ms.height = height;
	}

	//Unattach OIS before window shutdown (very important under Linux)
	virtual void windowClosed(RenderWindow* rw)
	{
		//Only close for window that created OIS (the main window in these demos)
		if( rw == mWindow )
		{
			if( mInputManager )
			{
				mInputManager->destroyInputObject( mMouse );
				mInputManager->destroyInputObject( mKeyboard );
				mInputManager->destroyInputObject( mJoy );

				OIS::InputManager::destroyInputSystem(mInputManager);
				mInputManager = 0;
			}
		}
	}

	virtual ~InteractListener()
	{
		//Remove ourself as a Window listener
		WindowEventUtilities::removeWindowEventListener(mWindow, this);
		windowClosed(mWindow);
	}

	virtual bool processUnbufferedKeyInput(const FrameEvent& evt)
	{
		using namespace OIS;

		if(mKeyboard->isKeyDown(KC_A))
			mTranslateVector.x = -mMoveScale;	// Move camera left

		if(mKeyboard->isKeyDown(KC_D))
			mTranslateVector.x = mMoveScale;	// Move camera RIGHT

		if(mKeyboard->isKeyDown(KC_UP) || mKeyboard->isKeyDown(KC_W) )
			mTranslateVector.z = -mMoveScale;	// Move camera forward

		if(mKeyboard->isKeyDown(KC_DOWN) || mKeyboard->isKeyDown(KC_S) )
			mTranslateVector.z = mMoveScale;	// Move camera backward

		if(mKeyboard->isKeyDown(KC_PGUP))
			mTranslateVector.y = mMoveScale;	// Move camera up

		if(mKeyboard->isKeyDown(KC_PGDOWN))
			mTranslateVector.y = -mMoveScale;	// Move camera down

		if(mKeyboard->isKeyDown(KC_RIGHT))
			mCamera->yaw(-mRotScale);

		if(mKeyboard->isKeyDown(KC_LEFT))
			mCamera->yaw(mRotScale);

		if( mKeyboard->isKeyDown(KC_ESCAPE) || mKeyboard->isKeyDown(KC_Q)){
			return false;
		}

		if( mKeyboard->isKeyDown(KC_R) && mTimeUntilNextToggle <= 0 )
		{
			GameManager::getSingleton().restartPartie();
			mTimeUntilNextToggle = 1;
		}

		if( mKeyboard->isKeyDown(KC_SPACE) && mTimeUntilNextToggle <= 0 )
		{
			GameManager* mGameMgr = GameManager::getSingletonPtr();
			unsigned int nNbTeams = mGameMgr->m_TeamMap.size(); // Nombre d'�quipes cr�ees

	
			Agent * oAgentCourrant	= mGameMgr->createAgent("robot.mesh",Vector3(200,300,-100));

			oAgentCourrant->repop();
			mGameMgr->m_TeamMap[mGameMgr->mAgentMap.size()%nNbTeams]->addTeamMember(oAgentCourrant->getName());
			mTimeUntilNextToggle = 1;

		}

       	if( mKeyboard->isKeyDown(KC_F) && mTimeUntilNextToggle <= 0 )
		{
			mStatsOn = !mStatsOn;
			showDebugOverlay(mStatsOn);
			mTimeUntilNextToggle = 1;
		}

		if( mKeyboard->isKeyDown(KC_T) && mTimeUntilNextToggle <= 0 )
		{
			switch(mFiltering)
			{
			case TFO_BILINEAR:
				mFiltering = TFO_TRILINEAR;
				mAniso = 1;
				break;
			case TFO_TRILINEAR:
				mFiltering = TFO_ANISOTROPIC;
				mAniso = 8;
				break;
			case TFO_ANISOTROPIC:
				mFiltering = TFO_BILINEAR;
				mAniso = 1;
				break;
			default: break;
			}
			MaterialManager::getSingleton().setDefaultTextureFiltering(mFiltering);
			MaterialManager::getSingleton().setDefaultAnisotropy(mAniso);

			showDebugOverlay(mStatsOn);
			mTimeUntilNextToggle = 1;
		}

		if(mKeyboard->isKeyDown(KC_SYSRQ) && mTimeUntilNextToggle <= 0)
		{
			std::ostringstream ss;
			ss << "screenshot_" << ++mNumScreenShots << ".png";
			mWindow->writeContentsToFile(ss.str());
			mTimeUntilNextToggle = 0.5;
			mDebugText = "Saved: " + ss.str();
		}

		if(mKeyboard->isKeyDown(KC_O) && mTimeUntilNextToggle <=0)
		{
			mSceneDetailIndex = (mSceneDetailIndex+1)%3 ;
			switch(mSceneDetailIndex) {
				case 0 : mCamera->setPolygonMode(PM_SOLID); break;
				case 1 : mCamera->setPolygonMode(PM_WIREFRAME); break;
				case 2 : mCamera->setPolygonMode(PM_POINTS); break;
			}
			mTimeUntilNextToggle = 0.5;
		}

		static bool displayCameraDetails = false;
		if(mKeyboard->isKeyDown(KC_P) && mTimeUntilNextToggle <= 0)
		{
			displayCameraDetails = !displayCameraDetails;
			mTimeUntilNextToggle = 0.5;
			if (!displayCameraDetails)
				mDebugText = "";
		}

		// Print camera details
		if(displayCameraDetails)
			mDebugText = "P: " + StringConverter::toString(mCamera->getDerivedPosition()) +
						 " " + "O: " + StringConverter::toString(mCamera->getDerivedOrientation());

		// Return true to continue rendering
		return true;
	}

	bool processUnbufferedMouseInput(const FrameEvent& evt)
	{
		using namespace OIS;

		// Rotation factors, may not be used if the second mouse button is pressed
		// 2nd mouse button - slide, otherwise rotate
		const MouseState &ms = mMouse->getMouseState();
		/*if( ms.buttonDown( MB_Right) && ms.buttonDown( MB_Left))
		{
			CEGUI::MouseCursor::getSingleton().hide();
			mTranslateVector.x += ms.X.rel * 0.13;
			mTranslateVector.y -= ms.Y.rel * 0.13;
		}
		else*/
		if(ms.buttonDown( MB_Right) && ms.buttonDown( MB_Left))
		{
			CEGUI::MouseCursor::getSingleton().hide();
			mRotX = Degree(-ms.X.rel * 0.13);
			mRotY = Degree(-ms.Y.rel * 0.13);
		}
		else if (ms.buttonDown( OIS::MB_Left))
		{
			CEGUI::Point mousePos = CEGUI::MouseCursor::getSingleton().getPosition();
			Ray ionX=mCamera->getCameraToViewportRay(mousePos.d_x/float(ms.width), mousePos.d_y/float(ms.height));
			
			NxOgre::RayCaster * rc= new NxOgre::RayCaster(ionX.getOrigin(),ionX.getDirection().normalisedCopy(),5000,NxOgre::RayCaster::RCT_CLOSEST,GameManager::getSingleton().getScene());
			/*
			Waypoint *wp= new Waypoint(ionX.getOrigin());
			Waypoint *wp2= new Waypoint((ionX.getDirection()*5000));
			wp->drawLine(wp2,GameManager::getSingleton().getSceneManager());
			*/
			

			if(   rc->castShape( NxOgre::RayCaster::AF_NONE ) ) //sert a detecter tous les types de collisions
			{ 
				 NxOgre::RayCastHit hit = rc->mReport._begin(); 
				 while(!rc->mReport._atEnd() && hit.mActor->getName().substr(0,5)!="Agent")
				 {
					 hit = rc->mReport._next();  
				 }
				 if(hit.mActor->getName().substr(0,5)=="Agent" ) //On verifie que on voit bien un agent, et pas une caisse, ou la carte
				 {
					 GameManager::getSingleton().selectAgent(hit.mActor->getName());
				 }
				 else GameManager::getSingleton().unselectAllAgent();
			}
		}
		else if (ms.buttonDown( OIS::MB_Right))
		{
			CEGUI::Point mousePos = CEGUI::MouseCursor::getSingleton().getPosition();
			Ray ionX=mCamera->getCameraToViewportRay(mousePos.d_x/float(ms.width), mousePos.d_y/float(ms.height));
			
			NxOgre::RayCaster * rc= new NxOgre::RayCaster(ionX.getOrigin(),ionX.getDirection().normalisedCopy(),5000,NxOgre::RayCaster::RCT_CLOSEST,GameManager::getSingleton().getScene());
			/*
			Waypoint *wp= new Waypoint(ionX.getOrigin());
			Waypoint *wp2= new Waypoint((ionX.getDirection()*5000));
			wp->drawLine(wp2,GameManager::getSingleton().getSceneManager());
			*/
			

			if(   rc->castShape( NxOgre::RayCaster::AF_NONE ) ) //sert a detecter tous les types de collisions
			{ 
				 NxOgre::RayCastHit hit = rc->mReport._begin(); 
				 while(!rc->mReport._atEnd() && hit.mActor->getName().substr(0,5)!="Agent" && hit.mActor->getName().substr(0,5)!="Carte")
				 {
					 hit = rc->mReport._next();  
				 }
				 
				 Agent* agentSelected=GameManager::getSingleton().agentSelected;
				 if(agentSelected!=NULL)
				 {
					 //Si on click droit sur l'agent selectionn� et qu'il est control�, on le d�controle
					 if(hit.mActor->getName()==agentSelected->getName() && agentSelected->mIsControled)
					 {
						 GameManager::getSingleton().uncontrolAgent(hit.mActor->getName());
					 }
					 //Si on click droit sur un agent, alors l'agent selectionn� l'attack
					 else if(hit.mActor->getName().substr(0,5)=="Agent" && hit.mActor->getName()!=agentSelected->getName())
					 {
						GameManager::getSingleton().controlAgent(agentSelected->getName());
						agentSelected->setCible(GameManager::getSingleton().mAgentMap[hit.mActor->getName()]);
						agentSelected->delFirstAction();
						agentSelected->addInFirstAction(new ActionAttack(agentSelected));
					 }
					 //Si on click droit sur la map, l'agent selectionn� va � cet endroit
					 else if(hit.mActor->getName().substr(0,5)=="Carte")
					 {
						 GameManager::getSingleton().controlAgent(agentSelected->getName());

						 //On fait un GoTo ou un GoByPath 
						Vector3 dir=hit.mWorldImpact-agentSelected->getPosition();
						NxOgre::RayCaster * rc2= new NxOgre::RayCaster(agentSelected->getPositionTete(),dir.normalisedCopy(),dir.length(),NxOgre::RayCaster::RCT_CLOSEST,GameManager::getSingleton().getScene());
						//Waypoint wp(agentSelected->getPositionTete());
						//wp.drawLine(new Waypoint(wp+dir),GameManager::getSingleton().getSceneManager());
						//agentSelected->TextFrag->setCaption("je touche rien");
						if(   rc2->castShape( NxOgre::RayCaster::AF_NONE ) ) //sert a detecter tous les types de collisions
						{ 
							NxOgre::RayCastHit hit2 = rc2->mReport._begin(); 
							int nbToucheCarte=0;
							while(!rc2->mReport._atEnd() )
							{
								if(hit2.mActor->getName().substr(0,5)=="Carte") nbToucheCarte++;
								hit2 = rc2->mReport._next();  
							}
							//agentSelected->TextFrag->setCaption(StringConverter::toString(nbToucheCarte));
							agentSelected->setCible(NULL);
							agentSelected->clearActionList();
							agentSelected->addInFirstAction(new ActionIdle(agentSelected));

							agentSelected->addInFirstAction(new ActionGoByPath(agentSelected, hit.mWorldImpact));
							
						}
						else // si pas de collision
						{
							agentSelected->setCible(NULL);
							agentSelected->clearActionList();
							agentSelected->addInFirstAction(new ActionIdle(agentSelected));

							agentSelected->addInFirstAction(new ActionGoTo(agentSelected, hit.mWorldImpact));
						}
					 }
				 }
			}
		}
		else
		{
			CEGUI::MouseCursor::getSingleton().show();
			// Update CEGUI with the mouse motion	
			CEGUI::System::getSingleton().injectMouseMove(ms.X.rel, ms.Y.rel);

		}

		return true;
	}

	void moveCamera()
	{
		// Make all the changes to the camera
		// Note that YAW direction is around a fixed axis (freelook style) rather than a natural YAW
		//(e.g. airplane)
		mCamera->yaw(mRotX);
		mCamera->pitch(mRotY);
		mCamera->moveRelative(mTranslateVector);
	}

	void showDebugOverlay(bool show)
	{
		if (mDebugOverlay)
		{
			if (show)
				mDebugOverlay->show();
			else
				mDebugOverlay->hide();
		}
	}

	// Override frameStarted event to process that (don't care about frameEnded)
	bool frameStarted(const FrameEvent& evt)
	{
		using namespace OIS;

		if(mWindow->isClosed())	return false;

		//Need to capture/update each device
		mKeyboard->capture();
		mMouse->capture();
		if( mJoy ) mJoy->capture();

		bool buffJ = (mJoy) ? mJoy->buffered() : true;

		//Check if one of the devices is not buffered
		if( !mMouse->buffered() || !mKeyboard->buffered() || !buffJ )
		{
			// one of the input modes is immediate, so setup what is needed for immediate movement
			if (mTimeUntilNextToggle >= 0)
				mTimeUntilNextToggle -= evt.timeSinceLastFrame;

			// If this is the first frame, pick a speed
			if (evt.timeSinceLastFrame == 0)
			{
				mMoveScale = 1;
				mRotScale = 0.1;
			}
			// Otherwise scale movement units by time passed since last frame
			else
			{
				// Move about 100 units per second,
				mMoveScale = mMoveSpeed * evt.timeSinceLastFrame;
				// Take about 10 seconds for full rotation
				mRotScale = mRotateSpeed * evt.timeSinceLastFrame;
			}
			mRotX = 0;
			mRotY = 0;
			mTranslateVector = Ogre::Vector3::ZERO;
		}

		//Check to see which device is not buffered, and handle it
		if( !mKeyboard->buffered() )
			if( processUnbufferedKeyInput(evt) == false )
				return false;
		if( !mMouse->buffered() )
			if( processUnbufferedMouseInput(evt) == false )
				return false;

		if( !mMouse->buffered() || !mKeyboard->buffered() || !buffJ )
			moveCamera();

		return true;
	}

	bool frameEnded(const FrameEvent& evt)
	{
		updateStats();
		return true;
	}

	bool mouseMoved(const OIS::MouseEvent &arg)
    {
		// Update CEGUI with the mouse motion
       CEGUI::System::getSingleton().injectMouseMove(arg.state.X.rel, arg.state.Y.rel);

        return true;
    }

    bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id)
    {
        // Left mouse button down
       if (id == OIS::MB_Left)
       {
           mLMouseDown = true;
       } // if

       // Right mouse button down
       else if (id == OIS::MB_Right)
       {
           CEGUI::MouseCursor::getSingleton().hide();
           mRMouseDown = true;
       } 
	   else if (id == OIS::MB_Middle)
       {
           CEGUI::MouseCursor::getSingleton().hide();
           mCMouseDown = true;
       } 
		return true;
    }

    bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id)
    {
		// Left mouse button up
       if (id == OIS::MB_Left)
       {
           mLMouseDown = false;
       } // if

       // Right mouse button up
	   else if (id == OIS::MB_Middle)
       {
           CEGUI::MouseCursor::getSingleton().show();
           mCMouseDown = false;
       }
       else if (id == OIS::MB_Right)
       {
           CEGUI::MouseCursor::getSingleton().show();
           mRMouseDown = false;
       }

        return true;
    }

protected:
	Camera* mCamera;

	Vector3 mTranslateVector;
	RenderWindow* mWindow;
	bool mStatsOn;

	std::string mDebugText;

	unsigned int mNumScreenShots;
	float mMoveScale;
	Degree mRotScale;
	// just to stop toggles flipping too fast
	Real mTimeUntilNextToggle ;
	Radian mRotX, mRotY;
	TextureFilterOptions mFiltering;
	int mAniso;

	int mSceneDetailIndex ;
	Real mMoveSpeed;
	Degree mRotateSpeed;
	Overlay* mDebugOverlay;

	//OIS Input devices
	OIS::InputManager* mInputManager;
	OIS::Mouse*    mMouse;
	OIS::Keyboard* mKeyboard;
	OIS::JoyStick* mJoy;

	CEGUI::Renderer *mGUIRenderer;     // cegui renderer
	CEGUI::System * mGUISystem;


	bool mLMouseDown, mRMouseDown, mCMouseDown;     // True if the mouse buttons are down
};

#endif
