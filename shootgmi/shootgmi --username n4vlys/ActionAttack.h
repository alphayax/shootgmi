///////////////////////////////////////////////////////////
//  ActionAttack.h
//  Implementation of the Class ActionAttack
//  Created on:      02-nov.-2007 18:52:36
///////////////////////////////////////////////////////////

#if !defined(ACTIONATTACK_H)
#define ACTIONATTACK_H

#include "Action.h"

class ActionAttack : public Action
{

public:
	ActionAttack();
	ActionAttack(Agent* pAgent);
	virtual ~ActionAttack();

	virtual bool isEnd();
    virtual void init();
	virtual void reset();
	virtual void update(const FrameEvent& pEvt);

	bool mIsStop;
};
#endif // ActionAttack_H
