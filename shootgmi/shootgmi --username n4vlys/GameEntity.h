///////////////////////////////////////////////////////////
//  GameEntity.h
//  Implementation of the Class GameEntity
//  Created on:      02-nov.-2007 18:52:37
///////////////////////////////////////////////////////////

#if !defined(GAMEENTITY_H)
#define GAMEENTITY_H
#include "ogre.h"

class GameEntity
{

public:
	GameEntity();
	virtual ~GameEntity();

};
#endif // GAMEENTITY_H
