#include "Team_AgentsInSight.h"

/**
* @desc Constructeur par d�faut
*/
Team_AgentsInSight::Team_AgentsInSight(){}


/**
* @desc Constructeur particulier. Demande qui a vu et ce qu'il a vu.
*/
Team_AgentsInSight::Team_AgentsInSight(Ogre::String psMember, Ogre::String psAgent)
{
	this->updateTarget(psMember, psAgent);
}


/**
* @desc Dit si la cible est toujours en vue par quelqu'un
*/
bool Team_AgentsInSight::isVisible()
{
	return this->m_bAlreadyVisible;
}


/**
* @desc Retourne les membres de l'�quipe qui voient la cible courrante
*/
vecIdAgents Team_AgentsInSight::isVisibleBy()
{
	return this->m_vTargetByMembers;
}


/**
* @desc Met a jour les donn�es d'une cible
*/
void Team_AgentsInSight::updateTarget(Ogre::String psMember, Ogre::String psAgent)
{
	// Mise a jour de la position
	this->m_oLastWPSeen		= GameManager::getSingletonPtr()->mAgentMap[psAgent]->getPosition();

	// On dit bien qu'on l'a d�couvert
	this->m_bAlreadyVisible	= true;

	// On se rajoute � la liste des personnes le voyant
	this->m_vTargetByMembers.push_back(psMember);
}

void Team_AgentsInSight::remMemberViewing(Ogre::String psMember)
{
	vecIdAgents VIDA = this->m_vTargetByMembers;

	vecIdAgents::iterator it;

		for (it=VIDA.begin(); it!=VIDA.end(); ++it)
		{
			if (psMember == *it) {VIDA.erase(it); return;}
		}
}
