///////////////////////////////////////////////////////////
//  ActionGoTo.h
//  Implementation of the Class ActionGoTo
//  Created on:      02-nov.-2007 18:52:36
///////////////////////////////////////////////////////////

#if !defined(ACTIONSUIVRE_H)
#define ACTIONSUIVRE_H

#include "Action.h"

class ActionSuivre : public Action
{

public:
	ActionSuivre();
	ActionSuivre(Agent* pAgentActeur, Agent* pAgentCible);
	virtual ~ActionSuivre();

	virtual bool isEnd();
    virtual void init();
	virtual void reset();
	virtual void update(const FrameEvent& pEvt);

	virtual void setCible(Agent* pAgentCible);

protected:
	Agent* mCible;
	Real mDistance;
};
#endif // ACTIONGOTO_H
