///////////////////////////////////////////////////////////
//  ManoeuvreControler.h
///////////////////////////////////////////////////////////

#ifndef MANOEUVRECONTROLER_H
#define MANOEUVRECONTROLER_H

#include <deque>

#include "Manoeuvre.h"

class Manoeuvre;

typedef	std::deque< Manoeuvre* >	dequeManoeuvre;

class ManoeuvreControler
{

public:
	ManoeuvreControler();
	~ManoeuvreControler();

public:
	void addFrontManoeuvre(Manoeuvre* pManoeuvre);	// Ajoute une manoeuvre en haut de la pile
	void addBackManoeuvre(Manoeuvre* pManoeuvre);	// Ajoute une manoeuvre en fin de pile
	void remFrontManoeuvre(void);					// Supprime la manoeuvre en haut de la pile
	Manoeuvre* getFrontManoeuvre(void);				// Retourne la manoeuvre courrante

	bool isEmpty();	// Retourne vrai si la pile est vide
	void clear();	// Efface le contenu de la pile

	dequeManoeuvre m_dManoeuvreList;

};

#endif 
