#if !defined(ACTIONSEEK_H)
#define ACTIONSEEK_H
#include "Action.h"
#include "Carte.h"


class ActionGoByPath;

class ActionSeek : public Action
{

public:
	ActionSeek();
	ActionSeek(Agent* pAgent, Agent* pAgentTarget);
	virtual ~ActionSeek();

    virtual bool isEnd();
    virtual void init();
	virtual void reset();
	virtual void update(const FrameEvent& pEvt);

protected:

	ActionGoByPath * mActionGoByPath;
	ActionGoTo * mActionGoTo;
	Agent * mAgentTarget;
	Waypoint* mTargetLastWaypoint;
	Timer mLastNewPathTimer;
};
#endif // ACTIONSEEK_H