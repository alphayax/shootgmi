///////////////////////////////////////////////////////////
//  ActionWander.cpp
//  Implementation of the Class ActionWander
//  Created on:      02-nov.-2007 18:52:36
///////////////////////////////////////////////////////////

#include "ActionWander.h"
#include "GameManager.h"


ActionWander::ActionWander() : Action()
{
    mActionType="ActionWander";
	mActionGoByPath=NULL;
	mDestWp=NULL;
}


ActionWander::ActionWander(Agent* pAgent)
    : Action(pAgent)
{
    mActionType="ActionWander";
	mActionGoByPath=NULL;
	mDestWp=NULL;
}


ActionWander::~ActionWander()
{
	mActionGoByPath;
}

void ActionWander::init()
{
	reset();
	Carte * carte=GameManager::getSingleton().getCarte();

	int dest=rand()%(carte->mWaypointList.size()-1);
	mDestWp=carte->mWaypointList[dest];

	mActionGoByPath=new ActionGoByPath(mAgent, mDestWp);
	mActionGoByPath->init();

    mIsInit=true;
}

void ActionWander::reset()
{
	delete mActionGoByPath;
	mActionGoByPath=NULL;
    mIsInit=false;
}

bool ActionWander::isEnd()
{
	return  false;
}


void ActionWander::update(const FrameEvent& pEvt)
{
	if(mActionGoByPath->isEnd())
	{
		Carte * carte=GameManager::getSingleton().getCarte();
		int dest=rand()%(carte->mWaypointList.size()-1);
		mDestWp=carte->mWaypointList[dest];
		mActionGoByPath->setDestination(mDestWp);
	}
	mActionGoByPath->update(pEvt);
	mLinear=mActionGoByPath->getLinear();
	mAngular=mActionGoByPath->getAngular();
}

