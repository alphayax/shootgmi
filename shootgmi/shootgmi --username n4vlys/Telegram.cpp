#include "Telegram.h"

Telegram::Telegram(){}

Telegram::Telegram(String psSender, String psReceiver, void *poExtra)
{
	this->m_sSender		= psSender;
	this->m_sReceiver	= psReceiver;
	this->m_nType		= MESSAGE_DEFAULT;
	this->m_oExtra		= poExtra;
}

int Telegram::getType()
{
	return this->m_nType;
}

String Telegram::getSender()
{
	return this->m_sSender;
}

String Telegram::getReceiver()
{
	return this->m_sReceiver;
}