#include "FSMState.h"

FSMState::FSMState(int id, unsigned maxTransition)
{
	//Initialisation
	m_StateId = id;

	if(!maxTransition){ //si maxT = 0
		m_MaxNumTransition = 1;
	}else{
		m_MaxNumTransition = maxTransition;
	}

	//Allocation des tableaux
	m_Input = new int[m_MaxNumTransition];
	for(int i = 0;i<m_MaxNumTransition;++i){
		m_Input[i] = 0;
	}

	m_OutputState = new int[m_MaxNumTransition];
	for(int i = 0;i<m_MaxNumTransition;++i){
		m_OutputState[i] = 0;
	}


}

FSMState::~FSMState(void)
{
	//Lib�re la m�moire
	delete [] m_Input;
	delete [] m_OutputState;
}
	
void FSMState::addTransition(int inputTransition, int outputState){


	//On cherche la premiere case non utilise
	int i = 0;
	for(i = 0; i<m_MaxNumTransition; ++i){
		if(!m_OutputState[i])
			break;
	}
	// on ajoute la transition
	if(i < m_MaxNumTransition){
		m_OutputState[i] = outputState;
		m_Input[i]      = inputTransition ;
	}
}

void FSMState::deleteTransition(int outputState){
	//On cherche la transition a supprimer
	int i = 0;
	for(i = 0; i<m_MaxNumTransition; ++i){
		if(m_OutputState[i] == outputState)
			break;
	}

	//si l'offset i est superieur au max
	if(i>=m_MaxNumTransition) return;
	
	//Remise a zero
	m_OutputState[i] = 0;
	m_Input[i]       = 0;

	//On decale le contenu
	for(; i<(m_MaxNumTransition - 1); ++i){
		if(!m_OutputState[i])
			break;

		m_Input[i]       = m_Input[i+1];
		m_OutputState[i] = m_OutputState[i+1];

	}
	//Le dernier element deviens donc libre
	//on le remets a zero*
	m_Input[i]       = 0;
	m_OutputState[i] = 0;
}

int FSMState::getOuput(int inputTransition){
	int outputState = m_StateId;
	

	for(int i = 0; i<m_MaxNumTransition; ++i){
		//si fin du tableau
		if(!m_OutputState[i]) break;
		
		//si on a trouver une transition possible
		if(inputTransition == m_Input[i]){
			outputState = m_OutputState[i];
			return outputState;
		}
	}
	
	//Pas de transition valide
	return outputState;
}

