#include "Arme.h"

Arme::Arme(void)
{
}

Arme::Arme(typeArme pArme)
{
	switch(pArme)
	{
	case PISTOL:
		name="Pistol";
		cadence=600;
		precision=0.8;
		distMin=0;
		distMax=800;
		poids=1;
		degat=20;
		break;

	case ASSAULT:
		name="Assault";
		cadence=300;
		precision=0.6;
		distMin=100;
		distMax=1000;
		poids=1;
		degat=10;
		break;

	case SHOTGUN:
		name="Shotgun";
		cadence=1000;
		precision=0.4;
		distMin=0;
		distMax=600;
		poids=0.9;
		degat=10; //10 par balles donc 40 en tout
		break;

	case SNIPE:
		name="Snipe";
		cadence=3000;
		precision=1;
		distMin=700;
		distMax=3000;
		poids=0.9;
		degat=80;
		break;
	}
}

Arme::~Arme(void)
{
		name="None";
}

